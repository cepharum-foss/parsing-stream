# parsing-stream  [![pipeline status](https://gitlab.com/cepharum-foss/parsing-stream/badges/master/pipeline.svg)](https://gitlab.com/cepharum-foss/parsing-stream/-/commits/master)

_attaching parser/processor to Node.js streams_

## License

[MIT](LICENSE)

## About

This module provides an octet-mode transform stream supporting attachment of a parser which is observing streamed data for certain conditions triggering extra actions such as collecting or replacing matches, extracting information, switching parsers, redirecting data to different streams etc.

## Documentation

We've set up a [separate website with additional information on this package](https://cepharum-foss.gitlab.io/parsing-stream).
