/**
 * @typedef {object} ParserResult
 * @param {Buffer[]} parsed list of buffers parsed before
 * @param {?Buffer} unparsed buffer containing data left unparsed
 * @param {?Parser} parser selects new parser to use for any unparsed data
 */

/**
 * @typedef {function(this:Parser, context:Context, buffers:Array<Buffer>, matchOffset:int):(Parser|Promise<Parser>)} MatchHandlerCallback
 */

/**
 * Implements generic parser interface suitable for successively parsing a
 * sequence of buffers.
 *
 * @type {Parser}
 * @name Parser
 * @property {Buffer[]} buffers raw set of collected buffers previously kept for
 *                      probably matching current parser's expectations
 * @property {Buffer} data buffer with data previously kept for probably
 *                    matching current parser's expectations
 * @property {MatchHandlerCallback} onMatch callback invoked if current parser has got matching input
 */
export class Parser {
	/**
	 * @param {ParserOptions} options customizations for parser instance
	 */
	constructor( options = null ) {
		/** @type MatchHandler */
		let handler = null;

		if ( this.onMatch && typeof this.onMatch === "function" ) {
			handler = this.onMatch;
		}

		/** @type Buffer[] */
		let heldBuffers = [];

		Object.defineProperties( this, {
			onMatch: {
				get: () => handler,
				set: fn => {
					if ( !fn || typeof fn === "function" ) {
						handler = fn;
					} else {
						throw new TypeError( "invalid match handler" );
					}
				}
			},

			heldBuffers: {
				get: () => heldBuffers,
				set: buffers => {
					if ( !Array.isArray( buffers ) ) {
						throw new TypeError( "invalid list of held buffers" );
					}

					for ( let i = 0, l = buffers.length; i < l; i++ ) {
						if ( !Buffer.isBuffer( buffers[i] ) ) {
							throw new TypeError( "invalid item in list of held buffers" );
						}
					}

					heldBuffers = buffers;
				},
			}
		} );

		if ( options && options.onMatch ) {
			this.onMatch = options.onMatch;
		}

		this.resetState();
	}

	/**
	 * Resets current state of parser causing it to restart search on next chunk
	 * of provided data.
	 *
	 * @note This method is utilizing static method Parser.resetState() due to
	 *       the required composition of certain parser implementations caused
	 *       by Javascript's lack of multi inheritance.
	 *
	 * @returns {void}
	 */
	resetState() {
		this.constructor.resetState( this );
	}

	/**
	 * Resets state of provided parser usually causing that parser to restart
	 * searching on next provided chunk of data.
	 *
	 * @param {Parser} parser instance of parser to be updated
	 * @returns {void}
	 */
	static resetState( parser ) {
		parser.state = {}; // eslint-disable-line no-param-reassign
	}

	/**
	 * Fetches raw data matching current parser's expectations.
	 *
	 * @returns {?Buffer} fetched data
	 */
	get data() {
		const length = this.heldBuffers.length;
		if ( length > 1 ) {
			this.heldBuffers.splice( 0, length, Buffer.concat( this.heldBuffers ) );
		}

		return length > 0 ? this.heldBuffers[0] : Buffer.alloc( 0 );
	}

	/**
	 * Fetches number of recently seen octets considered beginning of a larger
	 * match.
	 *
	 * @returns {number} number of matching octets seen in previous chunks of streamed data
	 */
	get previouslyMatchingOctetsCount() {
		let count = 0;

		for ( let i = 0, b = this.heldBuffers, l = b.length; i < l; i++ ) {
			count += b[i].length;
		}

		return count;
	}

	/**
	 * Fetches chunks with recently seen octets collected before for being
	 * considered beginning of a larger match.
	 *
	 * @returns {Buffer[]} chunks of matching octets previously seen in stream
	 */
	get previouslyMatchingChunks() {
		return this.heldBuffers;
	}

	/**
	 * Indicates if parser was matching partially previously.
	 *
	 * @returns {boolean} true if partial match has been encountered
	 */
	get wasMatchingPartially() {
		return this.heldBuffers.length > 0;
	}

	/**
	 * Invokes attached handler when parser has matched some input.
	 *
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer[]} buffers set of buffers containing matching data
	 * @param {int} matchOffset octet offset into stream of match
	 * @returns {?(Promise<Parser>|Parser)} promises optional parser replacing current one in processing the stream
	 */
	_handleMatch( context, buffers, matchOffset ) {
		const matchHandler = this.onMatch;
		let result = null;

		if ( typeof matchHandler === "function" ) {
			result = matchHandler.call( this, context, buffers, matchOffset );
		}

		return result instanceof Promise ? result : Promise.resolve( result );
	}

	/**
	 * Gets invoked on attaching this parser to a stream.
	 *
	 * @param {Stream} stream stream instance parser is being attached to
	 * @returns {void}
	 */
	onAttach( stream ) {
		this.stream = stream;
	}

	/**
	 * Gets invoked on detaching this parser from a stream.
	 *
	 * @returns {void}
	 */
	onDetach() {
		this.stream = null;
	}

	/**
	 * Parses another buffer considered representing slice of file or stream at
	 * provided offset for sharing extracted information in provided context.
	 *
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @param {boolean} isLast true if provided chunk is last one of attached stream
	 * @returns {Promise<ParserResult>} promises result of parser
	 * @abstract
	 */
	parse( context, buffer, atOffset, isLast ) { // eslint-disable-line no-unused-vars
		return Promise.reject( new Error( "invalid use of basic parser" ) );
	}

	/**
	 * Collects buffer ready for release by means of forwarding to output end of
	 * stream this parser is attached to.
	 *
	 * @param {Array<Buffer>} collector collection of buffers to release next (which is some time in near future)
	 * @param {Buffer} buffer buffer to collect for release
	 * @returns {void}
	 */
	collectForwarded( collector, buffer ) {
		collector.push( buffer );
	}

	/**
	 * Releases octets of previously held back buffers and provided follow-up
	 * buffer for not containing any matching octets.
	 *
	 * @param {Buffer} buffer currently processed buffer following any previously held back buffers
	 * @param {int} firstIndex index of first octet to hold back further on, 0 is first octet of provided buffer
	 * @returns {Buffer[]} buffers with released octets
	 * @protected
	 */
	releaseHeldBuffers( buffer, firstIndex ) {
		const released = [];

		if ( firstIndex > -1 ) {
			// current match starts in bounds of currently processed buffer
			// -> instantly move any previously collected buffers w/ partial
			//    matches to list of buffers ready for output
			if ( this.heldBuffers.length > 0 ) {
				for ( let i = 0, m = this.heldBuffers, l = m.length; i < l; i++ ) {
					this.collectForwarded( released, m[i] );
				}

				this.heldBuffers = [];
			}

			if ( firstIndex > 0 ) {
				// some leading part of current buffer is suitable for being
				// moved to list of buffers ready for output, too
				this.collectForwarded( released, buffer.slice( 0, firstIndex ) );
			}
		} else if ( firstIndex < 0 ) {
			const minFirstIndex = -this.previouslyMatchingOctetsCount;

			if ( firstIndex < minFirstIndex ) {
				throw new Error( "cannot hold back more octets than previously collected" );
			}

			// release all octets preceding indicated index of match
			for ( let numReleaseOctets = firstIndex - minFirstIndex; numReleaseOctets > 0; ) {
				const earliest = this.heldBuffers.shift();

				if ( earliest.length > numReleaseOctets ) {
					this.collectForwarded( released, earliest.slice( 0, numReleaseOctets ) );
					this.heldBuffers.unshift( earliest.slice( numReleaseOctets ) );
				} else {
					this.collectForwarded( released, earliest );
				}

				numReleaseOctets -= earliest.length;
			}
		}

		return released;
	}

	/**
	 * Creates parser result indicating discovery of a full match in provided
	 * chunk of streamed data.
	 *
	 * @param {Context} context context of stream shared by parsers and handlers, used for sharing with invoked match handler
	 * @param {Buffer} buffer buffer containing (trailing parts of) match
	 * @param {int} atOffset starting offset of currently processed chunk of streamed data, used for sharing with invoked match handler
	 * @param {int} firstIndex index of first octet of found match, might be negative if match has started as partial match before
	 * @param {int} endIndex index of first octet of buffer following found match
	 * @returns {Promise<ParserResult>} promises description of full match as a parser result
	 * @protected
	 */
	signalFullMatch( context, buffer, atOffset, firstIndex, endIndex ) {
		if ( endIndex < firstIndex ) {
			throw new TypeError( "invalid order of indices on indicating match" );
		}

		// release all data preceding reported match
		const released = this.releaseHeldBuffers( buffer, firstIndex );

		// extract buffers with reported match and collect follow-up buffers separately
		let matches = [];
		let unparsed = [...this.heldBuffers];

		if ( endIndex < 1 ) {
			// match is in previously held buffers, only
			// -> extract buffers of reported match and collect in separate list
			let numOctets = endIndex - firstIndex;

			for ( ; numOctets > 0; ) {
				const chunk = unparsed.shift();

				if ( chunk.length > numOctets ) {
					matches.push( chunk.slice( 0, numOctets ) );
					unparsed.unshift( chunk.slice( numOctets ) );
				} else {
					matches.push( chunk );
				}

				numOctets -= chunk.length;
			}

			// current buffer hasn't been parsed yet
			unparsed.push( buffer );
		} else {
			const partial = endIndex < buffer.length;

			// -> reported match maybe start in buffers held back, but ends in current buffer
			matches = this.heldBuffers;
			matches.push( buffer.slice( Math.max( 0, firstIndex ), endIndex ) );

			// put octets following matching data in current chunk into separate buffer
			unparsed = partial ? [buffer.slice( endIndex )] : [];
		}

		// properly expose buffers with octets reported as matching
		this.heldBuffers = matches;

		return this._handleMatch( context, matches, atOffset + firstIndex )
			.then( nextParser => {
				// match has been handled

				// release octets of buffers reported as matching (or whatever
				// invoked match handler has injected into that list of buffers)
				for ( let i = 0, length = matches.length; i < length; i++ ) {
					const current = matches[i];

					if ( current.length > 0 ) {
						released.push( current );
					}
				}

				// inject unparsed buffers for upcoming runs of parser
				this.heldBuffers = [];

				return {
					parsed: released,
					unparsed: Buffer.concat( unparsed ),
					parser: nextParser,
				};
			} );
	}

	/**
	 * Creates parser result indicating partial match at end of provided chunk
	 * of streamed data.
	 *
	 * @param {Buffer} buffer chunk of streamed data containing partial match at its end
	 * @param {number} firstMatchIndex offset of first matching octet in stream of data, relative to beginning of current chunk (thus might be negative)
	 * @returns {Promise<ParserResult>} promises parser result
	 * @protected
	 */
	signalPartialMatch( buffer, firstMatchIndex ) {
		const released = this.releaseHeldBuffers( buffer, firstMatchIndex );

		if ( firstMatchIndex < buffer.length ) {
			this.heldBuffers.push( firstMatchIndex > 0 ? buffer.slice( firstMatchIndex ) : buffer );
		}

		return Promise.resolve( {
			parsed: released
		} );
	}

	/**
	 * Creates parser result indicating lack of any match in provided chunk of
	 * streamed data.
	 *
	 * @param {Buffer} buffer chunk of streamed data lacking any match
	 * @returns {Promise<ParserResult>} promises parser result
	 * @protected
	 */
	signalNoMatch( buffer ) {
		return this.signalPartialMatch( buffer, buffer.length );
	}
}
