/**
 * Implements class managing single step of pattern.
 */
export class PatternStep {
	/**
	 * Renders current step of pattern as code suitable for parsing again.
	 *
	 * @returns {string} rendered code of current pattern step
	 */
	toString() {
		const { values, min, max, negated } = this;
		let quantifier = `${min},${max}`;

		switch ( quantifier ) {
			case "0,1" :
				quantifier = "?";
				break;

			case "1,1" :
				quantifier = "";
				break;

			case "0,Infinity" :
				quantifier = "*";
				break;

			case "1,Infinity" :
				quantifier = "+";
				break;

			default :
				quantifier = `{${quantifier}}`;
		}

		switch ( values.length ) {
			case 0 :
				return `.${quantifier}`;

			case 1 :
				if ( values[0].toString( "utf8" ).length > 1 ) {
					return `(${values[0].toString( "hex" )})${quantifier}`;
				}

				return `${values[0].toString( "utf8" )}${quantifier}`;

			default :
				return `[${negated ? "^" : ""}${values.map( value => value.toString( "utf8" ) ).join( "" )}]${quantifier}`;
		}
	}
}

/**
 * @typedef {object} PatternCursor
 * @property {string} source source enumerated by cursor
 * @property {number} index index into source of cursor's position
 */

/**
 * Implements code for handling patterns describing variable sequences of
 * octets.
 */
export class Pattern {
	/**
	 * Converts pattern source into sequence of pattern elements suitable for
	 * searching.
	 *
	 * @param {string} source source of pattern
	 * @param {BufferEncoding} encoding encoding of pattern source
	 * @returns {Array<PatternStep>} sequence of steps described by pattern source
	 */
	static fromSource( source, encoding = "utf8" ) {
		if ( typeof source !== "string" ) {
			throw new TypeError( "source of pattern must be a string" );
		}

		if ( source.length < 1 ) {
			throw new TypeError( "source of pattern must not be empty" );
		}

		const cursor = { source, index: 0 };
		const steps = [];
		let step;

		for ( ; cursor.index < cursor.source.length; cursor.index++ ) {
			const ch = cursor.source.charAt( cursor.index );

			if ( ch === "\\" && cursor.index < cursor.source.length - 1 ) {
				cursor.index++;
				step = { values: [this.readEscaped( cursor, encoding )], min: 1, max: 1 };
				steps.push( step );
			} else {
				switch ( ch ) {
					case "?" :
					case "*" :
					case "+" :
						if ( step ) {
							step.min = ch === "+" ? 1 : 0;
							step.max = ch === "?" ? 1 : Infinity;
							step = undefined;
						} else {
							throw new TypeError( "invalid quantifier without preceding character" );
						}
						break;

					case "[" :
						step = { ...this.groupFromSource( cursor, encoding ), min: 1, max: 1 };
						steps.push( step );
						break;

					case "(" :
						step = { values: [this.octetSequence( cursor )], min: 1, max: 1 };
						steps.push( step );
						break;

					case "{" :
						if ( step ) {
							const [ min, max ] = this.parseRange( cursor );

							step.min = min;
							step.max = max;
							step = undefined;
						} else {
							throw new TypeError( "invalid range quantifier without preceding character" );
						}
						break;

					case "." :
						step = { values: [], min: 1, max: 1 };
						steps.push( step );
						break;

					default :
						step = { values: [Buffer.from( ch, encoding )], min: 1, max: 1 };
						steps.push( step );
						break;
				}
			}
		}

		for ( let i = 0, n = steps.length; i < n; i++ ) {
			steps[i] = Object.assign( Object.create( PatternStep.prototype ), steps[i] );
		}

		return steps;
	}

	/**
	 * Retrieves buffer according to an escape sequence.
	 *
	 * @param {PatternCursor} cursor cursor into source addressing escaped character
	 * @param {BufferEncoding} encoding encoding for converting characters in source into octets
	 * @returns {Buffer} octets described by found escape sequence
	 */
	static readEscaped( cursor, encoding = "utf8" ) {
		const escaped = cursor.source[cursor.index]; // eslint-disable-line no-param-reassign

		switch ( escaped ) {
			case "x" : {
				const code = /^[0-9a-f]{2}/i.exec( cursor.source.slice( ++cursor.index ) ); // eslint-disable-line no-param-reassign
				if ( !code ) {
					throw new TypeError( "\\x must be followed by two hexadecimal digits" );
				}

				cursor.index++; // eslint-disable-line no-param-reassign
				return Buffer.from( code[0], "hex" );
			}

			case "n" :
				return Buffer.from( [10] );
			case "r" :
				return Buffer.from( [13] );
			case "t" :
				return Buffer.from( [9] );

			default :
				return Buffer.from( escaped, encoding );
		}
	}

	/**
	 * Collects group of characters from provided source.
	 *
	 * @param {PatternCursor} cursor source to compile
	 * @param {BufferEncoding} encoding assumed encoding of octets to search
	 * @returns {{negated: boolean, values: Buffer[]}} compiled group which is its sets of octets and whether it is accepting or rejecting those
	 */
	static groupFromSource( cursor, encoding = "utf8" ) {
		let negated = cursor.source[cursor.index + 1] === "^";
		if ( negated ) {
			cursor.index++; // eslint-disable-line no-param-reassign
		}

		const values = [];
		let start = false;

		for ( cursor.index++; cursor.index < cursor.source.length; cursor.index++ ) { // eslint-disable-line no-param-reassign
			const ch = cursor.source.charAt( cursor.index );

			if ( ch === "\\" && cursor.index < cursor.source.length - 1 ) {
				cursor.index++; // eslint-disable-line no-param-reassign
				const value = this.readEscaped( cursor, encoding ).toString( encoding ).codePointAt( 0 );

				if ( start ) {
					const from = values.pop();
					values.push( [ Math.min( from, value ), Math.max( from, value ) ] );
					start = false;
				} else {
					values.push( value );
				}
			} else {
				switch ( ch ) {
					case "]" : {
						if ( start ) {
							values.push( "-".codePointAt( 0 ) );
						}

						if ( negated && values.length < 1 ) {
							negated = false;
							values.push( "^".codePointAt( 0 ) );
						}

						const codes = {};

						for ( const range of values ) {
							if ( Array.isArray( range ) ) {
								let [ from, to ] = range; // eslint-disable-line prefer-const

								for ( ; from <= to; from++ ) {
									codes[from] = true;
								}
							} else {
								codes[range] = true;
							}
						}

						const unique = Object.keys( codes );
						if ( unique.length < 1 ) {
							throw new TypeError( "group must not be empty" );
						}

						unique.sort( ( l, r ) => parseInt( l ) - parseInt( r ) );

						return {
							negated,
							values: unique.map( code => Buffer.from( String.fromCodePoint( code ), encoding ) )
						};
					}

					case "-" :
						if ( !start && values.length > 0 && cursor.index < cursor.source.length - 1 ) { // eslint-disable-line no-param-reassign
							start = true;
							break;
						}

					// falls through
					default : {
						const value = cursor.source.codePointAt( cursor.index );

						if ( start ) {
							const from = values.pop();
							values.push( [ Math.min( from, value ), Math.max( from, value ) ] );
							start = false;
						} else {
							values.push( value );
						}
					}
				}
			}
		}

		throw new TypeError( "unclosed group of characters" );
	}

	/**
	 * Parses source at current position for definition of a custom range
	 * quantifier.
	 *
	 * @param {PatternCursor} cursor pattern source with current position of cursor
	 * @returns {[number, number]} extracted range
	 */
	static parseRange( cursor ) {
		const tail = cursor.source.slice( cursor.index );
		let min, max;

		let match = /^{?(?:\s*(\d+))?\s*,\s*(?:([1-9]\d*)\s*)?}/g.exec( tail );
		if ( match ) {
			min = match[1] == null ? 0 : parseInt( match[1] );
			max = match[2] == null ? Infinity : parseInt( match[2] );

			if ( min > max ) {
				throw new TypeError( "invalid order of ends in range quantifier" );
			}
		} else {
			match = /^{?\s*([1-9]\d*)\s*}/g.exec( tail );
			if ( match ) {
				min = max = parseInt( match[1] );
			} else {
				throw new TypeError( "invalid range quantifier" );
			}
		}

		cursor.index += match[0].length - 1; // eslint-disable-line no-param-reassign

		return [ min, max ];
	}

	/**
	 * Extracts sequence of octets described by a sequence of hexadecimal
	 * digits.
	 *
	 * @param {PatternCursor} cursor pattern source with cursor pointing at opening parenthesis
	 * @returns {Buffer} extracted sequence of octets
	 */
	static octetSequence( cursor ) {
		if ( cursor.source[cursor.index] !== "(" ) {
			throw new TypeError( "cursor does not point at opening parenthesis" );
		}

		const end = cursor.source.indexOf( ")", cursor.index + 1 );
		if ( end < 0 ) {
			throw new TypeError( "unfinished sequence of octets" );
		}

		const codes = cursor.source.slice( cursor.index + 1, end );
		if ( codes.length < 1 || codes.length % 2 !== 0 ) {
			throw new TypeError( "invalid length of sequence of octets" );
		}

		if ( /[^0-9a-f]/i.test( codes ) ) {
			throw new TypeError( "invalid character in sequence of octets" );
		}

		cursor.index = end; // eslint-disable-line no-param-reassign

		return Buffer.from( codes, "hex" );
	}
}
