import { Readable, Writable } from "stream";


/**
 * Streams into buffer promising all streamed data when finished.
 *
 * @property {Promise<Buffer>} asPromise promises single buffer containing all received data when stream finished
 */
export class BufferWriteStream extends Writable {
	/**
	 * @param {object} options options passed to underlying `Writable`
	 */
	constructor( options = {} ) {
		super( { ...options, objectMode: false, decodeStrings: false } );

		this._buffers = [];

		Object.defineProperty( this, "asPromise", {
			value: new Promise( ( resolve, reject ) => {
				this.on( "finish", () => resolve( Buffer.concat( this._buffers ) ) );
				this.on( "error", reject );
			} )
		} );
	}

	/**
	 * Processes chunk of data written into stream.
	 *
	 * @param {string|Buffer} chunk chunk of written data
	 * @param {BufferEncoding} encoding optional encoding when provided data is a string
	 * @param {function(error:Error)} doneFn callback to invoke when finished processing written data
	 * @returns {void}
	 * @private
	 */
	_write( chunk, encoding, doneFn ) {
		const _chunk = typeof chunk === "string" ? Buffer.from( chunk, encoding ) : chunk;

		this._buffers.push( _chunk );
		doneFn();
	}
}

/**
 * Streams from buffer in chunks of definable size.
 */
export class BufferReadStream extends Readable {
	/**
	 * @param {Buffer} buffer single buffer to be streamed from.
	 * @param {int} chunkSize limits number of octets to deliver per chunk
	 * @param {int} delay milliseconds to wait before delivering another sole chunk, omit for delivering chunks w/o delay in a row
	 * @param {object} options additional stream options passed to super-ordinated `Readable`
	 */
	constructor( buffer, chunkSize = 256, delay = 1, options = {} ) {
		super( { ...options, objectMode: false } );

		this._buffer = typeof buffer === "string" ? Buffer.from( buffer, "utf8" ) : buffer;
		this._size = chunkSize;
		this._delay = delay;
		this._offset = 0;

		if ( !Buffer.isBuffer( this._buffer ) ) {
			throw new TypeError( "invalid buffer for feeding stream from" );
		}
	}

	/**
	 * Streams chunk(s) from buffer.
	 *
	 * @returns {void}
	 * @private
	 */
	_read() {
		while ( this._offset < this._buffer.length ) {
			const start = this._offset;
			const end = start + this._size;

			const slice = this._buffer.slice( start, end );
			this._offset = end;

			if ( this._delay > 1 ) {
				setTimeout( () => {
					this.push( slice );
				}, this._delay );

				return;
			}

			if ( this._delay > 0 ) {
				process.nextTick( () => {
					this.push( slice );
				} );

				return;
			}

			if ( this.push( slice ) === false ) {
				return;
			}
		}

		// met EOB
		this.push( null );
	}
}
