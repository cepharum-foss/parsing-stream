import { CollectingParser } from "./collecting.mjs";
import { SubstringParser } from "./substring.mjs";
import { Writable } from "stream";

/**
 * Implements parser searching stream for a particular sequence of octets
 * collecting all octets preceding a match.
 */
export class CollectingSubstringParser extends CollectingParser {
	/**
	 * @param {Buffer} needle buffer describing sequence of octets to search for
	 * @param {CollectingParserOptions} options parser customizations
	 */
	constructor( needle, options = {} ) {
		super( options );

		SubstringParser.prepare( this, needle );
	}

	/** @inheritDoc */
	resetState() {
		return SubstringParser.resetState( this );
	}

	/** @inheritDoc */
	async parse( context, buffer, atOffset, isLast ) {
		const result = await SubstringParser.findSubstring( this, context, buffer, atOffset );

		if ( isLast && !result.unparsed?.length && this.collected instanceof Writable ) {
			this.collected.end();
		}

		return result;
	}
}
