import { Parser } from "../parser.mjs";


/**
 * Implements parser searching stream for a particular sequence of octets.
 */
export class SubstringParser extends Parser {
	/**
	 * @param {Buffer} needle buffer describing sequence of octets to search for
	 * @param {ParserOptions} options customizations for parser instance
	 */
	constructor( needle, options = null ) {
		super( options );

		SubstringParser.prepare( this, needle );
	}

	/**
	 * Parses provided buffer for containing data matching expectations.
	 *
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	parse( context, buffer, atOffset ) {
		return this.constructor.findSubstring( this, context, buffer, atOffset );
	}

	/**
	 * Prepares parser to search for sequence of octets matching provided
	 * needle.
	 *
	 * @param {Parser} parser parser to expose provided needle
	 * @param {Buffer} needle needle to validate and expose for searching
	 * @returns {void}
	 */
	static prepare( parser, needle ) {
		const _needle = typeof needle === "string" ? Buffer.from( needle, "utf8" ) : needle;

		if ( !Buffer.isBuffer( _needle ) || _needle.length < 1 ) {
			throw new TypeError( "invalid buffer to search for" );
		}

		Object.defineProperty( parser, "needle", { value: _needle } );
	}

	/** @inheritDoc */
	static resetState( parser ) {
		parser.state = { // eslint-disable-line no-param-reassign
			index: 0,
		};
	}

	/**
	 * Implements core capability of SubstringParser for re-use in other types
	 * of parsers (mostly required due to lack of multi-inheritance support).
	 *
	 * This method is a relocated implementation for `Parser#parse()`. It has
	 * been relocated to simplify more complex derivations relying on multi
	 * inheritance such as CollectingSubstringParser.
	 *
	 * @param {Parser} parser instance of parser using this helper function
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	static findSubstring( parser, context, buffer, atOffset ) {
		let ref = 0;
		let chunk = buffer;
		let chunkSize = buffer.length;
		let bailedBefore = false;

		const state = parser.state;
		const needle = parser.needle;
		const needleSize = needle.length;
		let matchIndex = state.index;

		for ( let read = ref; read < chunkSize; read++ ) {
			if ( chunk[read] === needle[matchIndex] ) {
				matchIndex++;

				if ( matchIndex >= needleSize ) {
					state.index = 0; // eslint-disable-line no-param-reassign

					const firstIndex = read - ref - matchIndex + 1;

					return parser.signalFullMatch( context, buffer, atOffset, firstIndex, read - ref + 1 );
				}
			} else {
				if ( !bailedBefore ) {
					// have to bail -> recover all previously matched octets
					bailedBefore = true;

					ref = parser.previouslyMatchingOctetsCount;
					chunk = Buffer.concat( [ ...parser.previouslyMatchingChunks, buffer ] );
					chunkSize = chunk.length;
					read += ref;
				}

				read -= matchIndex;
				matchIndex = 0;
			}
		}

		state.index = matchIndex; // eslint-disable-line no-param-reassign

		return parser.signalPartialMatch( buffer, buffer.length - matchIndex );
	}
}
