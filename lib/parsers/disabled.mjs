import { Parser } from "../parser.mjs";


/**
 * Implements parser for explicitly disabling any parsing for the rest of all
 * streamed data.
 */
export class DisabledParser extends Parser {
	/**
	 * Parses provided buffer for containing data matching expectations.
	 *
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	parse( context, buffer ) {
		return this.signalNoMatch( buffer );
	}
}
