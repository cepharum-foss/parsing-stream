/* eslint-disable max-depth */

import { Parser } from "../parser.mjs";
import { Pattern } from "../pattern.mjs";

/**
 * Implements parser searching stream for a particular sequence of octets
 * matching defined pattern.
 *
 * @property {{stack: PatternParserState[]}} state state of pattern parser
 * @property {PatternStep[]} pattern list of pattern steps to search for
 */
export class PatternParser extends Parser {
	/**
	 * @param {string} pattern source of pattern to search for
	 * @param {ParserOptions} options customizations for parser instance
	 */
	constructor( pattern, options = null ) {
		super( options );

		PatternParser.prepare( this, pattern );
	}

	/**
	 * Parses provided buffer for containing data matching expectations.
	 *
	 * @param {Context} context reference on context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed, might be empty
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @param {boolean} isLast true if this chunk is last one of stream to parse
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	parse( context, buffer, atOffset, isLast ) {
		return this.constructor.findPattern( this, context, buffer, atOffset, isLast );
	}

	/**
	 * Prepares parser for searching sequence of octets matching provided
	 * pattern.
	 *
	 * @param {Parser} parser some parser to prepare
	 * @param {string } pattern source of pattern to search for
	 * @returns {void}
	 */
	static prepare( parser, pattern ) {
		Object.defineProperties( parser, {
			pattern: { value: Pattern.fromSource( pattern ) },
		} );
	}

	/** @inheritDoc */
	static resetState( parser ) {
		parser.state = { // eslint-disable-line no-param-reassign
			stack: [{ step: 0, value: 0, octet: 0, sizes: [], cursor: 0 }],
		};
	}

	/**
	 * Implements core capability of PatternParser for re-use in other types
	 * of parsers (mostly required due to lack of multi-inheritance support).
	 *
	 * This method is a relocated implementation for `PatternParser#parse()`. It
	 * has been relocated to simplify more complex derivations relying on multi
	 * inheritance such as CollectingPatternParser.
	 *
	 * @param {PatternParser} parser instance of parser using this helper function
	 * @param {Context} context context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @param {boolean} isLast true if provided chunk is last one of stream to parse
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	static findPattern( parser, context, buffer, atOffset, isLast ) {
		const { state: { stack }, pattern } = parser;

		const fullChunk = Buffer.concat( [ ...parser.previouslyMatchingChunks, buffer ] );
		const heldBackOctetCount = parser.previouslyMatchingOctetsCount;
		const fullSize = fullChunk.length;
		let state = stack[0];
		let skipOffset = 0;
		let stopAt = fullSize - skipOffset;

		for ( ; ( isLast && stack.length > 1 ? state.cursor <= stopAt : state.cursor < stopAt ) && state.cursor > -1; ) {
			const { values, negated } = pattern[state.step];
			let valueMatches = false;

			// test octet to match expected one
			const octets = values[state.value];
			const octetMatches = state.cursor >= stopAt ? false : values.length > 0 ? fullChunk[skipOffset + state.cursor] === octets[state.octet] : true;
			let bail = false;

			if ( octetMatches ) {
				// advance indices
				state.cursor++;
				state.octet++;

				if ( !values.length || state.octet >= octets.length ) {
					// whole value has matched
					valueMatches = true;

					if ( negated ) {
						bail = true;
					}
				}
			} else {
				bail = true;
			}

			if ( bail ) {
				// value has failed

				// -> returning to state prior to checking current value
				state.cursor -= state.octet;
				state.octet = 0;

				if ( valueMatches && negated ) {
					// current value matched unexpectedly
					// -> skip all further values of this step to be rejected as well
					state.value = values.length;
				} else {
					// -> try next value of current step
					state.value++;

					if ( state.value >= values.length && negated ) {
						// all listed values failed, but test is negated

						// -> so, the first octet of current step is a match
						bail = false;
						valueMatches = true;

						// FIXME matching single octet this way isn't UTF8 safe
						state.cursor++;
						state.octet++;
					}
				}
			}

			if ( bail ) {
				// eslint-disable-next-line max-len
				// console.log(`--- ${stack.length}: #${state.value} of /${pattern[state.step]}/ at #${state.cursor} of '${fullChunk.slice(skipOffset, skipOffset+state.cursor+1).toString()}'`);

				for ( let iteration = 0, bailAgain = true; bailAgain; iteration++ ) {
					bailAgain = false;

					if ( state.value >= pattern[state.step].values.length ) {
						// met end of accepted values in current step
						// console.log("->| met last value of step", iteration);

						if ( iteration > 0 || state.sizes.length < pattern[state.step].min ) {
							// step is not satisfied yet -> step failed
							// console.log("    step failed");

							// -> recover state of most recent match ...
							if ( stack.length > 0 ) {
								// eslint-disable-next-line max-len
								// console.log("    tracking back", { ...state, sizes: state.sizes.join( "," ) }, "->", stack && stack[1] && { ...stack[1], sizes: stack[1].sizes.join( "," ) } );

								stack.shift();
								state = stack[0];
								bailAgain = Boolean( state );

								// ... revert it ...
								if ( state ) {
									state.cursor -= state.octet;
									state.octet = 0;

									if ( state.value < pattern[state.step].values.length - 1 ) {
										// ... and try next value of same step
										state.value++;
									} else if ( state.sizes.length > 0 && state.sizes.length > pattern[state.step].min ) {
										// ... and try matching follow-up step a single match earlier
										state.cursor -= state.sizes.shift();

										// re-save adjusted previous state
										stack.unshift( state = { ...state } );

										// advance to next step of pattern
										state.step++;
										state.value = 0;
										state.sizes = [];
									} else {
										// ...and cause testing next step
										state.value++;
									}
								}
							}
						} else {
							// current step is fine

							// save this state for returning while backtracking later
							// console.log( "    saving", { ...state, sizes: state.sizes.join( "," ) } )
							stack.unshift( state = { ...state } );

							// -> try next step instead
							state.step++;
							state.value = 0;
							state.sizes = [];
						}
					}
				}

				if ( stack.length < 1 ) {
					// no pattern starts at octet recently considered first one
					// -> skip in future and stop holding it back any further
					skipOffset++;
					stopAt--;

					stack.unshift( state = { step: 0, value: 0, octet: 0, sizes: [], cursor: 0 } );
				}
			} else if ( valueMatches ) {
				// fully matching current value as expected
				// eslint-disable-next-line max-len
				// console.log(`+++ ${stack.length}: #${state.value} /${pattern[state.step]}/ at #${state.cursor - 1} of '${fullChunk.slice(skipOffset, skipOffset+state.cursor).toString()}'`);

				// -> track this value's number of matching octets
				state.sizes.push( state.octet );
				state.octet = 0;

				// -> save this state for returning while backtracking later
				// console.log( "    saving", { ...state, sizes: state.sizes.join( "," ) } )
				stack.unshift( state = { ...state, ...{ sizes: [...state.sizes] } } );

				// -> prepare for matching another occurrence of current pattern step
				state.value = 0;

				if ( state.sizes.length >= pattern[state.step].max ) {
					// satisfied current step as much as possible
					// -> try next step
					state.step++;
					state.sizes = [];
				}
			}

			if ( state.step >= pattern.length ) {
				// passed all steps of pattern -> we've got a match
				// console.log( `!!! MATCH ${skipOffset} + ${state.cursor} '${fullChunk.slice( skipOffset, skipOffset + state.cursor ).toString()}'` );

				// -> drop stack
				stack.splice( 0, stack.length, { step: 0, value: 0, octet: 0, sizes: [], cursor: 0 } );

				// -> report the match
				const startIndex = skipOffset - heldBackOctetCount;
				return parser.signalFullMatch( context, buffer, atOffset, startIndex, startIndex + state.cursor );
			}
		}

		if ( isLast ) {
			// console.log( "FLUSH", { ...state, sizes: state.sizes.join( "," ) } );
			const numSteps = pattern.length;
			let i = state.step;

			for ( ; i < numSteps; i++ ) {
				const isFollowUp = i > state.step;
				const step = pattern[i];
				const count = isFollowUp ? 0 : state.sizes.length;
				const octet = isFollowUp ? 0 : state.octet;

				if ( count < step.min || count > step.max || octet > 0 ) {
					// current step of pattern isn't satisfied
					// -> not a match
					break;
				}
			}

			if ( i === numSteps ) {
				// console.log( "!!F MATCH", { ...state, sizes: state.sizes.join( "," ) } );
				// -> drop stack
				stack.splice( 0, stack.length, { step: 0, value: 0, octet: 0, sizes: [], cursor: 0 } );

				// -> report the match
				const startIndex = skipOffset - heldBackOctetCount;
				return parser.signalFullMatch( context, buffer, atOffset, startIndex, startIndex + state.cursor );
			}

			return parser.signalPartialMatch( buffer, buffer.length );
		}

		return parser.signalPartialMatch( buffer, skipOffset - heldBackOctetCount );
	}
}
