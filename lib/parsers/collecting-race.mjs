import { CollectingParser } from "./collecting.mjs";
import { RaceParser } from "./race.mjs";
import { Writable } from "stream";

/**
 * Implements parser searching stream for another match using a set of attached
 * parsers while collecting all seen octets locally.
 */
export class CollectingRaceParser extends CollectingParser {
	/**
	 * @param {Parser[]} parsers list of parsers racing for another match
	 * @param {CollectingParserOptions} options parser customizations
	 */
	constructor( parsers, options = {} ) {
		super( options );

		RaceParser.prepare( this, parsers );
	}

	/** @inheritDoc */
	async parse( context, buffer, atOffset, isLast ) {
		const result = await RaceParser.race( this, context, buffer, atOffset, isLast );

		if ( isLast && !result.unparsed?.length && this.collected instanceof Writable ) {
			this.collected.end();
		}

		return result;
	}
}
