import { PassThrough, Writable } from "stream";

import { Parser } from "../parser.mjs";

/**
 * Implements non-matching parser collecting all data dedicated for release
 * otherwise.
 *
 * @note This parser is incomplete! You still need to derive it and override its
 *       parse() method.
 *
 * @property {Readable} collected streams non-matching octets
 * @property {boolean} tee true if non-matching octets shall be streamed via `collected` in addition to parsed stream, false to push octets on `collected`, only
 * @abstract
 */
export class CollectingParser extends Parser {
	/**
	 * @param {CollectingParserOptions} options customizations for parser instance
	 */
	constructor( options = null ) {
		super( options );

		this.tee = Boolean( options && options.hasOwnProperty( "tee" ) ? options.tee : true );

		this._switchCollectionOnAttach = false;

		this.switchCollected();
	}

	/** @inheritDoc */
	collectForwarded( collector, buffer ) {
		this.collected.write( buffer );

		if ( this.tee ) {
			collector.push( buffer );
		}
	}

	/** @inheritDoc */
	onAttach( stream ) {
		super.onAttach( stream );

		if ( this._switchCollectionOnAttach ) {
			this._switchCollectionOnAttach = false;

			this.collected = new PassThrough( { objectMode: false } );
		}
	}

	/** @inheritDoc */
	onDetach() {
		super.onDetach();

		this._switchCollectionOnAttach = true;

		if ( this.collected instanceof Writable ) {
			this.collected.end();
		}
	}

	/**
	 * Closes current stream collecting non-matching octets and replaces it with
	 * a new one.
	 *
	 * @returns {CollectingParser} fluid interface
	 */
	switchCollected() {
		if ( this.collected instanceof Writable ) {
			this.collected.end();
		}

		this.collected = new PassThrough( { objectMode: false } );

		this._switchCollectionOnAttach = false;

		return this;
	}
}
