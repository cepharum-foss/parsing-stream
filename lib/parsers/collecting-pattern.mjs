import { CollectingParser } from "./collecting.mjs";
import { PatternParser } from "./pattern.mjs";
import { Writable } from "stream";

/**
 * Implements parser searching stream for a particular sequence of octets
 * collecting all octets preceding a match.
 */
export class CollectingPatternParser extends CollectingParser {
	/**
	 * @param {string} pattern source of pattern
	 * @param {CollectingParserOptions} options parser customizations
	 */
	constructor( pattern, options = {} ) {
		super( options );

		PatternParser.prepare( this, pattern );
	}

	/** @inheritDoc */
	resetState() {
		return PatternParser.resetState( this );
	}

	/** @inheritDoc */
	async parse( context, buffer, atOffset, isLast ) {
		const result = await PatternParser.findPattern( this, context, buffer, atOffset, isLast );

		if ( isLast && !result.unparsed?.length && this.collected instanceof Writable ) {
			this.collected.end();
		}

		return result;
	}
}
