import { Parser } from "../parser.mjs";
import { CollectingParser } from "./collecting.mjs";

/**
 * Implements parser searching stream for matches of one or more attached
 * parsers. Whenever one of the listed parsers is matching, it is selected
 * to handle the match and all other parsers are reset. If RaceParser itself
 * has a match handler, it is used in preference over either matching
 * parser's handler and that matching parser is provided as an additional
 * argument.
 *
 * @property {Parser[]} parsers list of racing parsers
 */
export class RaceParser extends Parser {
	/**
	 * @param {Parser[]} parsers list of racing parsers
	 * @param {ParserOptions} options customizations for parser instance
	 */
	constructor( parsers, options = null ) {
		super( options );

		this.constructor.prepare( this, parsers );
	}

	/** @inheritDoc */
	parse( context, buffer, atOffset, isLast ) {
		return this.constructor.race( this, context, buffer, atOffset, isLast );
	}

	/**
	 * Prepares provided parser to combine multiple subordinated parser racing
	 * for next match.
	 *
	 * @param {Parser} raceParser parser to prepare
	 * @param {Parser[]} parsers list of parsers to attach
	 * @return {void}
	 */
	static prepare( raceParser, parsers ) {
		if ( !Array.isArray( parsers ) ) {
			throw new TypeError( "invalid list of racing parsers" );
		}

		if ( !parsers.length ) {
			throw new TypeError( "invalid empty list of racing parsers" );
		}

		if ( !parsers.every( parser => parser instanceof Parser && !( parser instanceof RaceParser ) && !( parser instanceof CollectingParser ) ) ) {
			throw new TypeError( "invalid items in list of racing parsers" );
		}

		Object.defineProperties( raceParser, {
			parsers: { value: parsers, enumerable: true },
		} );

		Object.defineProperties( raceParser, {
			/**
			 * Implements proxy handlers for controlling runtime context of single parser in
			 * current race while searching another chunk of data for a match.
			 */
			parseProxy: { value: {
				get: ( target, name, receiver ) => {
					switch ( name ) {
						case "signalFullMatch" :
						case "signalPartialMatch" :
						case "signalNoMatch" :
							return function( ...args ) {
								target.latestMatch = { name, args }; // eslint-disable-line no-param-reassign
							};

						case "heldBuffers" :
							return raceParser.heldBuffers;

						default :
							return Reflect.get( target, name, receiver );
					}
				},

				set: ( target, name, value, receiver ) => {
					switch ( name ) {
						case "heldBuffers" :
							throw new Error( "parser participating in a race must not replace held buffers but has to rely on signal*Match-methods" );

						default :
							return Reflect.set( target, name, value, receiver );
					}
				},
			} },

			/**
			 * Implements proxy handlers for controlling runtime context of single parser in
			 * current race winning over its competitors for signalling what data can be
			 * released and what data must be held back.
			 */
			signalProxy: { value: {
				get: ( target, name, receiver ) => {
					switch ( name ) {
						case "heldBuffers" :
							return raceParser.heldBuffers;

						case "onMatch" :
							if ( raceParser.onMatch ) {
								return ( ...args ) => raceParser.onMatch( ...args, target );
							}

							return Reflect.get( target, name, receiver );

						case "collectForwarded" :
							if ( raceParser instanceof CollectingParser ) {
								return ( ...args ) => raceParser.collectForwarded( ...args );
							}
							// falls through

						default :
							return Reflect.get( target, name, receiver );
					}
				},

				set: ( target, name, value, receiver ) => {
					switch ( name ) {
						case "heldBuffers" :
							raceParser.heldBuffers = value; // eslint-disable-line no-param-reassign
							return true;

						default :
							return Reflect.set( target, name, value, receiver );
					}
				},
			} }
		} );
	}

	/**
	 * Implements core capability of SubstringParser for re-use in other types
	 * of parsers (mostly required due to lack of multi-inheritance support).
	 *
	 * This method is a relocated implementation for `RaceParser#parse()`. It
	 * has been relocated to simplify more complex derivations relying on multi
	 * inheritance such as CollectingRaceParser.
	 *
	 * @param {RaceParser} raceParser instance of parser using this helper function
	 * @param {Context} context context for sharing results of parsing
	 * @param {Buffer} buffer slice of data to be parsed
	 * @param {int} atOffset provides offset of provided buffer in context of a larger file or stream
	 * @param {boolean} isLast true if provided chunk is last one of stream to parse
	 * @returns {Promise<ParserResult>} promises result of parsing
	 */
	static async race( raceParser, context, buffer, atOffset, isLast ) {
		const { parsers } = raceParser;
		const numParsers = parsers.length;

		if ( numParsers < 1 ) {
			throw new Error( "empty list of racing parsers" );
		}

		// try every parser
		for ( let i = 0; i < numParsers; i++ ) {
			const parser = parsers[i];

			if ( !( parser instanceof Parser ) || parser instanceof RaceParser || parser instanceof CollectingParser ) {
				throw new Error( "invalid parser" );
			}

			await new Proxy( parser, raceParser.parseProxy ).parse( context, buffer, atOffset, isLast ); // eslint-disable-line no-await-in-loop
		}

		// find parser winning this stage of race
		// - use any full match, prefer the one with earliest start index
		// - otherwise use partial match holding back most number of octets
		const full = { start: Infinity, end: Infinity };
		const partial = { start: Infinity };

		for ( let i = 0; i < numParsers; i++ ) {
			const parser = parsers[i];
			const match = parser.latestMatch;
			const { name, args } = match;

			switch ( name ) {
				case "signalFullMatch" : {
					const start = atOffset + args[3];
					const end = atOffset + args[4];

					if ( end < full.end || ( end === full.end && start < full.start ) ) {
						full.start = start;
						full.end = end;
						full.parser = parser;
						full.match = match;
					}
					break;
				}

				case "signalPartialMatch" :
					if ( atOffset + args[1] < partial.start ) {
						partial.start = atOffset + args[1];
						partial.parser = parser;
						partial.match = match;
					}
					break;

				case "signalNoMatch" :
					if ( atOffset + args[0].length < partial.start ) {
						partial.start = atOffset + args[0].length;
						partial.parser = parser;
						partial.match = match;
					}
					break;
			}
		}

		// found full?
		if ( full.start < Infinity ) {
			for ( let i = 0; i < numParsers; i++ ) {
				const parser = parsers[i];

				if ( parser !== full.parser ) {
					parser.resetState();
				}
			}

			return new Proxy( full.parser, raceParser.signalProxy ).signalFullMatch( ...full.match.args );
		}

		// signal partial/no match
		return new Proxy( partial.parser, raceParser.signalProxy )[partial.match.name]( ...partial.match.args );
	}
}
