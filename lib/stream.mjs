import { Transform } from "stream";

import { Context } from "./context.mjs";
import { Parser } from "./parser.mjs";

/**
 * Implements Node.js transform stream for searching sequence of octets passing
 * through using attachable parser instances.
 *
 * @property {Context} context context of stream, shared by attached parsers and any of their methods called for processing streamed data
 * @property {?Parser} parser currently attached parser instance
 * @property {Promise<Context>} asPromise promises stream's context after processing all streamed data, is rejected on stream error
 */
export class ParsingStream extends Transform {
	/**
	 * @param {object} options options passed to underlying `Transform` stream
	 */
	constructor( options = {} ) {
		if ( options.objectMode ) {
			throw new TypeError( "object-mode streams not supported" );
		}

		if ( options.context && !( options.context instanceof Context ) ) {
			throw new TypeError( "invalid context" );
		}

		options.decodeStrings = false; // eslint-disable-line no-param-reassign

		super( options );

		let parser = null;

		this._offset = 0;

		Object.defineProperties( this, {
			context: { value: options.context || new Context() },
			parser: {
				get: () => parser,
				set: newParser => {
					if ( newParser && !( newParser instanceof Parser ) ) {
						throw new TypeError( "invalid parser" );
					}

					if ( parser ) {
						parser.onDetach();
					}

					parser = newParser || undefined;

					if ( parser ) {
						parser.onAttach( this );
					}
				}
			},
			asPromise: {
				value: new Promise( ( resolve, reject ) => {
					this.on( "finish", () => resolve( this.context ) );
					this.on( "error", reject );
				} ),
			},
		} );

		if ( options.initialParser ) {
			this.parser = options.initialParser;
		}
	}

	/**
	 * Provides further data to be read from current stream.
	 *
	 * @param {function(error:Error=)} doneFn callback invoked when data has been flushed
	 * @returns {void}
	 * @protected
	 */
	_flush( doneFn ) {
		if ( this.parser ) {
			this._transform( null, null, error => {
				if ( error ) {
					doneFn( error );
				} else {
					this.push( this.parser.data );

					doneFn();
				}
			} );
		} else {
			doneFn();
		}
	}

	/**
	 * Receives another chunk of data for parsing and optionally transforming.
	 *
	 * @param {Buffer|string} chunk chunk of data to transform
	 * @param {BufferEncoding} encoding optional encoding of data provided as string
	 * @param {function(error:Error=)} doneFn callback invoked when data has been transformed
	 * @returns {void}
	 * @private
	 */
	_transform( chunk, encoding, doneFn ) {
		const isLast = chunk == null; // see _flush() above
		const _chunk = isLast ? Buffer.from( [] ) : typeof chunk === "string" ? Buffer.from( chunk, encoding ) : chunk;

		// process this chunk by trying to parse as much as possible before
		// passing it to reading side of current stream
		processor( this, _chunk );


		/**
		 * Tries to parse (another) (part of) received chunk data.
		 *
		 * @param {ParsingStream} stream parsing stream
		 * @param {Buffer} buffer buffer to inspect
		 * @returns {void}
		 */
		function processor( stream, buffer ) {
			let promise;

			// ask any current parser to parse as much data as possible
			const parser = stream.parser;
			if ( parser ) {
				try {
					promise = parser.parse( stream.context, buffer, stream._offset, isLast );
				} catch ( cause ) {
					doneFn( cause );
					return;
				}

				if ( !( promise instanceof Promise ) ) {
					if ( !promise ) {
						doneFn( new Error( "attached parser must describe result of parsing chunk of data" ) );
						return;
					}

					promise = Promise.resolve( promise );
				}
			} else {
				promise = Promise.resolve( { parsed: [buffer] } );
			}

			// handle response from parser
			promise.then( ( { parsed, unparsed, parser: nextParser } ) => {
				/*
				 * #1: push any parsed data to reading side of stream
				 */
				if ( parsed && parsed.length ) {
					for ( let i = 0, length = parsed.length; i < length; i++ ) {
						stream.push( parsed[i] );
					}
				}


				/*
				 * #2: optionally switch parser
				 */
				if ( nextParser ) {
					stream.parser = nextParser; // eslint-disable-line no-param-reassign
				}


				/*
				 * #3: restart processing any data left unparsed by now
				 */
				stream._offset += buffer.length; // eslint-disable-line no-param-reassign

				if ( unparsed && unparsed.length ) {
					stream._offset -= unparsed.length; // eslint-disable-line no-param-reassign

					process.nextTick( processor, stream, unparsed );
				} else {
					if ( isLast && stream.parser ) {
						stream.parser.onDetach();
					}

					doneFn();
				}
			} )
				.catch( doneFn );
		}
	}
}
