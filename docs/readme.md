---
home: true
heroText: parsing-stream 
tagline: Searching huge data on-the-fly ...
actionText: Get started
actionLink: guide.html
features:
- title: Native Javascript
  details: This package is based on Node.js core APIs, only. It doesn't even depend on other packages.
- title: Well tested
  details: The package has been thoroughly unit-tested. It's test coverage is kept close to 100%.
- title: Pattern-based search
  details: In addition to regular substring search, octet streams can be searched using patterns resembling regular expressions. 
footer: MIT Licensed | © 2021 cepharum GmbH
---

[parsing-stream](https://www.npmjs.com/package/parsing-stream) provides an octet-mode transform stream supporting attachment of a parser which is observing streamed data for matching certain criteria triggering callback function implementing custom actions such as collecting or replacing matches, extracting information, switching parsers, redirecting data to different streams etc.
