# Quick start

## Installation

The package is installed the usual way. In your project run

```bash
npm install parsing-stream
```

## Simple example

```javascript
import { createReadStream } from "fs";
import { Stream, SubstringParser } from "parsing-stream";

const parser = new SubstringParser( "needle", {
    onMatch( context, buffers, offset ) {
        buffers.splice( 0, buffers.length, Buffer.from( "found" ) );
    }
} );

const filter = new Stream( { initialParser: parser } );

process.stdin.pipe( filter ).pipe( process.stdout );
```

This example is reading data from stdin, replacing all occurrences of `needle` with `found` prior to writing all data to stdout.

## Search for pattern

```javascript
import { createReadStream } from "fs";
import { Stream, PatternParser } from "parsing-stream";

const parser = new PatternParser( "[nN]e+dle" );
const filter = new Stream( { initialParser: parser } );

parser.onMatch = ( context, buffers, offset ) => {
    buffers.splice( 0, buffers.length, Buffer.from( "found" ) );
};

process.stdin.pipe( filter ).pipe( process.stdout );
```

This time, the parser is searching for a pattern `[Nn]e+dle` which is resembling a regular expression by intention. However, [don't expect too much here](api/pattern.html#syntax). This isn't a regular expression as commonly supported by Javascript.

## Process markup

```javascript
import { createReadStream } from "fs";
import { Stream, SubstringParser, CollectingSubstringParser, BufferStream } from "parsing-stream";

const start = new SubstringParser( "{{" );
const stop = new CollectingSubstringParser( "}}", { tee: false } );
const filter = new Stream( { initialParser: start } );

start.onMatch = ( context, buffers, offset ) => {
    buffers.splice( 0 );
    
    context.markup = end.collected.pipe( new BufferStream.Writer() );
    
    return end;
};

end.onMatch = async ( context, buffers, offset ) => {
    end.switchCollected();
    
    const markup = await context.markup.asPromise;

    switch ( markup.toString().trim() ) {
        case "name" :
            buffers.splice( 0, buffers.length, Buffer.from( "John Doe" ) );
            break;
        
        default :
            buffers.splice( 0 );
    }
    
    return start;
};

process.stdin.pipe( filter ).pipe( process.stdout );
```

This example is looking for special markup wrapped in pairs of double curly braces like

::: v-pre
`{{ name }}`
:::

It relies on two separate parsers `start` and `end`: 

* `start` is a simple substring parser looking for the opening braces `{{`. On matching the second parser `end` is attached to the stream thus replacing `start`. In addition, those curly braces are dropped to suppress them in resulting output. 

* `end` is a slightly more complex variant of a substring parser. Just like `start`, it is searching for another substring. In addition, it is _exclusively_ writing all non-matching octets preceding the match to a separate stream `collected`. 

  The separately collected data is piped into a helper capturing all streamed data in a buffer, eventually. 

  On matching closing braces `}}`, that buffer is read and used to lookup value for the name found between the `{{` and `}}`. That value is used to replace `}}` in resulting output. Because of collecting in-between content exclusively, this eventually replaces the whole marker.

  Finally, the first parser is re-attached to the stream to look for another occurrence of `{{`.

This code would convert a streamed text like

```
Hello {{ name }}, this is a parsing stream at work.
```

into

```
Hello John Doe, this is a parsing stream at work.
```
