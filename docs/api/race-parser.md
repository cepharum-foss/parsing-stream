# RaceParser

The `RaceParser` class combines one or more parsers to race for a match. 

Chunks of stream are presented to either listed parser. The first one signalling a full match is causing this race parser to invoke its match handler (or the match handler of signalling parser if there is no match handler on the race parser itself). After that, the race restarts.

Any parser but another race parser or some collecting parser can participate in a race. 

There is a [collecting variant of this parser](collecting-race-parser.html) in case you need to collect all octets of stream seen before matching.

## Construction

**Signature:** `new RaceParser( parsers, options? )`

* `parsers` is a list of parsers to be combined in racing for a match.
* `options` is customizing the parser. It is basically identical to [constructor options of abstract `Parser`](parser.html#constructor)

## Methods

### onMatch()

You need to implement a custom match handler either by assigning one to any created instance explicitly or by inheriting another class with a proper match handler from this class.

See [related documentation for `Parser` class](parser.html#onmatch).

:::tip And the winner is ...  
In opposition to match handlers of most other parsers, this one is receiving the matching parser in another argument.  
:::
