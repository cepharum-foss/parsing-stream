# DisabledParser

The `DisabledParser` class is a custom parser based on [`Parser` class](parser.html). It is meant to never match any octet. Attach an instance of it to the stream to improve performance after having found whatever you have been interested in.

:::warning Be aware!
This parser never matches any other data thus it is never going to invoke some - in this case useless - custom `onMatch()` handler.
:::
