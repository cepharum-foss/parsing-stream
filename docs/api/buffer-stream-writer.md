# BufferStream.Writer

This helper class is a [writable stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_writable_streams) collecting chunks of octets delivered via the stream over time.

It is extensively used in unit-testing this package. Apart from that, it is of no special use for the parsing-stream package itself. Thus, it is available for convenience, here.

You can import it with

```javascript
import { BufferStream: { Writer } } from "./index.mjs";

const sink = new Writer();
```

or

```javascript
import { BufferStream } from "./index.mjs";

const sink = new BufferStream.Writer();
```

After that, you can pipe an octet stream into `sink`, conveniently wait for it to end and get all received data in a single buffer.

```javascript
source.pipe( sink );
source.on( "error", cause => sink.destroy( cause ) );

const data = await sink.asPromise;
```

Instances of this class are customized on construction. 

**Signature:** `new Writer( options )`

* `options` is a set a stream options passed to underlying [writable stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_writable_streams). Options `octetStream` and `decodeStrings` are always set to `true` and `false` and thus can't be customized.
