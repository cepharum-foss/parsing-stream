# CollectingSubstringParser

The `CollectingSubstringParser` class implements parser searching data of attached stream for some fixed sequence of octets while pushing any non-matching octet into separate stream. It is meant to extend API of [`SubstringParser` class](substring-parser.html), though actually inheriting from classes [`CollectingParser`](collecting-parser.html) and [`Parser`](parser.html). 


## Construction

**Signature:** `new CollectingSubstringParser( needle, options )`

* `needle` is a Buffer or string describing the sequence of octets to search for.
* `options` is a set of [options](collecting-parser.html#construction-options) supported by underlying [`CollectingParser`](collecting-parser.html).

## Methods

### onMatch()

You need to implement a custom match handler either by assigning one to any created instance explicitly or by inheriting another class with a proper match handler from this class.

See [related documentation for `Parser` class](parser.html#onmatch).
