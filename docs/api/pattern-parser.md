# PatternParser

The `SubstringParser` class implements parser searching data of attached stream for some variable sequence of octets complying with a pattern resembling regular expressions. It is based on [`Parser` class](parser.html).


## Construction

**Signature:** `new PatternParser( pattern, options? )`

* `pattern` is a string describing the sequence of octets to search for. It must comply with certain [syntax rules for patterns](pattern.html#syntax).
* `options` is customizing the parser. It is basically identical to [constructor options of abstract `Parser`](parser.html#constructor)

## Methods

### onMatch()

You need to implement a custom match handler either by assigning one to any created instance explicitly or by inheriting another class with a proper match handler from this class.

See [related documentation for `Parser` class](parser.html#onmatch).
