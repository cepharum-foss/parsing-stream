# CollectingParser

The `CollectingParser` class is an _abstract_ base class based on [`Parser`](parser.html) implementing common API for exclusively or additionally providing all non-matching octets in a separate stream.

## Ready-to-use variants

**You can't use `CollectingParser` directly.** 

Instead, you have to inherit another class which is implementing the proper code for search octets. There are a few such classes included:

* [CollectingSubstringParser](collecting-substring-parser.html) for searching single fixed-size sequence of octets and feed a separate stream for all non-matching octets. 
* [CollectingPatternParser](collecting-pattern-parser.html) for searching octets matching kind of regular expression and feed a separate stream for all non-matching octets.


## Construction options

The constructor accepts an object of options which is basically identical what is accepting by [constructor of abstract `Parser` class](parser.html#constructor).

In addition, these options related to collecting parsers are accepted:

### tee

The `tee` option is a boolean controlling whether non-matching octets are forwarded to [`collected` stream](#collected) and sent to output of attached stream (`true`) or just the former (`false`).

The default is `true`.

## Properties

### collected

This readable stream is delivering all non-matching octets. 


## Methods

### switchCollected()

**Signature:** `switchCollected()`

This method is closing current stream exposed as [`collected`](#collected) and replace it with another one.
