# Pattern

This class is implementing a parser for compiling a string describing a variable sequence of octets into a sequence of pattern steps suitable for use with classes  [`PatternParser`](pattern-parser.html) and [`CollectingPatternParser`](collecting-pattern-parser.html).

This class is used by those parsers to convert a provided description of a pattern in computable form suitable for searching matches in data provided chunk by chunk over time.

## Syntax

Patterns supported by this class resemble regular expressions to some very limited extent.

::: warning Note!
This pattern compiler doesn't support regular expression features which haven't been described here, too.
:::

### Literals

Due to using string for describing octets, every non-functional character is representing sequence of octets used to encode it in UTF-8.

```javascript
"abc"
```

is describing sequence of three octets `0x61`, `0x62` and `0x63`.

### Escaping

Any functional character can be provided as literal character by prefixing it with a literal backslash character. Due to Javascript also using it for escaping, you need to provide it twice in a literal string to get one literal backslash.

```javascript
"\\["
```

is representing single octet `0x5b` for opening brackets. Without the backslash, the `[` is assumed to start a class of characters as demonstrated next. 

### Character classes

A class of characters must be wrapped in a pair of brackets.

```javascript
"[abc]"
```

This is describing a single octet matching either `0x61`, `0x62` or `0x63`.

Ranges are supported:

```javascript
"[0-9]"
```

is equivalent to

```javascript
"[0123456789]"
```

Negations are supported, too:

```javascript
"[^0-9]"
```

describes any character but some digit.

::: warning Use with care!
Using negation results in a rather expensive search. Prevent using it as much as possible. 

In addition, due to literal characters being meant to just represent arbitrary octets of data with UTF-8 codepoints consuming different number of octets, succeeding on a negated tests can't be UTF-8 safe and always results in a single octet considered matching, only.
:::

### Literal sequences of octets

Any fixed set of octets can be injected using two-digit hex codes per octet wrapped in a pair of parentheses:

```javascript
"(010203ff)"
```

describes sequence of octets with values `0x01`, `0x02`, `0x03` and `0xff`.

### Wildcard

```javascript
"."
```

matches any single octet.

```javascript
"[.]"
```

or 

```javascript
"\\."
```

match single octet with value `0x2e`.

### Quantifiers

Every literal, character class, sequence of octets or wildcard in a pattern may be quantified using quantifier given as minimum and maximum number of occurrences wrapped in curly braces.

```javascript
"a{2,5}"
```

is matching a sequence of 2 to 5 octets with value `0x61` each.

On omitting the latter value while keeping the former and the separating comma, maximum number of accepted occurrences is `+Infinity`.

```javascript
"a{2,}"
```

is matching a sequence with two or more octets with value `0x61` each.

When omitting one of the boundaries and the separating comma, the sole given value is used for both limits.

```javascript
"a{2}"
```

matches exactly two consecutive octets with value `0x61`.

Some shortcuts exist:

| shortcut | is equivalent to |
|---|---|
| `?` | `{0,1}` |
| `*` | `{0,}` |
| `+` | `{1,}` |
