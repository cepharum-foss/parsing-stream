# CollectingRaceParser

The `CollectingRaceParser` is the collecting variant of [`RaceParser`](race-parser.html). As such, it is combining multiple parsers to race for a match while collecting all octets preceding any match. 

## Construction

**Signature:** `new CollectingRaceParser( parsers, options? )`

* `parsers` is a list of basic parsers to participate in race for next match.
* `options` is a set of [options](collecting-parser.html#construction-options) supported by underlying [`CollectingParser`](collecting-parser.html).

## Methods

### onMatch()

You need to implement a custom match handler either by assigning one to any created instance explicitly or by inheriting another class with a proper match handler from this class.

See [related documentation for `Parser` class](parser.html#onmatch).

:::tip And the winner is ...  
In opposition to match handlers of most other parsers, this one is receiving the matching parser in another argument.  
:::
