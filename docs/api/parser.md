# Parser

The `Parser` class is an _abstract_ base class implementing common API for observing all octets seen by an instance of [class Stream](stream.html) and search for some octets matching certain criteria.

Included features are:

* integrate with transform stream processing streamed octets chunk by chunk,
* integrate with match handling,
* managing octets held back for further investigation and
* providing common callbacks for signalling matches.


## Ready-to-use variants

**You can't use `Parser` directly.** 

Instead, you have to inherit another class which is implementing the proper code for searching streamed octets chunk by chunk. The following lists introduce parsers included with **parsing-stream** package.

### Basic Parsers

Basic parsers search streamed data for sequences of octets. 

* [SubstringParser](substring-parser.html) is searching for occurrences of a single fixed-size sequence of octets.
* [PatternParser](pattern-parser.html) is searching for occurrences of octets matching some [_kind of_ regular expression](pattern.html).
* [DisabledParser](disabled-parser.html) isn't searching for anything. It is meant to replace an existing parser to stop searching streamed data for improved performance.

### Combining Parsers

Combining parsers are distributing chunks of streamed data to subordinated parser instances.

* [RaceParser](race-parser.html) is integrating multiple basic parsers enabling them to race for matches. First matching parser wins. On every match, a new race for another match is started.

### Collecting Parsers

Parsers listed above are looking for matches in streamed data, only. There are variants of those parsers collecting any non-matching octets in addition.

Variants are:

* [CollectingSubstringParser](collecting-substring-parser.html)
* [CollectingPatternParser](collecting-pattern-parser.html)
* [CollectingRaceParser](collecting-race-parser.html)

## Constructor

You may provide an object with options for customizing the resulting parser instantly.

* It might contain `onMatch()` method replacing the original one which is basically idling.

  ```javascript
  const parser = new Parser( {
      onMatch( context, buffers, atOffset ) {
          // your options here:
          // * handle the match
          // * replace buffers in provided list to replace content of stream
          // * return different parser to replace current one on parsing stream
      }
  } )
  ```

## Properties

### data

This `Buffer` is a concatenation of chunks previously held back for re-investigating on next chunk arriving. It's a getter concatenating chunks exposed as [`previouslyMatchingChunks`](#previouslymatchingchunks) in a single buffer, thus it is more expensive than just fetching that list. 

### heldBuffers

This read-only list is providing recently seen chunks of octets that have been held back due to containing start of a potential match which has to be investigated further on next presented chunk of octets.

### previouslyMatchingOctetsCount

This integer provides the total number of octets found in previously held back chunks of seen octets as exposed in [`previouslyMatchingChunks`](#previouslymatchingchunks). 

### previouslyMatchingChunks

This array of buffers lists all chunks held back recently for re-investigating on reception of another chunk.

### wasMatchingPartially

Indicates if there are previously held back chunks of octets due to assuming they contain parts of a potential match.


## Methods

### parse()

**Signature:** `parse( context, buffer, atOffset, isLast )`

This method **must be** implemented in a class inheriting from this one. It is meant to search provided chunk of data to match that parser's criteria.

* `context` is the stream's context as provided as first argument to method `parse()`.

* `buffer` is the current chunk of data inspected while discovering the match. It is identical to the second argument provided to method `parse()`.

* `atOffset` is the given chunk's offset into the stream of octets. It is identical to the third argument provided to method `parse()`.

* `isLast` indicates whether stream has been closed and this is the last chunk to inspect.

  ::: warning Last isn't last
  This flag is an indicator referring to the chunk provided by the attached stream. It doesn't imply last invocation of `parse()`, though.

  When signalling full match ending prior to end of that provided chunk, `parse()` will be invoked another time with `isLast` being `true` again.
  :::

#### Parser results

Implementations of `parse()` are meant to instantly return whatever is returned from invoking one out of [`signalFullMatch()`](#signalfullmatch), [`signalPartialMatch()`](#signalpartialmatch) or [`signalNoMatch()`](#signalnomatch).

On signalling a full match in the mid of current chunk, `parse()` will be re-invoked to process the rest of that chunk afterwards.

### onMatch()

**Signature:** `onMatch( context, buffers, atOffset ): Parser | Promise<Parser>`

This method is invoked whenever a full match is reported. It is optional and missing in abstract `Parser` class as in all [ready-to-use variants](#ready-to-use-variants) included with parsing-stream package. It is up to a consuming application to assign a custom match handler. 

* `context` is referring to the [stream's context](context.html).
* `buffers` is an ordered array of `Buffer` instances jointly containing all octets representing the match.
* `atOffset` is the index into all octets seen by attached stream of first octet in describing list of buffers.

The method _may_ return an instance of another parser to replace current one as attached to the stream. Current parser is kept attached on returning nothing or `undefined`. A match handler may defer the processing by returning a promise which is either resolving to another parser's instance or to `undefined`.

:::info  
For convenience, you can assign a match handler using [parser options on parser construction](#constructor).
:::

### onAttach()

**Signature:** `onAttach( stream ): void`

When attaching parser to a stream this method is invoked to notify the parser accordingly.

### onDetach()

**Signature:** `onDetach(): void`

When detaching parser from a stream this method is invoked to notify the parser accordingly.

### collectForwarded()

**Signature:** `collectForwarded( collector, buffer ): void`

This method is used to re-inject any processed data into the readable part of attached stream. You can overload it in a derived class e.g. to prevent non-matching octets from being forwarded to that stream's output.

* `collector` is a list of Buffer instances meant to be pushed into reading end of attached stream. Don't rely on any information pre-existing in this list. Just append whatever data should be sent.
* `buffer` is current chunk to be sent to output. It isn't limited to matching or non-matching octets and from within this method is not clear which sort of octets is included here.

The method does not return anything.

### signalFullMatch()

**Signature:** `signalFullMatch( context, buffer, atOffset, firstIndex, endIndex )`

::: warning  
`signalFullMatch` is one out of three methods to pick from to indicate outcome of parsing a chunk of data. It must be invoked last at the end of any implementation of method `parser()`. Its result can be instantly forwarded as result of method `parse()`, too.  
:::

This helper method is indicating full discovery of a match. It takes care of invoking any available match handler and of putting out octets afterwards.

* `context` is the stream's context as provided as first argument to method `parse()`.

* `buffer` is the current chunk of data inspected while discovering the match. It is identical to the second argument provided to method `parse()`.

* `atOffset` is the given chunk's offset into the stream of octets. It is identical to the third argument provided to method `parse()`.

* `firstIndex` is the relative index of first octet of match. `0` is referring to the first octet in provided `buffer`. Negative values are referring to octets in previously held back chunks of data.

  Valid range is from `-previouslyMatchingOctetsCount` to `buffer.length`. It must not be greater than `endIndex`.

* `endIndex` is the relative index of first octet following match. See `firstIndex` for additional rules applying.

The result is a description of buffers to release/put out, buffers to hold back and a parser to attach to stream instead of current one. All information is optional. 

### signalPartialMatch()

**Signature:** `signalPartialMatch( buffer, firstIndex )`

::: warning  
`signalPartialMatch` is one out of three methods to pick from to indicate outcome of parsing a chunk of data. It must be invoked last at the end of any implementation of method `parser()`. Its result can be instantly forwarded as result of method `parse()`, too.  
:::

This helper method is indicating partial discovery of a potential match. Use this to hold back octets of current chunk or keep holding back some octets of previously hold back chunks.

* `buffer` is the current chunk of data inspected while discovering the partial match. It is identical to the second argument provided to method `parse()`.

* `firstIndex` is the relative index of first octet of potential match. `0` is referring to the first octet in provided `buffer`. Negative values are referring to octets in previously held back chunks of data.

  Valid range is from `-previouslyMatchingOctetsCount` to `buffer.length`. 

The result is a description of buffers to release/put out, buffers to hold back and a parser to attach to stream instead of current one. All information is optional.

### signalNoMatch()

**Signature:** `signalNoMatch( buffer )`

::: warning  
`signalNoMatch` is one out of three methods to pick from to indicate outcome of parsing a chunk of data. It must be invoked last at the end of any implementation of method `parser()`. Its result can be instantly forwarded as result of method `parse()`, too.  
:::

This helper method is indicating lack of any matching octets in provided `buffer` or any previously held back chunk. It is thus releasing any previously held back chunk as well as current one.

* `buffer` is the current chunk of data inspected while discovering the partial match. It is identical to the second argument provided to method `parse()`.

The result is a description of buffers to release/put out, buffers to hold back and a parser to attach to stream instead of current one. All information is optional.
