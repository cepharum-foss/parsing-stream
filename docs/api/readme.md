# API Overview

This package provides these classes:

## Streaming

### Basics

* [Stream](stream.html)
* [Context](context.html)

### Helpers

* [BufferStream.Reader](buffer-stream-reader.html)
* [BufferStream.Writer](buffer-stream-writer.html)

## Parsers

### Basics

* [Parser](parser.html)
* [CollectingParser](collecting-parser.html)

### Searching

* [SubstringParser](substring-parser.html)
* [PatternParser](pattern-parser.html)
  * [Pattern](pattern.html)

### Combining

* [RaceParser](race-parser.html)

### Collecting

* [CollectingSubstringParser](collecting-substring-parser.html)
* [CollectingPatternParser](collecting-pattern-parser.html)
* [CollectingRaceParser](collecting-race-parser.html)

### Special use

* [DisabledParser](disabled-parser.html)
