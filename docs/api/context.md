# Context

The Context class is meant to be a common base class for any context supported by instances of [`Stream` class](stream.html). This base class doesn't have any custom properties or methods. 

When implementing code based on parsing stream, derived classes may be created to serve as contexts which are recognized as such by parsing stream.

::: warning
A custom context must be provided on [constructing Stream instance](stream.html#context).
:::
