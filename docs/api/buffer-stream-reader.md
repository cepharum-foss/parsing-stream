# BufferStream.Reader

This helper class is a [readable stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_readable_streams) delivering provided data in chunks of customizable size and with a customizable delay per delivered chunk.

It is extensively used in unit-testing this package. Apart from that, it is of no special use for the parsing-stream package itself. Thus, it is available for convenience, here.

You can import it with

```javascript
import { BufferStream: { Reader } } from "./index.mjs";

const source = new Reader( "some content" );
```

or

```javascript
import { BufferStream } from "./index.mjs";

const source = new BufferStream.Reader( "some content" );
```

Instances of this class are customized on construction.

**Signature:** `new Reader( data, chunkSize, delay, options )`

* The `data` is either a string or an instance of `Buffer`. It represents the whole data provided via the created stream.
* The `chunkSize` is a positive integer setting number of octets to be written at once. Default is `256` octets per chunk.
* The `delay` is the number of milliseconds to wait between writing chunks. It may be
  * `0` to write as many chunks synchronously as possible until some watermark is reached,
  * `1` (default) to delay writing chunks with `process.nextTick()` or
  * any higher integer value to delay writing chunks with `setTimeout()` accordingly.
* `options` is a set a stream options passed to underlying [readable stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_readable_streams). Option `octetStream` is always set to `true` and thus can't be customized.
