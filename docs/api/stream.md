# Stream

The `Stream` class is a custom [transform stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_class_stream_transform) integrating [a parser](parser.html) for inspecting each and every passing octet.

For every stream, there is a context shared by all parsers and their match handlers. This is also used to resolve a promise when stream has finished.

## Constructor options

In addition to most options supported by underlying [transform stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_class_stream_transform), these options are supported in addition:

### context

This option must be an instance of [class `Context`](context.html). It is optional. When provided, that instance is used as stream's context instead of an instance created internally by default. 

A stream's context can't be changed afterwards.

### initialParser

This option is selecting a [parser](parser.html) to be attached initially. It is optional and may be changed later at any time using [stream's property `parser`](#parser).

## Properties

These properties are available in addition to those provided by underlying [transform stream](https://nodejs.org/dist/latest/docs/api/stream.html#stream_class_stream_transform).

### context

This read-only property exposes [context](context.html) of stream which is shared by all attached parsers and their match handlers. It may be used to customize attached parsers or to collect information derived from stream data. Multiple parsers can rely on the context for [managing tasks requiring interaction of multiple parsers](../guide.html#process-markup).

### parser

This property refers to the currently attached [parser](parser.html). It can be used to replace it, too. However, apart from attaching initial parser after stream's creation, parser should be changed from within a match handler, only.

### asPromise

This property is a promise resolved with the stream's [context](#context) when the stream has finished processing all streamed data. This is useful to conveniently defer processing until a change of data has been processed completely.
