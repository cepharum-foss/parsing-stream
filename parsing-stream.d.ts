import type {
    Readable,
    Writable,
    Transform,
    ReadableOptions,
    WritableOptions, TransformOptions
} from "stream";

declare module "parsing-stream" {
    /**
     * Implements simple stream emitting data of a provided buffer chunk by
     * chunk with optional delay in between.
     */
    export class BufferReadStream extends Readable {
        constructor(buffer: Buffer, chunkSize?: number, delay?: number, options?: ReadableOptions);
    }

    /**
     * Implements simple stream suitable for collecting stream data in a
     * resulting buffer instance.
     */
    export class BufferWriteStream extends Writable {
        constructor(options?: WritableOptions);

        /**
         * Promises all received octets after stream has finished.
         */
        readonly asPromise: Promise<Buffer>;
    }

    /**
     * Describes base class for context shared by parsers attached a stream and
     * any callback involved in discovering and handling data found in stream.
     */
    export class Context {
    }

    /**
     * Integrates attached parser on octets passing through.
     */
    export class Stream extends Transform {
        constructor(options?: StreamOptions);

        /**
         * Exposes context shared by parsers attached to this stream and all the
         * callbacks involved in processing.
         */
        readonly context: Context;

        /**
         * Controls parser currently attached to stream for inspecting and
         * processing streamed data further on.
         */
        parser?: Parser;

        /**
         * Promises stream's context when stream has finished processing
         * provided stream of data. It is rejected on stream failing.
         */
        asPromise: Promise<Context>;
    }

    export interface StreamOptions extends TransformOptions {
        /**
         * Assigns context to use on a stream instead of implicitly created one.
         */
        context?: Context;

        /**
         * Picks parser to attach to stream initially.
         */
        initialParser?: Parser;
    }

    /**
     * Exposes buffer streams using aliases.
     */
    export interface BufferStream {
        Reader: BufferReadStream;
        Writer: BufferWriteStream;
    }

    /**
     * Describes callback invoked on parsers attached to streams for handling an
     * encountered match.
     *
     * The provided list of buffers is forwarded to reading end of stream
     * afterwards. Thus, a match handler is capable of adjusting streamed data
     * by replacing listed buffers in-place using @see Array#splice().
     */
    export type MatchHandler = (this: Parser, context: Context, buffers: Buffer[], matchOffset: number) => Parser | Promise<Parser>;

    /**
     * Implements base functionality for parsers suitable for integration with a
     * stream of octets to discover parts of streamed data and handle them
     * accordingly.
     */
    export class Parser {
        constructor(options?: ParserOptions);

        /**
         * Resets state of provided parser according to assumptions of current
         * type of parser.
         *
         * This method is used to drop a parser's state of previous partial
         * matches causing that parser to start searching for another match from
         * the beginning on next chunk of data. This is required in context of
         * RaceParser.
         */
        resetState(): void;

        /**
         * Resets state of provided parser according to assumptions of current
         * type of parser.
         *
         * This method is used by Parser#resetState() internally. It is required
         * there to handle Javascript's lack of supporting multi-inheritance.
         *
         * @param parser
         */
        protected static resetState(parser: Parser): void;

        /**
         * Exposes joined list of chunks that have been collected recently for
         * being part of a match.
         */
        readonly data: Buffer;

        /**
         * Indicates if chunks of streamed data have been collected for probably
         * containing a match to be discovered after seeing more data.
         */
        readonly wasMatchingPartially: boolean;

        /**
         * Provides number of octets previously held back for (probably)
         * containing (the leading part of) a match.
         */
        readonly previouslyMatchingOctetsCount: number;

        /**
         * Provides chunks of octets previously seen in stream and held back for
         * (probably) containing (the leading part of) a match.
         */
        readonly previouslyMatchingChunks: Buffer[];

        /**
         * Gets invoked whenever parser has reported a full match.
         */
        onMatch: MatchHandler;

        /**
         * Gets invoked whenever parser is _directly_ attached to a stream. It
         * doesn't apply to parsers attached to a stream through combining
         * parsers such as `RaceParser`.
         */
        onAttach( stream: Stream ): void;

        /**
         * Gets invoked whenever parser is detached from a stream it has been
         * _directly_ attached to before. It doesn't apply to parsers attached
         * to a stream through combining parsers such as `RaceParser`.
         */
        onDetach(): void;

        /**
         * Lists buffers currently held back after partially matching parser
         * until further octets have been seen to either release on eventual
         * mismatch or hold back until some match handler was able to inspect or
         * replace contained data.
         *
         * @note Use methods signalFullMatch(), signalPartialMatch() and
         *       signalNoMatch() to control this list's content.
         */
        protected readonly heldBuffers: Buffer[];

        /**
         * Tries to parse as much data as possible from provided chunk of octets
         * seen while streaming data.
         *
         * The parser may use provided context for cumulating results between
         * calls.
         *
         * @param context context of stream and its parsers for sharing custom data between calls
         * @param buffer chunk of data received, to be inspected
         * @param atOffset number of octets seen before provided chunk
         * @param isLast true if provided chunk is last one of attached stream
         */
        parse(context: Context, buffer: Buffer, atOffset: number, isLast: boolean): Promise<ParserResult>;

        /**
         * Creates parser result indicating discovery of full match in provided
         * chunk of streamed data (probaly in combination with previously seen
         * chunks).
         *
         * @param context context of stream and its parsers for sharing custom data between calls, used for sharing with invoked match handler
         * @param buffer currently processed chunk of streamed data
         * @param atOffset number of octets seen before this chunk, used for sharing with invoked match handler
         * @param firstIndex index of first matching octet in stream, relative to beginning of current chunk (thus might be negative)
         * @param endIndex index of first octet in provided buffer following detected match
         */
        protected signalFullMatch(context: Context, buffer: Buffer, atOffset: number, firstIndex: number, endIndex: number): Promise<ParserResult>;

        /**
         * Creates parser result indicating incomplete reception of a probable
         * match at end of provided chunk of streamed data.
         *
         * The indicated match might have begun in a previously seen chunk, thus
         * the given index might be negative. Any previously collected chunks
         * not included with index given here are released for passing on.
         *
         * @param buffer currently processed chunk of streamed data
         * @param firstIndex index of first matching octet in stream, relative to beginning of current chunk (thus might be negative)
         */
        protected signalPartialMatch(buffer: Buffer, firstIndex: number): Promise<ParserResult>;

        /**
         * Creates parser result indicating lack of any match in provided chunk
         * of streamed data.
         *
         * @param buffer currently processed chunk of streamed data
         */
        protected signalNoMatch(buffer: Buffer): Promise<ParserResult>;
    }

    /**
     * Describes customizations support by basic parser implementation.
     */
    export interface ParserOptions {
        /**
         * Provides match handler to explicitly use for handling matches.
         *
         * If omitted, a parser's default match handler is used. Match handler
         * can be adjusted by assigning callback to onMatch() after parser
         * creation, too. Matches are ignored if parser does not have any
         * default handler on parsing chunks of data.
         */
        onMatch?: MatchHandler;
    }

    /**
     * Describes result of parsing a chunk of data.
     */
    export interface ParserResult {
        /**
         * Lists chunks of previously seen octets that have been ignored by this
         * parser.
         */
        parsed: Buffer[];

        /**
         * Provides part of currently processed chunk of data which hasn't been
         * processed yet due to discovering a match.
         *
         * Usually, the octets in this buffer are following the matching octets.
         */
        unparsed?: Buffer;

        /**
         * Provides parser instance to use instead of current one in further
         * processing of stream, usually as a result of current parser matching.
         */
        parser?: Parser;
    }

    /**
     * Implements parser searching stream for a sequence of octets.
     *
     * This parser may be considered the counterpart to String#indexOf().
     */
    export class SubstringParser extends Parser {
        /**
         * @param sub sequence of octets to search for
         * @param options customization settings for parser
         */
        constructor(sub: Buffer|string, options?: ParserOptions);

        /**
         * Exposes _needle_ to search in _haystack_ of octet stream.
         */
        needle: Buffer;
    }

    export interface CollectingParserOptions extends ParserOptions {
        /**
         * Controls whether non-matching octets are written to stream of
         * collected octets in addition to passing them via parsed stream (true)
         * or written to the former stream of collected non-matching octets,
         * only (false).
         *
         * Default is true.
         */
        tee: boolean;
    }

    /**
     * Implements non-matching parser collecting passed octets rather than
     * forwarding.
     *
     * @note This parser does not work on its own for lacking actual
     *       implementation of method `Parser#parse()`.
     */
    export class CollectingParser extends Parser {
        /**
         * @param options parser customizations
         */
        constructor(options?: CollectingParserOptions);

        /**
         * Streams collected octets for further processing.
         */
        collected: Readable;

        /**
         * Closes existing stream of octets and creates another one.
         */
        switchCollected(): void;
    }

    /**
     * Implements parser searching stream for a sequence of octets literally
     * matching provided needle while providing all non-matching octets in
     * another readable stream.
     *
     * The parser combines features of `SubstringParser` and `CollectingParser`.
     * But it's derived from `CollectingParser`, only.
     */
    export class CollectingSubstringParser extends CollectingParser {
        /**
         * @param needle sequence of octets to search for
         * @param options parser customizations
         */
        constructor(needle: Buffer|string, options?: CollectingParserOptions);

        /**
         * Exposes _needle_ to search in _haystack_ of octet stream.
         */
        needle: Buffer;
    }

    /**
     * Implements parser searching stream for a sequence of octets described by
     * provided pattern while providing all non-matching octets in another
     * readable stream.
     *
     * The parser combines features of `PatternParser` and `CollectingParser`.
     * But it's derived from `CollectingParser`, only.
     */
    export class CollectingPatternParser extends CollectingParser {
        /**
         * @param pattern source of pattern to search for
         * @param options parser customizations
         */
        constructor(pattern: string, options?: CollectingParserOptions);

        /**
         * Exposes _needle_ to search in _haystack_ of octet stream.
         */
        needle: Buffer;
    }

    /**
     * Represents parser instance which isn't searching for any match.
     *
     * This type of parser is supported for use with parser result descriptor
     * picking different parser to use further on. A falsy selection of a parser
     * isn't working there and thus you need to provide this parser to disable
     * any further stream parsing.
     */
    export class DisabledParser extends Parser {
    }

    /**
     * Implements parser searching octet stream for sequence of octets matching
     * some described pattern.
     */
    export class PatternParser extends Parser {
        /**
         * @param pattern pattern of octets to search for (similar to regular expression)
         * @param options customization settings for parser
         */
        constructor(pattern: PatternExpression, options?: ParserOptions);

        readonly pattern: PatternStep[];
    }

    /**
     * Expresses pattern of octets to search for.
     *
     * This string accepts a format similar to regular expressions, limited in
     * functionality though.
     */
    export type PatternExpression = string;

    /**
     * Describes single step of pattern as parsed from provided pattern
     * expression.
     */
    export interface PatternStep {
        /**
         * Lists sequences of octets alternatively expected at current position.
         */
        readonly values: Buffer[];

        /**
         * Indicates if list of values indicates values rejected at current
         * position rather than accepted.
         */
        readonly negated: boolean;

        /**
         * Indicates lower limit on total number of consecutive occurrences of
         * listed values (if accepted) or total number of consecutive octets
         * not matching any value (if rejected).
         */
        readonly minimum: number;

        /**
         * Indicates upper limit on total number of consecutive occurrences of
         * listed values (if accepted) or total number of consecutive octets
         * not matching any value (if rejected).
         */
        readonly maximum: number;
    }

    /**
     * Implements helper functions for processing pattern expressions and
     * converting them into processable sequence of pattern steps.
     */
    export class Pattern {
        static fromSource(expression: PatternExpression): PatternStep[];
    }

    /**
     * Describes state of pattern-based parser.
     */
    export interface PatternParserState {
        /**
         * Selects step (by index) of parsed pattern the parser is expecting to
         * match next octet in stream.
         */
        step: number;

        /**
         * Selects value (by index) out of several possible values in current
         * step of parsed pattern the parser is expecting to match next octet in
         * stream.
         */
        value: number;

        /**
         * Selects octet (by index) of current (multi-octet) value in current
         * step of parsed pattern the parser is expecting to match next octet in
         * stream.
         */
        octet: number;

        /**
         * Tracks number of octets per value matched for current step so far.
         */
        sizes: number[];

        /**
         * Tracks index of tested octet into inspected chunk of streamed octets
         * currently held back for inspection.
         */
        cursor: number;
    }

    /**
     * Implements parser searching stream for matches of one or more attached
     * parsers. Whenever one of the listed parsers is matching, it is selected
     * to handle the match and all other parsers are reset. If RaceParser itself
     * has a match handler, it is used in preference over either matching
     * parser's handler and that matching parser is provided as an additional
     * argument.
     */
    export class RaceParser extends Parser {
        /**
         * @param parsers non-empty list of parsers to apply initially
         * @param options customization settings for parser
         */
        constructor(parsers: Parser[], options?: ParserOptions);

        /**
         * Lists racing parsers.
         *
         * This list must not be empty and must consist of parser instances
         * except for RaceParser and CollectingParser instances, only. Changes
         * are recognized on next attempt to parse a chunk of data.
         */
        readonly parsers: Parser[];
    }

    /**
     * Implements parser searching stream for matches of one or more attached
     * parsers. Whenever one of the listed parsers is matching, it is selected
     * to handle the match and all other parsers are reset.
     *
     * This parser is similar to RaceParser. But in addition, it is collecting
     * all octets seen before a match.
     */
    export class CollectingRaceParser extends Parser {
        /**
         * @param parsers non-empty list of parsers to apply initially
         * @param options customization settings for collecting parser
         */
        constructor(parsers: Parser[], options?: CollectingParserOptions);

        /**
         * Lists racing parsers.
         *
         * This list must not be empty and must consist of parser instances
         * except for RaceParser and CollectingParser instances, only. Changes
         * are recognized on next attempt to parse a chunk of data.
         */
        readonly parsers: Parser[];
    }
}
