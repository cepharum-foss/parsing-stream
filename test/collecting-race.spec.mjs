import { describe, it } from "mocha";
import "should";

import {
	CollectingPatternParser, CollectingSubstringParser,
	Parser,
	PatternParser,
	CollectingRaceParser,
	SubstringParser, RaceParser, DisabledParser, BufferStream
} from "../index.mjs";
import { tryParser } from "./helper.mjs";


describe( "CollectingRaceParser", () => {
	it( "is exposed", () => {
		CollectingRaceParser.should.be.ok();
	} );

	it( "rejects to be created without arguments", () => {
		( () => new CollectingRaceParser() ).should.throw();
	} );

	it( "requires non-empty list of parsers on construction", () => {
		( () => new CollectingRaceParser( null ) ).should.throw();
		( () => new CollectingRaceParser( undefined ) ).should.throw();
		( () => new CollectingRaceParser( false ) ).should.throw();
		( () => new CollectingRaceParser( true ) ).should.throw();
		( () => new CollectingRaceParser( 0 ) ).should.throw();
		( () => new CollectingRaceParser( 12 ) ).should.throw();
		( () => new CollectingRaceParser( -32.54 ) ).should.throw();
		( () => new CollectingRaceParser( "" ) ).should.throw();
		( () => new CollectingRaceParser( "   " ) ).should.throw();
		( () => new CollectingRaceParser( {} ) ).should.throw();
		( () => new CollectingRaceParser( { parser: new Parser() } ) ).should.throw();
		( () => new CollectingRaceParser( () => new Parser() ) ).should.throw();

		( () => new CollectingRaceParser( [] ) ).should.throw();
		( () => new CollectingRaceParser( [null] ) ).should.throw();
		( () => new CollectingRaceParser( [undefined] ) ).should.throw();
		( () => new CollectingRaceParser( [false] ) ).should.throw();
		( () => new CollectingRaceParser( [true] ) ).should.throw();
		( () => new CollectingRaceParser( [0] ) ).should.throw();
		( () => new CollectingRaceParser( [12] ) ).should.throw();
		( () => new CollectingRaceParser( [-32.54] ) ).should.throw();
		( () => new CollectingRaceParser( [""] ) ).should.throw();
		( () => new CollectingRaceParser( ["   "] ) ).should.throw();
		( () => new CollectingRaceParser( [{}] ) ).should.throw();
		( () => new CollectingRaceParser( [{ parser: new Parser() }] ) ).should.throw();
		( () => new CollectingRaceParser( [() => new Parser()] ) ).should.throw();

		( () => new CollectingRaceParser( [new Parser()] ) ).should.not.throw();
	} );

	it( "rejects list of parsers being replaced", () => {
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );
		const parser = new CollectingRaceParser( [ first, second ] );

		( () => { parser.parsers = undefined; } ).should.throw();
		( () => { parser.parsers = null; } ).should.throw();
		( () => { parser.parsers = false; } ).should.throw();
		( () => { parser.parsers = true; } ).should.throw();
		( () => { parser.parsers = 0; } ).should.throw();
		( () => { parser.parsers = 12; } ).should.throw();
		( () => { parser.parsers = -32.53; } ).should.throw();
		( () => { parser.parsers = ""; } ).should.throw();
		( () => { parser.parsers = "   "; } ).should.throw();
		( () => { parser.parsers = {}; } ).should.throw();
		( () => { parser.parsers = { parser: first }; } ).should.throw();
		( () => { parser.parsers = () => first; } ).should.throw();

		( () => { parser.parsers = [undefined]; } ).should.throw();
		( () => { parser.parsers = [null]; } ).should.throw();
		( () => { parser.parsers = [false]; } ).should.throw();
		( () => { parser.parsers = [true]; } ).should.throw();
		( () => { parser.parsers = [0]; } ).should.throw();
		( () => { parser.parsers = [12]; } ).should.throw();
		( () => { parser.parsers = [-32.53]; } ).should.throw();
		( () => { parser.parsers = [""]; } ).should.throw();
		( () => { parser.parsers = ["   "]; } ).should.throw();
		( () => { parser.parsers = [{}]; } ).should.throw();
		( () => { parser.parsers = [{ parser: first }]; } ).should.throw();
		( () => { parser.parsers = [() => first]; } ).should.throw();

		( () => { parser.parsers = [first]; } ).should.throw();
	} );

	it( "accepts list of parsers being modified", () => {
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );
		const parser = new CollectingRaceParser( [ first, second ] );

		( () => { parser.parsers[0] = undefined; } ).should.not.throw();
		( () => { parser.parsers[0] = null; } ).should.not.throw();
		( () => { parser.parsers[0] = false; } ).should.not.throw();
		( () => { parser.parsers[0] = true; } ).should.not.throw();
		( () => { parser.parsers[0] = 0; } ).should.not.throw();
		( () => { parser.parsers[0] = 12; } ).should.not.throw();
		( () => { parser.parsers[0] = -32.53; } ).should.not.throw();
		( () => { parser.parsers[0] = ""; } ).should.not.throw();
		( () => { parser.parsers[0] = "   "; } ).should.not.throw();
		( () => { parser.parsers[0] = {}; } ).should.not.throw();
		( () => { parser.parsers[0] = { parser: first }; } ).should.not.throw();
		( () => { parser.parsers[0] = () => first; } ).should.not.throw();

		( () => { parser.parsers[1] = [undefined]; } ).should.not.throw();
		( () => { parser.parsers[1] = [null]; } ).should.not.throw();
		( () => { parser.parsers[1] = [false]; } ).should.not.throw();
		( () => { parser.parsers[1] = [true]; } ).should.not.throw();
		( () => { parser.parsers[1] = [0]; } ).should.not.throw();
		( () => { parser.parsers[1] = [12]; } ).should.not.throw();
		( () => { parser.parsers[1] = [-32.53]; } ).should.not.throw();
		( () => { parser.parsers[1] = [""]; } ).should.not.throw();
		( () => { parser.parsers[1] = ["   "]; } ).should.not.throw();
		( () => { parser.parsers[1] = [{}]; } ).should.not.throw();
		( () => { parser.parsers[1] = [{ parser: first }]; } ).should.not.throw();
		( () => { parser.parsers[1] = [() => first]; } ).should.not.throw();

		( () => { parser.parsers[100] = [first]; } ).should.not.throw();

		for ( const name of [ "push", "unshift" ] ) {
			( () => { parser.parsers[name]( undefined ); } ).should.not.throw();
			( () => { parser.parsers[name]( null ); } ).should.not.throw();
			( () => { parser.parsers[name]( false ); } ).should.not.throw();
			( () => { parser.parsers[name]( true ); } ).should.not.throw();
			( () => { parser.parsers[name]( 0 ); } ).should.not.throw();
			( () => { parser.parsers[name]( 12 ); } ).should.not.throw();
			( () => { parser.parsers[name]( -32.53 ); } ).should.not.throw();
			( () => { parser.parsers[name]( "" ); } ).should.not.throw();
			( () => { parser.parsers[name]( "   " ); } ).should.not.throw();
			( () => { parser.parsers[name]( {} ); } ).should.not.throw();
			( () => { parser.parsers[name]( { parser: first } ); } ).should.not.throw();
			( () => { parser.parsers[name]( () => first ); } ).should.not.throw();

			( () => { parser.parsers[name]( [undefined] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [null] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [false] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [true] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [0] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [12] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [-32.53] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [""] ); } ).should.not.throw();
			( () => { parser.parsers[name]( ["   "] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [{}] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [{ parser: first }] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [() => first] ); } ).should.not.throw();

			( () => { parser.parsers[name]( [first] ); } ).should.not.throw();
		}

		for ( const [ start, num, add ] of [ [ 0, 1, true ], [ 100, 1, true ], [ 1, 1, true ], [ 1, 1, false ], [ 0, 100, false ] ] ) {
			( () => { parser.parsers.splice( start, num, ...add ? [undefined] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [null] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [false] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [true] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [0] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [12] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [-32.53] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [""] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? ["   "] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [{}] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [{ parser: first }] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [() => first] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [first] : [] ); } ).should.not.throw();

			if ( add ) {
				( () => { parser.parsers.splice( start, num, [undefined] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [null] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [false] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [true] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [0] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [12] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [-32.53] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [""] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, ["   "] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [{}] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [{ parser: first }] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [() => first] ); } ).should.not.throw();
			}
		}

		for ( const name of [ "pop", "shift" ] ) {
			parser.parsers.splice( 0, 100, first, second );

			( () => { parser.parsers[name](); } ).should.not.throw();
			( () => { parser.parsers[name](); } ).should.not.throw();
			( () => { parser.parsers[name](); } ).should.not.throw();
		}
	} );

	it( "fails on trying to parse data after having dropped all racing parsers", async() => {
		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );

		const goodParser = new CollectingRaceParser( [ first, second ] );
		await tryParser( goodParser, input ).should.be.resolved();

		const badParser = new CollectingRaceParser( [ first, second ] );
		badParser.parsers.splice( 0 );
		await tryParser( badParser, input ).should.be.rejectedWith( /empty list/i );
	} );

	it( "fails on trying to parse data after having corrupted list of racing parsers", async() => {
		for ( const fn of [
			/* eslint-disable no-param-reassign */
			parsers => { parsers[0] = null; },
			parsers => { parsers[1] = undefined; },
			parsers => { parsers[100] = false; },
			parsers => { parsers[2] = true; },
			parsers => { parsers[2] = 0; },
			parsers => { parsers[2] = 12; },
			parsers => { parsers[2] = -32.54; },
			parsers => { parsers[2] = ""; },
			parsers => { parsers[2] = "   "; },
			parsers => { parsers[2] = {}; },
			parsers => { parsers[2] = { parser: new SubstringParser( "baz" ) }; },
			parsers => { parsers[2] = () => new SubstringParser( "baz" ); },
			parsers => { parsers[2] = []; },
			parsers => { parsers[2] = [new SubstringParser( "baz" )]; },

			parsers => { parsers[0] = new RaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[1] = new RaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[2] = new RaceParser( [new SubstringParser( "baz" )] ); },

			parsers => { parsers[0] = new CollectingRaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[1] = new CollectingRaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[2] = new CollectingRaceParser( [new SubstringParser( "baz" )] ); },

			parsers => { parsers[0] = new CollectingSubstringParser( "baz" ); },
			parsers => { parsers[1] = new CollectingSubstringParser( "baz" ); },
			parsers => { parsers[2] = new CollectingSubstringParser( "baz" ); },

			parsers => { parsers[0] = new CollectingPatternParser( "baz" ); },
			parsers => { parsers[1] = new CollectingPatternParser( "baz" ); },
			parsers => { parsers[2] = new CollectingPatternParser( "baz" ); },
			/* eslint-enable no-param-reassign */
		] ) {
			const input = Buffer.from( "A foo and a bar are hiding in a jar." );
			const parser = new CollectingRaceParser( [ new SubstringParser( "foo" ), new SubstringParser( "bar" ) ] );

			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop

			( () => fn( parser.parsers ) ).should.not.throw();

			await tryParser( parser, input ).should.be.rejectedWith( /invalid parser/i ); // eslint-disable-line no-await-in-loop
		}
	} );

	it( "proceeds to parse data after properly adjusting list of racing parsers", async() => {
		const fix = new SubstringParser( "baz" );
		const dyn = new PatternParser( "baz" );

		for ( const fn of [
			/* eslint-disable no-param-reassign */
			parsers => { parsers[0] = fix; },
			parsers => { parsers[0] = dyn; },
			parsers => { parsers[1] = fix; },
			parsers => { parsers[1] = dyn; },
			parsers => { parsers.unshift( fix ); },
			parsers => { parsers.unshift( dyn ); },
			parsers => { parsers.push( fix ); },
			parsers => { parsers.push( dyn ); },
			parsers => { parsers.splice( 0, 0, fix ); },
			parsers => { parsers.splice( 0, 0, dyn ); },
			parsers => { parsers.splice( 0, 1, fix ); },
			parsers => { parsers.splice( 0, 1, dyn ); },
			parsers => { parsers.splice( 4, 0, fix ); },
			parsers => { parsers.splice( 4, 0, dyn ); },
			parsers => { parsers.splice( 1, 1, fix ); },
			parsers => { parsers.splice( 1, 1, dyn ); },
			/* eslint-enable no-param-reassign */
		] ) {
			const input = Buffer.from( "A foo and a bar are hiding in a jar." );
			const parser = new CollectingRaceParser( [ new SubstringParser( "foo" ), new SubstringParser( "bar" ) ] );

			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop

			( () => fn( parser.parsers ) ).should.not.throw();

			parser.switchCollected();
			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop
		}
	} );

	it( "finds matches of all attached parsers using match handlers per attached parser", async function() {
		this.timeout( 5000 );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo", { onMatch } );
		const second = new SubstringParser( "bar", { onMatch } );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new CollectingRaceParser( [ first, second ] );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", "bar" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 12 ] );
			}
		}
	} );

	it( "finds matches of all attached parsers using common match handler on race parser", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset, parser ) => {
			( parser === first || parser === second ).should.be.true();

			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new CollectingRaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", "bar" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 12 ] );
			}
		}
	} );

	it( "works with mixed set of substring and pattern parsers", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new PatternParser( " .+in" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset, parser ) => {
			( parser === first || parser === second ).should.be.true();

			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new CollectingRaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", " and a bar are hiding in" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 5 ] );
			}
		}
	} );

	it( "prefers earliest completely discovered match over partial matches when looking for matches capable of overlapping", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffixes" );
		const second = new SubstringParser( "fix" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new CollectingRaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "fix", "fix" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 13, 24 ] );
			}
		}
	} );

	it( "prefers earliest starting full match out of multiple full matches completely discovered simultaneously when looking for matches capable of overlapping", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix" );
		const second = new SubstringParser( "fix" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new CollectingRaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "suffix", "fix" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 10, 24 ] );
			}
		}
	} );

	it( "collects data preceding first match of lately set up race parser", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "fix" );
		const second = new PatternParser( "f+i" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new CollectingRaceParser( [ first, second ], { onMatch: () => new DisabledParser() } );
				const start = new SubstringParser( "oo", { onMatch: () => parser } );
				const collector = new BufferStream.Writer();
				parser.collected.pipe( collector );

				const { data } = await tryParser( start, input, size, delay ); // eslint-disable-line no-await-in-loop
				const collected = await collector.asPromise; // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				collected.toString().should.equal( "k into su" );
			}
		}
	} );
} );
