import { describe, it } from "mocha";
import "should";

import { BufferStream } from "../index.mjs";
import { createRandomData } from "./helper.mjs";

describe( "BufferStream.Reader", () => {
	it( "accepts string or Buffer as source", () => {
		( () => new BufferStream.Reader() ).should.throw();
		( () => new BufferStream.Reader( undefined ) ).should.throw();
		( () => new BufferStream.Reader( null ) ).should.throw();
		( () => new BufferStream.Reader( 0 ) ).should.throw();
		( () => new BufferStream.Reader( false ) ).should.throw();
		( () => new BufferStream.Reader( true ) ).should.throw();
		( () => new BufferStream.Reader( 1 ) ).should.throw();
		( () => new BufferStream.Reader( [] ) ).should.throw();
		( () => new BufferStream.Reader( [1] ) ).should.throw();
		( () => new BufferStream.Reader( ["1"] ) ).should.throw();
		( () => new BufferStream.Reader( {} ) ).should.throw();
		( () => new BufferStream.Reader( { data: "string" } ) ).should.throw();

		( () => new BufferStream.Reader( "string" ) ).should.not.throw();
		( () => new BufferStream.Reader( Buffer.from( "string" ) ) ).should.not.throw();
	} );

	it( "can be piped into writer with very little highWaterMark", done => {
		createRandomData( 1024 * 1024 )
			.then( ( { data } ) => {
				const source = new BufferStream.Reader( data, 128 * 1024, 0 );
				const sink = new BufferStream.Writer( { highWaterMark: 4096 } );

				source.pipe( sink );
				source.once( "error", cause => sink.destroy( cause ) );

				sink.once( "error", done );
				sink.once( "close", () => done() );
			} )
			.catch( done );
	} );
} );

describe( "BufferStream.Writer", () => {
	it( "accepts strings", done => {
		const sink = new BufferStream.Writer();

		sink.once( "error", done );
		sink.write( "test" );
		sink.end( done );
	} );

	it( "accepts buffers", done => {
		const sink = new BufferStream.Writer();

		sink.once( "error", done );
		sink.write( Buffer.from( "test" ) );
		sink.end( done );
	} );
} );
