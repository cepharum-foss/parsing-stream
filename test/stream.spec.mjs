import { Readable, Writable, Duplex, Transform } from "stream";

import { describe, it } from "mocha";
import "should";

import { Stream, Parser, Context } from "../index.mjs";
import { BufferReadStream, BufferWriteStream } from "../lib/buffer.mjs";

import { createRandomData, tryParser } from "./helper.mjs";


describe( "An instance of class Stream", () => {
	it( "is a readable stream", () => {
		const stream = new Stream();

		stream.should.be.instanceOf( Readable );
	} );

	it( "is a writable stream", () => {
		const stream = new Stream();

		stream.should.be.instanceOf( Writable );
	} );

	it( "hence is a duplex stream", () => {
		const stream = new Stream();

		stream.should.be.instanceOf( Duplex );
	} );

	it( "is a transform stream, actually", () => {
		const stream = new Stream();

		stream.should.be.instanceOf( Transform );
	} );

	it( "rejects to work in object-mode", () => {
		( () => new Stream( { objectMode: true } ) ).should.throw();
		( () => new Stream( { objectMode: false } ) ).should.not.throw();
		( () => new Stream() ).should.not.throw();
	} );

	it( "does not have any initially attached parser by default", () => {
		const stream = new Stream();

		( stream.parser == null ).should.be.true();
	} );

	it( "instantly exposes parser provided on construction", () => {
		const parser = new Parser();
		const stream = new Stream( {
			initialParser: parser
		} );

		stream.parser.should.equal( parser );
	} );

	it( "accepts parser assigned after creation", () => {
		const parser = new Parser();
		const stream = new Stream();

		( stream.parser == null ).should.be.true();
		stream.parser = parser;
		stream.parser.should.be.equal( parser );
	} );

	it( "accepts initial assignment of parser, only", () => {
		( () => new Stream( { initialParser: {} } ) ).should.throw();
		( () => new Stream( { initialParser() {} } ) ).should.throw();

		( () => new Stream( { initialParser: new Parser() } ) ).should.not.throw();
	} );

	it( "accepts late assignment of parser, only", () => {
		const stream = new Stream();

		( () => { stream.parser = {}; } ).should.throw();
		( () => { stream.parser = () => {}; } ).should.throw();

		( () => { stream.parser = new Parser(); } ).should.not.throw();
	} );

	it( "accepts late assignment of falsy _parser_, though, to detach any existing parser", () => {
		const stream = new Stream();

		stream.parser = new Parser();
		( stream.parser == null ).should.be.false();
		( () => { stream.parser = false; } ).should.not.throw();
		( stream.parser == null ).should.be.true();

		stream.parser = new Parser();
		( stream.parser == null ).should.be.false();
		( () => { stream.parser = 0; } ).should.not.throw();
		( stream.parser == null ).should.be.true();

		stream.parser = new Parser();
		( stream.parser == null ).should.be.false();
		( () => { stream.parser = null; } ).should.not.throw();
		( stream.parser == null ).should.be.true();

		stream.parser = new Parser();
		( stream.parser == null ).should.be.false();
		( () => { stream.parser = undefined; } ).should.not.throw();
		( stream.parser == null ).should.be.true();
	} );

	it( "exposes attached stream in context of parser", () => {
		const parserA = new Parser();
		const parserB = new Parser();

		const stream = new Stream( { initialParser: parserA } );

		parserA.stream.should.be.equal( stream );
		( parserB.stream == null ).should.be.true();

		stream.parser = parserB;

		( parserA.stream == null ).should.be.true();
		parserB.stream.should.be.equal( stream );

		stream.parser = null;

		( parserA.stream == null ).should.be.true();
		( parserB.stream == null ).should.be.true();
	} );

	it( "always exposes some context to use on parsing", () => {
		const stream = new Stream();

		stream.context.should.be.instanceOf( Context );
	} );

	it( "exposes same context explicitly provided on construction", () => {
		const context = new Context();
		const stream = new Stream( { context } );

		stream.context.should.equal( context );
	} );

	it( "rejects initial assignment of invalid context", () => {
		( () => new Stream( { context: {} } ) ).should.throw();
		( () => new Stream( { context() {} } ) ).should.throw();
	} );

	it( "rejects late assignment of any context", () => {
		const stream = new Stream();

		( () => { stream.context = new Context(); } ).should.throw();
		( () => { stream.context = {}; } ).should.throw();
		( () => { stream.context = () => {}; } ).should.throw();
	} );

	it( "passes any amount of data", async() => {
		const { data } = await createRandomData( 1024 * 1024 );

		const source = new BufferReadStream( data, 20480, 10 );
		const sink = new BufferWriteStream();
		const filter = new Stream();

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		const collected = await sink.asPromise;

		collected.length.should.be.equal( 1024 * 1024 );
		collected.equals( data ).should.be.true();
	} );

	it( "accepts string written to stream", done => {
		const filter = new Stream();

		filter.once( "error", done );
		filter.write( "test" );
		filter.end( done );
	} );

	it( "accepts Buffer written to stream", done => {
		const filter = new Stream();

		filter.once( "error", done );
		filter.write( Buffer.from( "test" ) );
		filter.end( done );
	} );

	it( "provides same context on integrating an attached parser", async() => {
		const { data } = await createRandomData( 256 * 1024 );

		const seenContexts = [];
		const parser = new Parser();
		parser.parse = ( context, buffer ) => {
			if ( seenContexts.indexOf( context ) < 0 ) {
				seenContexts.push( context );
			}

			return { parsed: [buffer] };
		};

		const source = new BufferReadStream( data, 8192, 10 );
		const sink = new BufferWriteStream();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		await sink.asPromise;

		seenContexts.should.have.length( 1 );
	} );

	it( "exposes parser instance as `this` on integrating an attached parser", async() => {
		const { data } = await createRandomData( 256 * 1024 );

		const seenParsers = [];
		const parser = new Parser();
		parser.parse = function( _, buffer ) {
			if ( seenParsers.indexOf( this ) < 0 ) {
				seenParsers.push( this );
			}

			return { parsed: [buffer] };
		};

		const source = new BufferReadStream( data, 8192, 10 );
		const sink = new BufferWriteStream();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		await sink.asPromise;

		seenParsers.should.have.length( 1 );
	} );

	it( "presents every chunk of streamed data to an attached parser", async() => {
		const { data } = await createRandomData( 256 * 1024 );

		const seenChunks = [];
		const parser = new Parser();
		parser.parse = ( context, buffer ) => {
			seenChunks.push( buffer );

			return { parsed: [buffer] };
		};

		const source = new BufferReadStream( data, 8192, 10 );
		const sink = new BufferWriteStream();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		await sink.asPromise;

		Buffer.concat( seenChunks ).equals( data ).should.be.true();
	} );

	it( "emits error when parser fails on flushing stream", async() => {
		const { data } = await createRandomData( 256 * 1024 );

		const parser = new Parser();
		parser.parse = ( context, buffer, atOffset, isLast ) => {
			if ( isLast ) {
				throw new Error( "fail on flushing" );
			}

			return { parsed: [buffer] };
		};

		await tryParser( parser, data ).should.be.rejected();
	} );

	it( "supports switching attached parser for processing upcoming data via result of integrating an attached parser", async() => {
		const { data } = await createRandomData( 256 * 1024 );

		const chunksOfParser = [];
		const parser = new Parser();
		parser.parse = ( context, buffer ) => {
			chunksOfParser.push( buffer );

			return { parsed: [buffer], parser: altParser };
		};

		const chunksOfAltParser = [];
		const altParser = new Parser();
		altParser.parse = ( context, buffer ) => {
			chunksOfAltParser.push( buffer );

			return { parsed: [buffer], parser: parser };
		};

		const source = new BufferReadStream( data, 8192, 10 );
		const sink = new BufferWriteStream();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		await sink.asPromise;

		Math.abs( chunksOfParser.length - chunksOfAltParser.length ).should.be.lessThanOrEqual( 1 );

		const merged = new Array( chunksOfParser.length + chunksOfAltParser.length );
		chunksOfParser.forEach( ( chunk, i ) => { merged[2 * i] = chunk; } );
		chunksOfAltParser.forEach( ( chunk, i ) => { merged[( 2 * i ) + 1] = chunk; } );
		Buffer.concat( merged ).equals( data ).should.be.true();
	} );
} );
