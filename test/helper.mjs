import { randomFill } from "crypto";
import { BufferStream, PatternParser, Stream } from "../index.mjs";


/**
 * Creates buffer containing provided static buffers at random positions in a
 * random stream of data.
 *
 * @param {number} numOctets number of octets in resulting buffer
 * @param {Buffer[]} subs sequence of subordinated buffers to inject into buffer
 * @param {boolean} random set true to have subordinated buffers injected in random order
 * @returns {Promise<{data: Buffer, offsets: number[]}>} resulting buffer and offsets per injected subordinated buffer
 */
export async function createRandomData( numOctets, subs = [], random = false ) {
	const data = await new Promise( ( resolve, reject ) => {
		randomFill( Buffer.alloc( numOctets ), ( error, buffer ) => {
			if ( error ) {
				reject( error );
			} else {
				resolve( buffer );
			}
		} );
	} );

	const numStrings = subs ? subs.length : 0;
	const offsets = new Array( numStrings );

	( subs || [] ).forEach( random ? randomDeploy : sequentialDeploy );

	return { data, offsets };

	/**
	 * Injects given buffer from list of subordinated buffers into data at
	 * random offset.
	 *
	 * @param {Buffer} buffer current buffer to inject
	 * @param {number} index index of buffer into list of buffers to inject
	 * @returns {void}
	 */
	function randomDeploy( buffer, index ) {
		if ( !Buffer.isBuffer( buffer ) ) {
			throw new TypeError( "subs must consist of Buffer instances, only" );
		}

		const max = data.length - buffer.length;

		for ( let j, i = 0; i < 100; i++ ) {
			const offset = Math.floor( Math.random() * max );

			for ( j = 0; j < index; j++ ) {
				const o = offsets[j];

				if ( offset >= o - buffer.length && offset < o + subs[j].length ) {
					break;
				}
			}

			if ( j <= index ) {
				offsets[index] = offset;

				buffer.copy( data, offset );

				return;
			}
		}

		throw new TypeError( `failed to find position for injecting sub #${index}` );
	}

	/**
	 * Injects given buffer from list of subordinated buffers into data at
	 * random offset though obeying sequence of provided buffers.
	 *
	 * @param {Buffer} buffer current buffer to inject
	 * @param {number} index index of buffer into list of buffers to inject
	 * @param {Buffer[]} buffers list of buffers to inject into data
	 * @returns {void}
	 */
	function sequentialDeploy( buffer, index, buffers ) {
		if ( !Buffer.isBuffer( buffer ) ) {
			throw new TypeError( "subs must consist of Buffer instances, only" );
		}

		const start = index > 0 ? offsets[index - 1] + buffers[index - 1].length : 0;

		let max = data.length;
		for ( let i = index; i < buffers.length; i++ ) {
			max -= buffers[i].length;
		}

		const offset = Math.floor( Math.random() * ( max - start ) );

		offsets[index] = offset;

		buffer.copy( data, offset );
	}
}

/**
 * Tries to parse provided haystack of data feeding a stream with different
 * chunk sizes.
 *
 * @param {Buffer|string} haystack
 * @param {() => Promise<Parser>} parserFn generates stream parser instance to attach to stream initially
 * @param {(parser:Parser, stream:ParsingStream, sunkData:Buffer) => Promise<void>} assertFn callback invoked to assert result
 * @param {int} stepSize number of octets to increase stream-feeding chunk size per iteration
 * @returns {Promise<void>}
 */
export async function tryAllChunkSizes( haystack, parserFn, assertFn, stepSize = undefined ) {
	const step = stepSize ? stepSize : Math.max( 1, Math.floor( haystack.length / 100 ) );

	for ( let size = 1; size < haystack.length; size += step ) {
		const parser = await parserFn(); // eslint-disable-line no-await-in-loop
		const { filter, data } = await tryParser( parser, haystack, size ); // eslint-disable-line no-await-in-loop

		data.should.be.instanceOf( Buffer );

		await assertFn( parser, filter, data ); // eslint-disable-line no-await-in-loop
	}
}

/**
 * Tries to parse provided haystack of data feeding a stream with different
 * chunk sizes.
 *
 * @param {Buffer|string} haystack
 * @param {Parser} parser stream parser instance to attach to stream initially
 * @param {int} chunkSize number of octets per stream-feeding chunk
 * @param {delay} delay delay in ms per chunk written to stream
 * @returns {Promise<{filter: Stream, data: Buffer}>} used filter stream and its output data
 */
export async function tryParser( parser, haystack, chunkSize = 7, delay = 1 ) {
	const source = new BufferStream.Reader( Buffer.isBuffer( haystack ) ? haystack : Buffer.from( haystack, "ascii" ), chunkSize, delay );
	const { filter, sink } = await pipe( source, parser );
	const data = await sink.asPromise;

	data.should.be.instanceOf( Buffer );

	return { filter, data };
}

/**
 * Pipes data of source through filter stream with provided parser attached
 * initially delivering that filter stream and a sink collecting its output.
 *
 * @param {Readable} source stream of data to be parsed
 * @param {Parser} parser stream parser instance to attach to stream initially
 * @param {Context} context initial context of filtering stream
 * @returns {Promise<{filter: Stream, sink: BufferStream.Writer}>} used filter stream and sink collecting its output
 */
export function pipe( source, parser, context = null ) {
	const sink = new BufferStream.Writer();
	const filter = new Stream( { initialParser: parser, context } );

	source.pipe( filter ).pipe( sink );

	source.once( "error", cause => filter.destroy( cause ) );
	filter.once( "error", cause => sink.destroy( cause ) );

	return { filter, sink };
}

/**
 * Generates parser instance for searching a stream of data for occurrences of
 * given pattern.
 *
 * @param {string} pattern source of pattern to search for
 * @returns {Promise<PatternParser>} stream parser capable of discovering octets matching pattern
 */
export function createParser( pattern ) {
	const parser = new PatternParser( pattern );

	parser.onMatch = function( context, buffers, atOffset ) {
		buffers.should.be.Array().which.is.not.empty();
		atOffset.should.be.Number().which.is.above( -1 );

		if ( !Array.isArray( context.matches ) ) {
			context.matches = []; // eslint-disable-line no-param-reassign
		}

		context.matches.push( { match: this.data, offset: atOffset } );
	};

	return Promise.resolve( parser );
}

/**
 * Implements template used to test different kinds of patterns used for parsing
 * stream.
 *
 * @param {string} message data to be streamed for searching
 * @param {string} pattern source of pattern to search for
 * @param {string[]} expectedMatches lists expected matches' content
 * @param {int[]} expectedOffsets lists expected matches' offsets into octets of message
 * @returns {Promise<void>} promises test passed
 */
export async function testParser( message, pattern, expectedMatches, expectedOffsets ) {
	const parserFn = () => createParser( pattern );
	const assertFn = ( parser, stream, data ) => {
		data.toString().should.be.equal( message );

		const { matches } = stream.context;

		matches.should.be.an.Array();
		matches.map( i => i.match.toString() ).should.be.deepEqual( expectedMatches );
		matches.map( i => i.offset ).should.be.deepEqual( expectedOffsets );
		matches.forEach( i => data.slice( i.offset, i.offset + i.match.length ).equals( i.match ).should.be.true() );
	};

	await tryAllChunkSizes( message, parserFn, assertFn );
}
