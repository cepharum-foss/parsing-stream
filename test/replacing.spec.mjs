import { describe, it } from "mocha";
import "should";

import { Stream, BufferStream, CollectingPatternParser, PatternParser } from "../index.mjs";


describe( "CollectingPatternParser", () => {
	it( "can be used to replace mark-up in a stream", async() => {
		const data = {
			name: "John Doe",
			direct: "you",
			count: 1234,
			where: "not used",
			"where?": "not used",
		};

		const start = new PatternParser( "\\{{2}" );
		const end = new CollectingPatternParser( "\\}{2}", { tee: false } );

		start.onMatch = ( context, buffers ) => {
			context.start = buffers.slice(); // eslint-disable-line no-param-reassign

			// delete all provided buffers describing match to exclude it from output
			buffers.splice( 0 );

			context.markup = new BufferStream.Writer(); // eslint-disable-line no-param-reassign
			end.switchCollected().collected.pipe( context.markup );
			end.collected.once( "error", cause => context.markup.destroy( cause ) );

			return end;
		};

		end.onMatch = async( context, buffers ) => {
			end.switchCollected();
			const markup = await context.markup.asPromise;

			// replace buffers describing end of marker with name of value
			buffers.splice( 0, buffers.length, Buffer.from( String( data[markup.toString().trim()] || "" ) ) );

			return start;
		};

		const source = new BufferStream.Reader( "Hello {{ name }}, nice to meet {{direct}}! There are {{ count}} other people {where?}.", 7, 10 );
		const sink = new BufferStream.Writer();
		const stream = new Stream( { initialParser: start } );

		source.pipe( stream ).pipe( sink );

		source.on( "error", cause => stream.destroy( cause ) );
		stream.on( "error", cause => sink.destroy( cause ) );

		const result = await sink.asPromise;

		result.toString().should.equal( "Hello John Doe, nice to meet you! There are 1234 other people {where?}." );
	} );
} );
