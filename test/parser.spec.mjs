import { describe, it } from "mocha";
import "should";

import {
	Stream,
	Parser,
	BufferStream,
	DisabledParser,
	Context,
	SubstringParser
} from "../index.mjs";
import { pipe, tryParser } from "./helper.mjs";


describe( "An instance of class Parser", () => {
	it( "can be attached to stream initially", () => {
		const parser = new Parser();
		const stream = new Stream( { initialParser: parser } );

		stream.parser.should.be.equal( parser );
	} );

	it( "can be attached to stream after creation", () => {
		const parser = new Parser();
		const stream = new Stream();

		( stream.parser == null ).should.be.true();
		stream.parser = parser;
		stream.parser.should.be.equal( parser );
	} );

	it( "relies on custom method `parse()` invoked by stream for integrating parser", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			context.buffers.push( buffer );
			context.offsets.push( atOffset );

			return this.signalNoMatch( buffer );
		};

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const { filter, sink } = pipe( source, parser, Object.assign( new Context(), { buffers: [], offsets: [] } ) );
		const data = await sink.asPromise;

		data.toString().should.be.equal( "hello world!" );

		Buffer.concat( filter.context.buffers ).toString().should.be.equal( "hello world!" );
		filter.context.offsets[0].should.be.equal( 0 );
	} );

	it( "rejects parsing stream without custom `parse()` method", () => {
		const parser = new Parser();

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const sink = new BufferStream.Writer();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		return sink.asPromise.should.be.rejected();
	} );

	it( "requires match handler to be function if provided", () => {
		( () => { new Parser().onMatch = true; } ).should.throw();
		( () => { new Parser().onMatch = {}; } ).should.throw();
		( () => { new Parser().onMatch = []; } ).should.throw();
		( () => { new Parser().onMatch = new Date(); } ).should.throw();

		( () => { new Parser().onMatch = () => {}; } ).should.not.throw();
	} );

	it( "must return valid result descriptor from callback invoked for integrating parser", async() => {
		const parser = new Parser();
		parser.parse = () => {};

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const sink = new BufferStream.Writer();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		await Promise.all( [ filter.asPromise, sink.asPromise ] ).should.be.rejected();
	} );

	it( "has helper method `signalNoMatch()` for use in `Parser#parse()` to indicate lack of any partial or full match in recently seen data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer ) {
			this.signalNoMatch.should.be.Function().which.has.length( 1 );

			return this.signalNoMatch( buffer );
		};

		await helper( "I say hello world!", parser, 2, 10 );
	} );

	it( "has helper method `signalPartialMatch()` for use in `Parser#parse()` to indicate partial match in recently seen data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer ) {
			this.signalPartialMatch.should.be.Function().which.has.length( 2 );

			// indicate "no match" by indicating start of partial match beyond end of any seen data
			return this.signalPartialMatch( buffer, buffer.length );
		};

		await helper( "I say hello world!", parser, 2, 10 );
	} );

	it( "rejects indication of partial match beyond beginning of any held back data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer ) {
			return this.signalPartialMatch( buffer, -1 );
		};

		await helper( "I say hello world!", parser, 2, 10 ).should.be.rejected();
	} );

	it( "has helper method `signalFullMatch()` for use in `Parser#parse()` to indicate full match in recently seen data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			this.signalFullMatch.should.be.Function().which.has.length( 5 );

			// always indicates whole provided `buffer` representing a full match
			return this.signalFullMatch( context, buffer, atOffset, 0, buffer.length );
		};

		await helper( "I say hello world!", parser, 2, 10 );
	} );

	it( "rejects indication of full match beyond beginning of any held back data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			return this.signalFullMatch( context, buffer, atOffset, -1, 0 );
		};

		await helper( "I say hello world!", parser, 2, 10 ).should.be.rejected();
	} );

	it( "rejects indication of full match with endIndex pointing before firstIndex", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			return this.signalFullMatch( context, buffer, atOffset, 1, 0 );
		};

		await helper( "I say hello world!", parser, 2, 10 ).should.be.rejected();
	} );

	describe( "has method onAttach() which", () => {
		it( "is a function", () => {
			new Parser().onAttach.should.be.Function();
		} );

		it( "requires one parameter", () => {
			new Parser().onAttach.should.have.length( 1 );
		} );

		it( "is invoked when creating stream with this parser as initial one", () => {
			const parser = new Parser();
			let attached;

			parser.onAttach = s => { attached = s; };

			const stream = new Stream( { initialParser: parser } );

			attached.should.equal( stream );
		} );

		it( "is invoked when assigning parser to stream after its creation", () => {
			const parser = new Parser();
			let attached;

			parser.onAttach = s => { attached = s; };

			const stream = new Stream();
			stream.parser = parser;

			attached.should.equal( stream );
		} );

		it( "is invoked when switching parser after match", async() => {
			const changed = new DisabledParser();
			const start = new SubstringParser( "foo", { onMatch: () => changed } );

			let attached;
			changed.onAttach = s => { attached = s; };

			const { filter } = await tryParser( start, "My name is foo bar!" );
			attached.should.equal( filter );
		} );
	} );

	describe( "has method onDetach() which", () => {
		it( "is a function", () => {
			new Parser().onDetach.should.be.Function().which.has.length( 0 );
		} );

		it( "is not invoked when creating stream with this parser as initial one", () => {
			const parser = new Parser();
			let called = 0;
			parser.onDetach = () => called++;

			new Stream( { initialParser: parser } );

			called.should.equal( 0 );
		} );

		it( "is invoked when assigning another parser to stream after its creation", () => {
			const parser = new Parser();
			let called = 0;
			parser.onDetach = () => called++;

			new Stream( { initialParser: parser } ).parser = new DisabledParser();

			called.should.equal( 1 );
		} );

		it( "is invoked when switching parser after match", async() => {
			const changed = new DisabledParser();
			const start = new SubstringParser( "foo", { onMatch: () => changed } );

			let called = 0;
			start.onDetach = () => called++;

			await tryParser( start, "My name is foo bar!" );
			called.should.equal( 1 );
		} );
	} );

	describe( "has property `heldBuffers` which", () => {
		it( "is a list", () => {
			new Parser().heldBuffers.should.be.Array();
		} );

		it( "can be replaced with another list of Buffer instances", () => {
			( () => { new Parser().heldBuffers = undefined; } ).should.throw();
			( () => { new Parser().heldBuffers = null; } ).should.throw();
			( () => { new Parser().heldBuffers = false; } ).should.throw();
			( () => { new Parser().heldBuffers = true; } ).should.throw();
			( () => { new Parser().heldBuffers = 0; } ).should.throw();
			( () => { new Parser().heldBuffers = 43; } ).should.throw();
			( () => { new Parser().heldBuffers = -53.987; } ).should.throw();
			( () => { new Parser().heldBuffers = ""; } ).should.throw();
			( () => { new Parser().heldBuffers = "foo"; } ).should.throw();
			( () => { new Parser().heldBuffers = ["foo"]; } ).should.throw();
			( () => { new Parser().heldBuffers = [ Buffer.from( "foo" ), "foo", Buffer.from( "foo" ) ]; } ).should.throw();
			( () => { new Parser().heldBuffers = {}; } ).should.throw();
			( () => { new Parser().heldBuffers = { buffer: Buffer.from( "foo" ) }; } ).should.throw();
			( () => { new Parser().heldBuffers = { list: [Buffer.from( "foo" )] }; } ).should.throw();
			( () => { new Parser().heldBuffers = Buffer.from( "foo" ); } ).should.throw();
			( () => { new Parser().heldBuffers = () => []; } ).should.throw();
			( () => { new Parser().heldBuffers = () => [Buffer.from( "foo" )]; } ).should.throw();

			( () => { new Parser().heldBuffers = []; } ).should.not.throw();
			( () => { new Parser().heldBuffers = [Buffer.from( "foo" )]; } ).should.not.throw();
		} );
	} );

	it( "has property `wasMatchingPartially` exposing live flag indicating whether there was an incomplete match at end of previously processed chunk", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer ) {
			if ( context.holdBack ) {
				this.wasMatchingPartially.should.be.true();

				context.holdBack = false; // eslint-disable-line no-param-reassign
				return this.signalNoMatch( buffer );
			}

			this.wasMatchingPartially.should.be.false();

			context.holdBack = true; // eslint-disable-line no-param-reassign
			return this.signalPartialMatch( buffer, 0 );
		};

		const source = new BufferStream.Reader( Buffer.from( "I say hello world!" ), 2, 10 );
		const { sink } = pipe( source, parser );

		await sink.asPromise;
	} );

	it( "has property `previouslyMatchingOctetsCount` exposing number of octets in previous chunks assumed to be beginning of a match", async() => {
		const message = "I say hello world!";

		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			this.previouslyMatchingOctetsCount.should.be.equal( atOffset );

			return this.signalPartialMatch( buffer, -this.previouslyMatchingOctetsCount );
		};

		const { output } = await helper( message, parser, 2, 10 );

		output.toString().should.equal( message );
	} );

	it( "has property `data` exposing joined buffer with octets seen in previous chunks assumed to be beginning of a match", async() => {
		const message = "I say hello world!";

		const parser = new Parser();
		parser.parse = function( context, buffer ) {
			this.data.toString().should.equal( message.substring( 0, this.previouslyMatchingOctetsCount ) );

			return this.signalPartialMatch( buffer, -this.previouslyMatchingOctetsCount );
		};

		await helper( message, parser, 2, 10 );
	} );

	it( "discovers a match spanning multiple seen chunks of streamed data", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			if ( buffer.equals( Buffer.from( "he" ) ) ) {
				this.previouslyMatchingOctetsCount.should.be.equal( 0 );
				return this.signalPartialMatch( buffer, 0 );
			}

			if ( buffer.equals( Buffer.from( "ll" ) ) ) {
				this.previouslyMatchingOctetsCount.should.be.equal( 2 );
				return this.signalPartialMatch( buffer, -this.previouslyMatchingOctetsCount );
			}

			if ( buffer.equals( Buffer.from( "o " ) ) ) {
				this.previouslyMatchingOctetsCount.should.be.equal( 4 );
				return this.signalFullMatch( context, buffer, atOffset, -this.previouslyMatchingOctetsCount + 1, 1 );
			}

			return this.signalNoMatch( buffer );
		};

		parser.onMatch = ( context, buffers, atOffset ) => {
			atOffset.should.be.equal( 7 );

			buffers.should.be.an.Array().which.has.length( 3 );
			buffers[0].equals( Buffer.from( "e" ) ).should.be.true();
			buffers[1].equals( Buffer.from( "ll" ) ).should.be.true();
			buffers[2].equals( Buffer.from( "o" ) ).should.be.true();
		};

		const source = new BufferStream.Reader( Buffer.from( "I say hello world!" ), 2, 10 );
		const { sink } = pipe( source, parser );
		const data = await sink.asPromise;

		data.equals( Buffer.from( "I say hello world!" ) ).should.be.true();
	} );

	it( "invokes existing match handler on every match", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			const heldBack = this.previouslyMatchingOctetsCount;

			if ( heldBack + buffer.length > 1 ) {
				// indicate another two octets matching
				return this.signalFullMatch( context, buffer, atOffset, -heldBack, -heldBack + 2 );
			}

			return this.signalPartialMatch( buffer, -heldBack );
		};

		parser.onMatch = ( context, matchBuffers, atOffset ) => {
			context.should.be.equal( filter.context );
			matchBuffers.should.be.an.Array().which.is.not.empty();
			( atOffset % 2 ).should.be.equal( 0 );

			const match = Buffer.concat( matchBuffers );
			match.length.should.be.equal( 2 );

			( context.matches || ( context.matches = [] ) ).push( match ); // eslint-disable-line no-param-reassign
		};

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const { filter, sink } = pipe( source, parser );
		const data = await sink.asPromise;

		data.toString().should.be.equal( "hello world!" );

		filter.context.matches.should.be.an.Array().which.is.not.empty();
	} );

	it( "supports derived parsers with custom match handler invoked on every match", async() => {
		class MyParser extends Parser {
			parse( context, buffer, atOffset ) {
				const heldBack = this.previouslyMatchingOctetsCount;

				if ( heldBack + buffer.length > 1 ) {
					// indicate another two octets matching
					return this.signalFullMatch( context, buffer, atOffset, -heldBack, -heldBack + 2 );
				}

				return this.signalPartialMatch( buffer, -heldBack );
			}

			onMatch( context, matchBuffers, atOffset ) {
				context.should.be.equal( filter.context );
				matchBuffers.should.be.an.Array().which.is.not.empty();
				( atOffset % 2 ).should.be.equal( 0 );

				const match = Buffer.concat( matchBuffers );
				match.length.should.be.equal( 2 );

				( context.matches || ( context.matches = [] ) ).push( match ); // eslint-disable-line no-param-reassign
			}
		}

		const parser = new MyParser();

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const sink = new BufferStream.Writer();
		const filter = new Stream( { initialParser: parser } );

		source.pipe( filter ).pipe( sink );

		source.once( "error", cause => filter.destroy( cause ) );
		filter.once( "error", cause => sink.destroy( cause ) );

		const data = await sink.asPromise;
		data.toString().should.be.equal( "hello world!" );

		filter.context.matches.should.be.an.Array().which.is.not.empty();
	} );

	it( "supports switching attached parser as a result of handling matches", async() => {
		function triggerMatch( context, buffer, atOffset ) {
			return this.signalFullMatch( context, buffer, atOffset, 0, 2 );
		}

		const parser = new Parser();
		parser.parse = triggerMatch;

		parser.onMatch = ( context, matchBuffers, atOffset ) => {
			( context.matches || ( context.matches = [] ) ).push( { // eslint-disable-line no-param-reassign
				match: Buffer.concat( matchBuffers ),
				offset: atOffset,
				alt: false,
			} );

			return altParser;
		};

		const altParser = new Parser();
		altParser.parse = triggerMatch;

		altParser.onMatch = ( context, matchBuffers, atOffset ) => {
			( context.matches || ( context.matches = [] ) ).push( { // eslint-disable-line no-param-reassign
				match: Buffer.concat( matchBuffers ),
				offset: atOffset,
				alt: true,
			} );

			return Promise.resolve( parser );
		};

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const { filter, sink } = pipe( source, parser );
		const data = await sink.asPromise;

		data.toString().should.be.equal( "hello world!" );

		filter.context.matches.should.be.an.Array().which.is.not.empty();
		filter.context.matches.forEach( ( { offset, alt }, index ) => {
			offset.should.be.equal( 2 * index );

			if ( index % 2 === 0 ) {
				alt.should.be.false();
			} else {
				alt.should.be.true();
			}
		} );

		Buffer.concat( filter.context.matches.map( i => i.match ) ).toString().should.be.equal( "hello world!" );
	} );

	it( "supports adjusting buffers containing matching sequence to adjust the passing stream of octets accordingly", async() => {
		const parser = new Parser();
		parser.parse = getSubstringParser( 2, 4 );

		parser.onMatch = ( context, buffers ) => {
			buffers.splice( 0, buffers.length, Buffer.from( "**" ), Buffer.from( "*" ) );

			return altParser;
		};

		const altParser = new Parser();
		altParser.parse = getSubstringParser( 2, 4 );

		altParser.onMatch = ( context, buffers ) => {
			buffers.splice( 0, 0, Buffer.from( "-" ) );

			return parser;
		};

		const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
		const { sink } = pipe( source, parser );
		const data = await sink.asPromise;

		data.toString().should.be.equal( "he***o -worl***" );
	} );

	it( "ignores falsy result for switching/detaching parser in `Parser#onMatch()`", async() => {
		await Promise.all( [ undefined, null, false ].map( async altParser => {
			const parser = new Parser();
			parser.parse = function( context, buffer, atOffset ) {
				return this.signalFullMatch( context, buffer, atOffset, 0, 2 );
			};

			parser.onMatch = context => {
				context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

				return altParser;
			};

			const { context } = await helper( "hello world!", parser );

			context.matches.should.be.greaterThan( 1 );
		} ) );
	} );

	it( "ignores falsy request in parser result for switching/detaching parser in `Parser#parse()`", async() => {
		await Promise.all( [ undefined, null, false ].map( async altParser => {
			const parser = new Parser();
			parser.parse = function( context, buffer, atOffset ) {
				context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

				return this.signalFullMatch( context, buffer, atOffset, 0, 2 )
					.then( result => {
						result.parser = altParser; // eslint-disable-line no-param-reassign
						return result;
					} );
			};

			const { context } = await helper( "hello world!", parser );

			context.matches.should.be.greaterThan( 1 );
		} ) );
	} );

	it( "requires Parser instance for switching parser in `Parser#onMatch()`", async() => {
		await Promise.all( [ {}, [], () => {}, new Date() ].map( async altParser => {
			const parser = new Parser();
			parser.parse = function( context, buffer, atOffset ) {
				return this.signalFullMatch( context, buffer, atOffset, 0, 2 );
			};

			parser.onMatch = context => {
				context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

				return altParser;
			};

			const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
			const sink = new BufferStream.Writer();
			const filter = new Stream( { initialParser: parser } );

			source.pipe( filter ).pipe( sink );

			source.once( "error", cause => filter.destroy( cause ) );
			filter.once( "error", cause => sink.destroy( cause ) );

			await Promise.all( [ filter.asPromise, sink.asPromise ] ).should.be.rejected();
		} ) );
	} );

	it( "requires Parser instance for switching parser in `Parser#parse()`", async() => {
		await Promise.all( [ {}, [], () => {}, new Date() ].map( async altParser => {
			const parser = new Parser();
			parser.parse = function( context, buffer, atOffset ) {
				context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

				return this.signalFullMatch( context, buffer, atOffset, 0, 2 )
					.then( result => {
						result.parser = altParser; // eslint-disable-line no-param-reassign
						return result;
					} );
			};

			const source = new BufferStream.Reader( Buffer.from( "hello world!" ) );
			const sink = new BufferStream.Writer();
			const filter = new Stream( { initialParser: parser } );

			source.pipe( filter ).pipe( sink );

			source.once( "error", cause => filter.destroy( cause ) );
			filter.once( "error", cause => sink.destroy( cause ) );

			await Promise.all( [ filter.asPromise, sink.asPromise ] ).should.be.rejected();
		} ) );
	} );

	it( "accepts special DisabledParser for detaching any parser in `Parser#onMatch()`", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			return this.signalFullMatch( context, buffer, atOffset, 0, 2 );
		};

		parser.onMatch = context => {
			context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

			return new DisabledParser();
		};

		const { context } = await helper( "hello world!", parser );

		context.matches.should.not.be.greaterThan( 1 );
	} );

	it( "accepts special DisabledParser in parser result for detaching parser in `Parser#parse()`", async() => {
		const parser = new Parser();
		parser.parse = function( context, buffer, atOffset ) {
			context.matches = ( context.matches || 0 ) + 1; // eslint-disable-line no-param-reassign

			return this.signalFullMatch( context, buffer, atOffset, 0, 2 )
				.then( result => {
					result.parser = new DisabledParser(); // eslint-disable-line no-param-reassign
					return result;
				} );
		};

		const { context } = await helper( "hello world!", parser );

		context.matches.should.not.be.greaterThan( 1 );
	} );
} );

/**
 * Pushes provided input through stream with given parser attached.
 *
 * @param {Buffer|string} input data to push through stream
 * @param {Parser} parser parser to be attached to stream initially
 * @param {int} readChunkSize number of octet to write into stream at once
 * @param {int} delay milliseconds to wait before writing another chunk of dat
 * @returns {Promise<{output: *, context: {value}}>} output of stream and its context available to attached parser(s)
 */
async function helper( input, parser, readChunkSize = 10, delay = 10 ) {
	const source = new BufferStream.Reader( input, readChunkSize, delay );
	const { filter, sink } = await pipe( source, parser );
	const output = await sink.asPromise;

	return { output, context: filter.context };
}

/**
 * Generates callback suitable for use as parse() method Parser to match octets
 * in slice as described.
 *
 * @param {int} firstIndex index into octets seen by this callback for starting slice of extracted octets
 * @param {int} endIndex index into octets seen by this callback for stopping slice of extracted octets
 * @returns {(function(*=, *=, *=): (*))|*} parser implementation
 */
function getSubstringParser( firstIndex, endIndex ) {
	return function( context, buffer, atOffset ) {
		// matches third and fourth octet of data seen by current parser (!!), only

		const heldBack = this.previouslyMatchingOctetsCount;
		if ( heldBack + buffer.length >= endIndex ) {
			return this.signalFullMatch( context, buffer, atOffset, firstIndex - heldBack, endIndex - heldBack );
		}

		return this.signalPartialMatch( buffer, -heldBack );
	};
}
