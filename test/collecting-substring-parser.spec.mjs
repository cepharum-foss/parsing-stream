import { Readable } from "stream";

import { describe, it } from "mocha";
import "should";

import {
	Stream,
	BufferStream,
	CollectingSubstringParser
} from "../index.mjs";


describe( "An instance of class CollectingSubstringParser", () => {
	it( "can be attached to stream initially", () => {
		const parser = new CollectingSubstringParser( Buffer.from( "foo" ) );
		const stream = new Stream( { initialParser: parser } );

		stream.parser.should.be.equal( parser );
	} );

	it( "can be attached to stream after creation", () => {
		const parser = new CollectingSubstringParser( Buffer.from( "foo" ) );
		const stream = new Stream();

		( stream.parser == null ).should.be.true();
		stream.parser = parser;
		stream.parser.should.be.equal( parser );
	} );

	it( "exposes `collected` data as readable stream", () => {
		const parser = new CollectingSubstringParser( Buffer.from( "foo" ) );

		parser.should.have.property( "collected" ).which.is.an.instanceof( Readable );
	} );

	it( "separately provides all non-matching octets in `collected` stream", async() => {
		const parser = new CollectingSubstringParser( "bar" );
		const stream = new Stream( { initialParser: parser } );

		const collector = parser.collected.pipe( new BufferStream.Writer() );
		const output = stream.pipe( new BufferStream.Writer() );

		stream.end( Buffer.from( "foo bar baz" ) );

		( await collector.asPromise ).should.deepEqual( Buffer.from( "foo  baz" ) );
		( await output.asPromise ).should.deepEqual( Buffer.from( "foo bar baz" ) );
	} );

	it( "provides all non-matching octets in `collected` stream, only, on demand", async() => {
		const parser = new CollectingSubstringParser( "bar", { tee: false } );
		const stream = new Stream( { initialParser: parser } );

		const collector = parser.collected.pipe( new BufferStream.Writer() );
		const output = stream.pipe( new BufferStream.Writer() );

		stream.end( Buffer.from( "foo bar baz" ) );

		( await collector.asPromise ).should.deepEqual( Buffer.from( "foo  baz" ) );
		( await output.asPromise ).should.deepEqual( Buffer.from( "bar" ) );
	} );

	it( "has method `switchCollected()` to close stream of non-matching octets and to replace it with another stream", async() => {
		const parser = new CollectingSubstringParser( "bar" );
		const stream = new Stream( { initialParser: parser } );

		const collector = parser.collected.pipe( new BufferStream.Writer() );
		const output = stream.pipe( new BufferStream.Writer() );

		parser.switchCollected.should.be.Function();
		parser.onMatch = () => {
			parser.switchCollected();
		};

		stream.end( Buffer.from( "foo bar baz" ) );

		( await collector.asPromise ).should.deepEqual( Buffer.from( "foo " ) );
		( await output.asPromise ).should.deepEqual( Buffer.from( "foo bar baz" ) );
		( await parser.collected.pipe( new BufferStream.Writer() ).asPromise ).should.deepEqual( Buffer.from( " baz" ) );
	} );

	it( "collects octets following last match, too", async() => {
		const parser = new CollectingSubstringParser( "bar" );
		const stream = new Stream( { initialParser: parser } );

		const collector = parser.collected.pipe( new BufferStream.Writer() );
		const output = stream.pipe( new BufferStream.Writer() );

		stream.write( Buffer.from( "foo ba" ) );
		stream.end( Buffer.from( "r baz" ) );

		( await collector.asPromise ).should.deepEqual( Buffer.from( "foo  baz" ) );
		( await output.asPromise ).should.deepEqual( Buffer.from( "foo bar baz" ) );
	} );

	it( "collects octets when matching last on final data", async() => {
		const parser = new CollectingSubstringParser( "bar" );
		const stream = new Stream( { initialParser: parser } );

		const collector = parser.collected.pipe( new BufferStream.Writer() );
		const output = stream.pipe( new BufferStream.Writer() );

		stream.write( Buffer.from( "foo ba" ) );
		stream.end( Buffer.from( "r" ) );

		( await collector.asPromise ).should.deepEqual( Buffer.from( "foo " ) );
		( await output.asPromise ).should.deepEqual( Buffer.from( "foo bar" ) );
	} );
} );
