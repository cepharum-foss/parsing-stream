import { describe, it } from "mocha";
import "should";

import {
	CollectingPatternParser, CollectingSubstringParser, DisabledParser,
	Parser,
	PatternParser,
	RaceParser,
	SubstringParser
} from "../index.mjs";
import { tryParser } from "./helper.mjs";


describe( "RaceParser", () => {
	it( "is exposed", () => {
		RaceParser.should.be.ok();
	} );

	it( "rejects to be created without arguments", () => {
		( () => new RaceParser() ).should.throw();
	} );

	it( "requires non-empty list of parsers on construction", () => {
		( () => new RaceParser( null ) ).should.throw();
		( () => new RaceParser( undefined ) ).should.throw();
		( () => new RaceParser( false ) ).should.throw();
		( () => new RaceParser( true ) ).should.throw();
		( () => new RaceParser( 0 ) ).should.throw();
		( () => new RaceParser( 12 ) ).should.throw();
		( () => new RaceParser( -32.54 ) ).should.throw();
		( () => new RaceParser( "" ) ).should.throw();
		( () => new RaceParser( "   " ) ).should.throw();
		( () => new RaceParser( {} ) ).should.throw();
		( () => new RaceParser( { parser: new Parser() } ) ).should.throw();
		( () => new RaceParser( () => new Parser() ) ).should.throw();

		( () => new RaceParser( [] ) ).should.throw();
		( () => new RaceParser( [null] ) ).should.throw();
		( () => new RaceParser( [undefined] ) ).should.throw();
		( () => new RaceParser( [false] ) ).should.throw();
		( () => new RaceParser( [true] ) ).should.throw();
		( () => new RaceParser( [0] ) ).should.throw();
		( () => new RaceParser( [12] ) ).should.throw();
		( () => new RaceParser( [-32.54] ) ).should.throw();
		( () => new RaceParser( [""] ) ).should.throw();
		( () => new RaceParser( ["   "] ) ).should.throw();
		( () => new RaceParser( [{}] ) ).should.throw();
		( () => new RaceParser( [{ parser: new Parser() }] ) ).should.throw();
		( () => new RaceParser( [() => new Parser()] ) ).should.throw();

		( () => new RaceParser( [new Parser()] ) ).should.not.throw();
	} );

	it( "rejects list of parsers being replaced", () => {
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );
		const parser = new RaceParser( [ first, second ] );

		( () => { parser.parsers = undefined; } ).should.throw();
		( () => { parser.parsers = null; } ).should.throw();
		( () => { parser.parsers = false; } ).should.throw();
		( () => { parser.parsers = true; } ).should.throw();
		( () => { parser.parsers = 0; } ).should.throw();
		( () => { parser.parsers = 12; } ).should.throw();
		( () => { parser.parsers = -32.53; } ).should.throw();
		( () => { parser.parsers = ""; } ).should.throw();
		( () => { parser.parsers = "   "; } ).should.throw();
		( () => { parser.parsers = {}; } ).should.throw();
		( () => { parser.parsers = { parser: first }; } ).should.throw();
		( () => { parser.parsers = () => first; } ).should.throw();

		( () => { parser.parsers = [undefined]; } ).should.throw();
		( () => { parser.parsers = [null]; } ).should.throw();
		( () => { parser.parsers = [false]; } ).should.throw();
		( () => { parser.parsers = [true]; } ).should.throw();
		( () => { parser.parsers = [0]; } ).should.throw();
		( () => { parser.parsers = [12]; } ).should.throw();
		( () => { parser.parsers = [-32.53]; } ).should.throw();
		( () => { parser.parsers = [""]; } ).should.throw();
		( () => { parser.parsers = ["   "]; } ).should.throw();
		( () => { parser.parsers = [{}]; } ).should.throw();
		( () => { parser.parsers = [{ parser: first }]; } ).should.throw();
		( () => { parser.parsers = [() => first]; } ).should.throw();

		( () => { parser.parsers = [first]; } ).should.throw();
	} );

	it( "accepts list of parsers being modified", () => {
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );
		const parser = new RaceParser( [ first, second ] );

		( () => { parser.parsers[0] = undefined; } ).should.not.throw();
		( () => { parser.parsers[0] = null; } ).should.not.throw();
		( () => { parser.parsers[0] = false; } ).should.not.throw();
		( () => { parser.parsers[0] = true; } ).should.not.throw();
		( () => { parser.parsers[0] = 0; } ).should.not.throw();
		( () => { parser.parsers[0] = 12; } ).should.not.throw();
		( () => { parser.parsers[0] = -32.53; } ).should.not.throw();
		( () => { parser.parsers[0] = ""; } ).should.not.throw();
		( () => { parser.parsers[0] = "   "; } ).should.not.throw();
		( () => { parser.parsers[0] = {}; } ).should.not.throw();
		( () => { parser.parsers[0] = { parser: first }; } ).should.not.throw();
		( () => { parser.parsers[0] = () => first; } ).should.not.throw();

		( () => { parser.parsers[1] = [undefined]; } ).should.not.throw();
		( () => { parser.parsers[1] = [null]; } ).should.not.throw();
		( () => { parser.parsers[1] = [false]; } ).should.not.throw();
		( () => { parser.parsers[1] = [true]; } ).should.not.throw();
		( () => { parser.parsers[1] = [0]; } ).should.not.throw();
		( () => { parser.parsers[1] = [12]; } ).should.not.throw();
		( () => { parser.parsers[1] = [-32.53]; } ).should.not.throw();
		( () => { parser.parsers[1] = [""]; } ).should.not.throw();
		( () => { parser.parsers[1] = ["   "]; } ).should.not.throw();
		( () => { parser.parsers[1] = [{}]; } ).should.not.throw();
		( () => { parser.parsers[1] = [{ parser: first }]; } ).should.not.throw();
		( () => { parser.parsers[1] = [() => first]; } ).should.not.throw();

		( () => { parser.parsers[100] = [first]; } ).should.not.throw();

		for ( const name of [ "push", "unshift" ] ) {
			( () => { parser.parsers[name]( undefined ); } ).should.not.throw();
			( () => { parser.parsers[name]( null ); } ).should.not.throw();
			( () => { parser.parsers[name]( false ); } ).should.not.throw();
			( () => { parser.parsers[name]( true ); } ).should.not.throw();
			( () => { parser.parsers[name]( 0 ); } ).should.not.throw();
			( () => { parser.parsers[name]( 12 ); } ).should.not.throw();
			( () => { parser.parsers[name]( -32.53 ); } ).should.not.throw();
			( () => { parser.parsers[name]( "" ); } ).should.not.throw();
			( () => { parser.parsers[name]( "   " ); } ).should.not.throw();
			( () => { parser.parsers[name]( {} ); } ).should.not.throw();
			( () => { parser.parsers[name]( { parser: first } ); } ).should.not.throw();
			( () => { parser.parsers[name]( () => first ); } ).should.not.throw();

			( () => { parser.parsers[name]( [undefined] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [null] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [false] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [true] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [0] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [12] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [-32.53] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [""] ); } ).should.not.throw();
			( () => { parser.parsers[name]( ["   "] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [{}] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [{ parser: first }] ); } ).should.not.throw();
			( () => { parser.parsers[name]( [() => first] ); } ).should.not.throw();

			( () => { parser.parsers[name]( [first] ); } ).should.not.throw();
		}

		for ( const [ start, num, add ] of [ [ 0, 1, true ], [ 100, 1, true ], [ 1, 1, true ], [ 1, 1, false ], [ 0, 100, false ] ] ) {
			( () => { parser.parsers.splice( start, num, ...add ? [undefined] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [null] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [false] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [true] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [0] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [12] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [-32.53] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [""] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? ["   "] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [{}] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [{ parser: first }] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [() => first] : [] ); } ).should.not.throw();
			( () => { parser.parsers.splice( start, num, ...add ? [first] : [] ); } ).should.not.throw();

			if ( add ) {
				( () => { parser.parsers.splice( start, num, [undefined] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [null] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [false] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [true] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [0] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [12] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [-32.53] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [""] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, ["   "] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [{}] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [{ parser: first }] ); } ).should.not.throw();
				( () => { parser.parsers.splice( start, num, [() => first] ); } ).should.not.throw();
			}
		}

		for ( const name of [ "pop", "shift" ] ) {
			parser.parsers.splice( 0, 100, first, second );

			( () => { parser.parsers[name](); } ).should.not.throw();
			( () => { parser.parsers[name](); } ).should.not.throw();
			( () => { parser.parsers[name](); } ).should.not.throw();
		}
	} );

	it( "fails on trying to parse data after having dropped all racing parsers", async() => {
		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );

		const goodParser = new RaceParser( [ first, second ] );
		await tryParser( goodParser, input ).should.be.resolved();

		const badParser = new RaceParser( [ first, second ] );
		badParser.parsers.splice( 0 );
		await tryParser( badParser, input ).should.be.rejectedWith( /empty list/i );
	} );

	it( "fails on trying to parse data after having corrupted list of racing parsers", async() => {
		for ( const fn of [
			/* eslint-disable no-param-reassign */
			parsers => { parsers[0] = null; },
			parsers => { parsers[1] = undefined; },
			parsers => { parsers[100] = false; },
			parsers => { parsers[2] = true; },
			parsers => { parsers[2] = 0; },
			parsers => { parsers[2] = 12; },
			parsers => { parsers[2] = -32.54; },
			parsers => { parsers[2] = ""; },
			parsers => { parsers[2] = "   "; },
			parsers => { parsers[2] = {}; },
			parsers => { parsers[2] = { parser: new SubstringParser( "baz" ) }; },
			parsers => { parsers[2] = () => new SubstringParser( "baz" ); },
			parsers => { parsers[2] = []; },
			parsers => { parsers[2] = [new SubstringParser( "baz" )]; },

			parsers => { parsers[0] = new RaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[1] = new RaceParser( [new SubstringParser( "baz" )] ); },
			parsers => { parsers[2] = new RaceParser( [new SubstringParser( "baz" )] ); },

			parsers => { parsers[0] = new CollectingSubstringParser( "baz" ); },
			parsers => { parsers[1] = new CollectingSubstringParser( "baz" ); },
			parsers => { parsers[2] = new CollectingSubstringParser( "baz" ); },

			parsers => { parsers[0] = new CollectingPatternParser( "baz" ); },
			parsers => { parsers[1] = new CollectingPatternParser( "baz" ); },
			parsers => { parsers[2] = new CollectingPatternParser( "baz" ); },
			/* eslint-enable no-param-reassign */
		] ) {
			const input = Buffer.from( "A foo and a bar are hiding in a jar." );
			const parser = new RaceParser( [ new SubstringParser( "foo" ), new SubstringParser( "bar" ) ] );

			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop

			( () => fn( parser.parsers ) ).should.not.throw();

			await tryParser( parser, input ).should.be.rejectedWith( /invalid parser/i ); // eslint-disable-line no-await-in-loop
		}
	} );

	it( "proceeds to parse data after properly adjusting list of racing parsers", async() => {
		const fix = new SubstringParser( "baz" );
		const dyn = new PatternParser( "baz" );
		const dis = new DisabledParser();

		for ( const fn of [
			/* eslint-disable no-param-reassign */
			parsers => { parsers[0] = fix; },
			parsers => { parsers[0] = dyn; },
			parsers => { parsers[0] = dis; },
			parsers => { parsers[1] = fix; },
			parsers => { parsers[1] = dyn; },
			parsers => { parsers[1] = dis; },
			parsers => { parsers.unshift( fix ); },
			parsers => { parsers.unshift( dyn ); },
			parsers => { parsers.unshift( dis ); },
			parsers => { parsers.push( fix ); },
			parsers => { parsers.push( dyn ); },
			parsers => { parsers.push( dis ); },
			parsers => { parsers.splice( 0, 0, fix ); },
			parsers => { parsers.splice( 0, 0, dyn ); },
			parsers => { parsers.splice( 0, 0, dis ); },
			parsers => { parsers.splice( 0, 1, fix ); },
			parsers => { parsers.splice( 0, 1, dyn ); },
			parsers => { parsers.splice( 0, 1, dis ); },
			parsers => { parsers.splice( 4, 0, fix ); },
			parsers => { parsers.splice( 4, 0, dyn ); },
			parsers => { parsers.splice( 4, 0, dis ); },
			parsers => { parsers.splice( 1, 1, fix ); },
			parsers => { parsers.splice( 1, 1, dyn ); },
			parsers => { parsers.splice( 1, 1, dis ); },
			/* eslint-enable no-param-reassign */
		] ) {
			const input = Buffer.from( "A foo and a bar are hiding in a jar." );
			const parser = new RaceParser( [ new SubstringParser( "foo" ), new SubstringParser( "bar" ) ] );

			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop

			( () => fn( parser.parsers ) ).should.not.throw();

			await tryParser( parser, input ).should.be.resolved(); // eslint-disable-line no-await-in-loop
		}
	} );

	it( "finds matches of all attached parsers using match handlers per attached parser", async function() {
		this.timeout( 5000 );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo", { onMatch } );
		const second = new SubstringParser( "bar", { onMatch } );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new RaceParser( [ first, second ] );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", "bar" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 12 ] );
			}
		}
	} );

	it( "finds matches of all attached parsers using common match handler on race parser", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new SubstringParser( "bar" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset, parser ) => {
			( parser === first || parser === second ).should.be.true();

			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new RaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", "bar" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 12 ] );
			}
		}
	} );

	it( "works with mixed set of substring and pattern parsers", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "A foo and a bar are hiding in a jar." );
		const first = new SubstringParser( "foo" );
		const second = new PatternParser( " .+in" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset, parser ) => {
			( parser === first || parser === second ).should.be.true();

			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new RaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "foo", " and a bar are hiding in" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 2, 5 ] );
			}
		}
	} );

	it( "prefers earliest completely discovered match over partial matches when looking for matches capable of overlapping", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffixes" );
		const second = new SubstringParser( "fix" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new RaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "fix", "fix" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 13, 24 ] );
			}
		}
	} );

	it( "prefers earliest starting full match out of multiple full matches completely discovered simultaneously when looking for matches capable of overlapping", async function() {
		this.timeout( 5000 );

		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix" );
		const second = new SubstringParser( "fix" );

		const matches = [];
		const onMatch = ( _, buffers, atOffset ) => {
			matches.push( {
				match: Buffer.concat( buffers ),
				atOffset,
			} );
		};

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				matches.splice( 0 );

				const parser = new RaceParser( [ first, second ], { onMatch } );

				const { data } = await tryParser( parser, input, size, delay ); // eslint-disable-line no-await-in-loop

				data.toString().should.equal( input.toString() );
				matches.length.should.equal( 2 );
				matches.map( match => match.match.toString() ).should.deepEqual( [ "suffix", "fix" ] );
				matches.map( match => match.atOffset ).should.deepEqual( [ 10, 24 ] );
			}
		}
	} );

	it( "fails parsing stream when race parser crashes synchronously on match", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix" );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ], { onMatch: () => { throw new TypeError( "boo!" ); } } );

				await tryParser( parser, input, size, delay ).should.be.rejectedWith( "boo!" ); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	it( "fails parsing stream when a subordinated parser crashes synchronously on match", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix", { onMatch: () => { throw new TypeError( "boo!" ); } } );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ] );

				await tryParser( parser, input, size, delay ).should.be.rejectedWith( "boo!" ); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	it( "does not fail parsing stream when a subordinated parser would crash synchronously on match but race parser's match handler is preferred", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix", { onMatch: () => { throw new TypeError( "boo!" ); } } );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ], { onMatch() {} } );

				await tryParser( parser, input, size, delay ).should.be.resolved(); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	it( "fails parsing stream when race parser crashes asynchronously on match", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix" );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ], { onMatch: () => new Promise( ( _, reject ) => setTimeout( reject, 20, new TypeError( "boo!" ) ) ) } );

				await tryParser( parser, input, size, delay ).should.be.rejectedWith( "boo!" ); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	it( "fails parsing stream when a subordinated parser crashes asynchronously on match", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix", { onMatch: () => new Promise( ( _, reject ) => setTimeout( reject, 20, new TypeError( "boo!" ) ) ) } );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ] );

				await tryParser( parser, input, size, delay ).should.be.rejectedWith( "boo!" ); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	it( "does not fail parsing stream when a subordinated parser would crash asynchronously on match but race parser's match handler is preferred", async() => {
		const input = Buffer.from( "Look into suffixes when fixing text." );
		const first = new SubstringParser( "suffix", { onMatch: () => new Promise( ( _, reject ) => setTimeout( reject, 20, new TypeError( "boo!" ) ) ) } );
		const second = new SubstringParser( "fix" );

		for ( const size of [ 1, 2, 3, 4, 5, 7, 13, 20, 60 ] ) {
			for ( const delay of [ 0, 10 ] ) {
				const parser = new RaceParser( [ first, second ], { onMatch() {} } );

				await tryParser( parser, input, size, delay ).should.be.resolved(); // eslint-disable-line no-await-in-loop
			}
		}
	} );

	describe( "redirects any subordinated parser's access on heldBuffers property while parsing chunk of data and thus", () => {
		it( "delivers race parser's `heldBuffers` list instead of subordinated parser's own list", async() => {
			let ref;

			class EvilParser extends Parser {
				anytime() {
					ref = this.heldBuffers;
				}

				parse( _, buffers ) {
					ref = this.heldBuffers;
					return this.signalNoMatch( buffers );
				}
			}

			const input = Buffer.from( "Look into suffixes when fixing text." );
			const sole = new EvilParser();
			const race = new RaceParser( [new EvilParser()] );

			sole.anytime();
			ref.should.be.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );

			await tryParser( race, input ).should.be.resolved();
			ref.should.not.be.equal( sole.heldBuffers ).and.equal( race.heldBuffers );
		} );

		it( "prevents subordinated parsers from replacing shared list of held buffers", async() => {
			class EvilParser extends Parser {
				parse( _, buffers ) {
					this.heldBuffers = [Buffer.from( "to be rejected" )];
					return this.signalNoMatch( buffers );
				}
			}

			const input = Buffer.from( "Look into suffixes when fixing text." );
			const sole = new EvilParser();

			await tryParser( sole, input ).should.be.resolved();

			const race = new RaceParser( [new EvilParser()] );

			await tryParser( race, input ).should.be.rejectedWith( /must not replace held buffers/ );
		} );

		it( "lets subordinated parsers adjust other own properties", async() => {
			class EvilParser extends Parser {
				parse( _, buffers ) {
					this.state = [Buffer.from( "to be rejected" )];
					return this.signalNoMatch( buffers );
				}
			}

			const input = Buffer.from( "Look into suffixes when fixing text." );
			const sole = new EvilParser();

			await tryParser( sole, input ).should.be.resolved();

			const race = new RaceParser( [new EvilParser()] );

			await tryParser( race, input ).should.be.resolved();
		} );

		it( "catches subordinated parsers invoking their signalling handlers to call it on winning parser, only", async() => {
			let ref = [];

			class EvilParser extends Parser {
				signalNoMatch( ...args ) {
					ref.push( this.fromParse ? "NO" : "no" );
					return super.signalNoMatch( ...args );
				}

				signalPartialMatch( ...args ) {
					ref.push( this.fromParse ? "PARTIAL" : "partial" );
					return super.signalPartialMatch( ...args );
				}

				signalFullMatch( ...args ) {
					ref.push( this.fromParse ? "FULL" : "full" );
					return super.signalFullMatch( ...args );
				}

				parse( context, buffers, atOffset ) {
					let result;

					this.fromParse = true;

					switch ( atOffset > 0 ? 2 : buffers[0] ) {
						case 0x00 : result = this.signalFullMatch( context, buffers, atOffset, 0, 1 ); break;
						case 0x01 : result = this.signalPartialMatch( buffers, 1 ); break;
						default : result = this.signalNoMatch( buffers );
					}

					this.fromParse = false;

					return result;
				}
			}

			const sole = new EvilParser();
			const race = new RaceParser( [ new EvilParser(), new EvilParser() ] );

			ref = [];
			await tryParser( sole, Buffer.from( [0] ) ).should.be.resolved();
			ref.should.deepEqual( [ "FULL", "NO", "PARTIAL" ] ); // signalNoMatch() is calling signalPartialMatch() internally

			ref = [];
			await tryParser( race, Buffer.from( [0] ) ).should.be.resolved();
			ref.should.deepEqual( [ "full", "no", "partial" ] ); // signalNoMatch() is calling signalPartialMatch() internally

			ref = [];
			await tryParser( sole, Buffer.from( [1] ) ).should.be.resolved();
			ref.should.deepEqual( [ "PARTIAL", "NO", "PARTIAL" ] ); // signalNoMatch() is calling signalPartialMatch() internally

			ref = [];
			await tryParser( race, Buffer.from( [1] ) ).should.be.resolved();
			ref.should.deepEqual( [ "partial", "no", "partial" ] ); // signalNoMatch() is calling signalPartialMatch() internally

			ref = [];
			await tryParser( sole, Buffer.from( [2] ) ).should.be.resolved();
			ref.should.deepEqual( [ "NO", "PARTIAL", "NO", "PARTIAL" ] ); // signalNoMatch() is calling signalPartialMatch() internally

			ref = [];
			await tryParser( race, Buffer.from( [2] ) ).should.be.resolved();
			ref.should.deepEqual( [ "no", "partial", "no", "partial" ] ); // signalNoMatch() is calling signalPartialMatch() internally
		} );

		it( "lets subordinated parsers replace shared list of held buffers from their signalling handlers", async() => {
			let whileParsing = false;
			let ref;

			class EvilParser extends Parser {
				signalNoMatch( ...args ) {
					this.heldBuffers = ref = [];
					return super.signalNoMatch( ...args );
				}

				signalPartialMatch( ...args ) {
					this.heldBuffers = ref = [];
					return super.signalPartialMatch( ...args );
				}

				signalFullMatch( ...args ) {
					this.heldBuffers = ref = [];
					return super.signalFullMatch( ...args );
				}

				parse( context, buffers, atOffset ) {
					if ( whileParsing ) {
						this.heldBuffers = [Buffer.from( "to be rejected" )];
					}

					switch ( atOffset > 0 ? 2 : buffers[0] ) {
						case 0x00 : return this.signalFullMatch( context, buffers, atOffset, 0, 1 );
						case 0x01 : return this.signalPartialMatch( buffers, 1 );
						default : return this.signalNoMatch( buffers );
					}
				}
			}

			const sole = new EvilParser();
			const race = new RaceParser( [new EvilParser()] );

			whileParsing = true;

			await tryParser( sole, Buffer.from( [0] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [0] ) ).should.be.rejected();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( sole, Buffer.from( [1] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [1] ) ).should.be.rejected();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( sole, Buffer.from( [2] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [2] ) ).should.be.rejected();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );

			whileParsing = false;

			await tryParser( sole, Buffer.from( [0] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [0] ) ).should.be.resolved();
			ref.should.not.equal( sole.heldBuffers ).and.equal( race.heldBuffers );
			await tryParser( sole, Buffer.from( [1] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [1] ) ).should.be.resolved();
			ref.should.not.equal( sole.heldBuffers ).and.equal( race.heldBuffers );
			await tryParser( sole, Buffer.from( [2] ) ).should.be.resolved();
			ref.should.equal( sole.heldBuffers ).and.not.equal( race.heldBuffers );
			await tryParser( race, Buffer.from( [2] ) ).should.be.resolved();
			ref.should.not.equal( sole.heldBuffers ).and.equal( race.heldBuffers );
		} );

		it( "lets subordinated parsers replace any other own property from their signalling handlers", async() => {
			class EvilParser extends Parser {
				constructor() {
					super();
					this.me = this;
				}

				signalNoMatch( ...args ) {
					this.state = this.me; // to bypass any Proxy
					return super.signalNoMatch( ...args );
				}

				signalPartialMatch( ...args ) {
					this.state = this.me; // to bypass any Proxy
					return super.signalPartialMatch( ...args );
				}

				signalFullMatch( ...args ) {
					this.state = this.me; // to bypass any Proxy
					return super.signalFullMatch( ...args );
				}

				parse( context, buffers, atOffset ) {
					switch ( atOffset > 0 ? 2 : buffers[0] ) {
						case 0x00 : return this.signalFullMatch( context, buffers, atOffset, 0, 1 );
						case 0x01 : return this.signalPartialMatch( buffers, 1 );
						default : return this.signalNoMatch( buffers );
					}
				}
			}

			const sole = new EvilParser();
			const race = new RaceParser( [new EvilParser()] );
			race.state = false;

			await tryParser( sole, Buffer.from( [0] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
			await tryParser( race, Buffer.from( [0] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
			await tryParser( sole, Buffer.from( [1] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
			await tryParser( race, Buffer.from( [1] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
			await tryParser( sole, Buffer.from( [2] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
			await tryParser( race, Buffer.from( [2] ) ).should.be.resolved();
			sole.state.should.equal( sole );
			race.state.should.be.false();
		} );
	} );
} );
