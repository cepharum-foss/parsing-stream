import { describe, it } from "mocha";
import "should";

import { Pattern } from "../lib/pattern.mjs";


describe( "Class `Pattern`", () => {
	describe( "has `fromSource()` which", () => {
		it( "is a method", () => {
			Pattern.fromSource.should.be.a.Function();
		} );

		it( "requires source of pattern provided as string", () => {
			( () => Pattern.fromSource() ).should.throw();
			( () => Pattern.fromSource( undefined ) ).should.throw();
			( () => Pattern.fromSource( null ) ).should.throw();
			( () => Pattern.fromSource( { foo: "foo" } ) ).should.throw();
			( () => Pattern.fromSource( ["foo"] ) ).should.throw();
			( () => Pattern.fromSource( () => "foo" ) ).should.throw();

			( () => Pattern.fromSource( "foo" ) ).should.not.throw();
		} );

		it( "rejects empty string as pattern", () => {
			( () => Pattern.fromSource( "" ) ).should.throw();
		} );

		describe( "returns sequence of pattern steps which", () => {
			it( "is an array", () => {
				Pattern.fromSource( "foo" ).should.be.an.Array();
			} );

			it( "lists all discovered steps of pattern", () => {
				const list = Pattern.fromSource( "fo?[op]+" );

				list.should.have.length( 3 );

				list[0].should.be.Object();
				list[0].should.have.property( "values" ).which.is.an.Array().which.is.deepEqual( [Buffer.from( "f" )] );
				list[0].should.have.property( "min" ).which.is.equal( 1 );
				list[0].should.have.property( "max" ).which.is.equal( 1 );
				list[1].should.be.Object();
				list[1].should.have.property( "values" ).which.is.an.Array().which.is.deepEqual( [Buffer.from( "o" )] );
				list[1].should.have.property( "min" ).which.is.equal( 0 );
				list[1].should.have.property( "max" ).which.is.equal( 1 );
				list[2].should.be.Object();
				list[2].should.have.property( "values" ).which.is.an.Array().which.is.deepEqual( [ Buffer.from( "o" ), Buffer.from( "p" ) ] );
				list[2].should.have.property( "min" ).which.is.equal( 1 );
				list[2].should.have.property( "max" ).which.is.equal( Infinity );
			} );

			it( "converts either discovered step of pattern back into string", () => {
				const list = Pattern.fromSource( "fo?[op]+t{0,4}.*(00010203)[^abc]" );

				String( list[0] ).should.be.equal( "f" );
				String( list[1] ).should.be.equal( "o?" );
				String( list[2] ).should.be.equal( "[op]+" );
				String( list[3] ).should.be.equal( "t{0,4}" );
				String( list[4] ).should.be.equal( ".*" );
				String( list[5] ).should.be.equal( "(00010203)" );
				String( list[6] ).should.be.equal( "[^abc]" );
			} );
		} );

		describe( "supports sequences of literally matching characters which", () => {
			it( "must not be empty", () => {
				( () => Pattern.fromSource( "" ) ).should.throw();
			} );

			it( "causes another pattern step per character", () => {
				const compiled = Pattern.fromSource( "foo" );

				compiled.should.be.an.Array().which.has.length( 3 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].values.should.have.length( 1 );
				compiled[1].values[0].equals( Buffer.from( "o" ) ).should.be.true();
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );
				compiled[2].values.should.have.length( 1 );
				compiled[2].values[0].equals( Buffer.from( "o" ) ).should.be.true();
				compiled[2].min.should.be.equal( 1 );
				compiled[2].max.should.be.equal( 1 );
			} );
		} );

		describe( "supports groups of characters which", () => {
			it( "must not be empty", () => {
				( () => Pattern.fromSource( "[]" ) ).should.throw();
			} );

			it( "may consist of single character", () => {
				const compiled = Pattern.fromSource( "[a]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "combines different characters in a single pattern step", () => {
				const compiled = Pattern.fromSource( "[foo]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "o" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "creates separate pattern steps for separate groups", () => {
				const compiled = Pattern.fromSource( "[foo][bar]" );

				compiled.should.be.an.Array().which.has.length( 2 );
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "o" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].values.should.have.length( 3 );
				compiled[1].values[0].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[1].values[1].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[1].values[2].equals( Buffer.from( "r" ) ).should.be.true();
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );
			} );

			it( "can be quantified", () => {
				let compiled = Pattern.fromSource( "[foo]*" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo]+" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo]?" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo]{2,3}" );

				compiled[0].min.should.be.equal( 2 );
				compiled[0].max.should.be.equal( 3 );

				compiled = Pattern.fromSource( "[foo]*[bar]" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( Infinity );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo]+[bar]" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( Infinity );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo]?[bar]" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo]{2,3}[bar]" );

				compiled[0].min.should.be.equal( 2 );
				compiled[0].max.should.be.equal( 3 );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo][bar]*" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 0 );
				compiled[1].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo][bar]+" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo][bar]?" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 0 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo][bar]{2,3}" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 2 );
				compiled[1].max.should.be.equal( 3 );

				compiled = Pattern.fromSource( "[foo]+[bar]*" );

				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( Infinity );
				compiled[1].min.should.be.equal( 0 );
				compiled[1].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo]?[bar]+" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( Infinity );

				compiled = Pattern.fromSource( "[foo]{2,3}[bar]?" );

				compiled[0].min.should.be.equal( 2 );
				compiled[0].max.should.be.equal( 3 );
				compiled[1].min.should.be.equal( 0 );
				compiled[1].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[foo]*[bar]{2,3}" );

				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( Infinity );
				compiled[1].min.should.be.equal( 2 );
				compiled[1].max.should.be.equal( 3 );
			} );

			it( "can be negated", () => {
				let compiled = Pattern.fromSource( "[foo]" );

				compiled[0].negated.should.be.false();

				compiled = Pattern.fromSource( "[^foo]" );

				compiled[0].negated.should.be.true();
			} );

			it( "is not considered negation if ^ is only character of group", () => {
				let compiled = Pattern.fromSource( "[^ ]" );

				compiled[0].negated.should.be.true();

				compiled = Pattern.fromSource( "[^]" );

				compiled[0].negated.should.be.false();
			} );

			it( "are negated with `^` in first position of group, only", () => {
				let compiled = Pattern.fromSource( "[^foo]" );

				compiled[0].negated.should.be.true();

				compiled = Pattern.fromSource( "[f^oo]" );

				compiled[0].negated.should.be.false();

				compiled = Pattern.fromSource( "[fo^o]" );

				compiled[0].negated.should.be.false();

				compiled = Pattern.fromSource( "[foo^]" );

				compiled[0].negated.should.be.false();
			} );

			it( "supports ranges", () => {
				const compiled = Pattern.fromSource( "[a-d]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 4 );
				compiled[0].values[0].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( "c" ) ).should.be.true();
				compiled[0].values[3].equals( Buffer.from( "d" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts literal `-` in leading position", () => {
				const compiled = Pattern.fromSource( "[-d]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.false();
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "d" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts literal `-` in final position", () => {
				const compiled = Pattern.fromSource( "[a-]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.false();
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts group consisting of sole `-`, only", () => {
				const compiled = Pattern.fromSource( "[-]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.false();
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts negated group with literal `-` in leading position", () => {
				const compiled = Pattern.fromSource( "[^-d]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.true();
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "d" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts negated group with literal `-` in final position", () => {
				const compiled = Pattern.fromSource( "[^a-]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.true();
				compiled[0].values.should.have.length( 2 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "accepts negated group consisting of sole `-`, only", () => {
				const compiled = Pattern.fromSource( "[^-]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.true();
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "must be finished", () => {
				( () => Pattern.fromSource( "[a" ) ).should.throw();
				( () => Pattern.fromSource( "[abc" ) ).should.throw();
				( () => Pattern.fromSource( "[-" ) ).should.throw();
				( () => Pattern.fromSource( "[a-" ) ).should.throw();
				( () => Pattern.fromSource( "[a-c" ) ).should.throw();
				( () => Pattern.fromSource( "[^a" ) ).should.throw();
				( () => Pattern.fromSource( "[^abc" ) ).should.throw();
				( () => Pattern.fromSource( "[^-" ) ).should.throw();
				( () => Pattern.fromSource( "[^a-" ) ).should.throw();
				( () => Pattern.fromSource( "[^a-c" ) ).should.throw();

				( () => Pattern.fromSource( "[a]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[abc]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[-]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[a-]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[a-c]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[^a]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[^abc]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[^-]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[^a-]" ) ).should.not.throw();
				( () => Pattern.fromSource( "[^a-c]" ) ).should.not.throw();
			} );
		} );

		describe( "supports `?` quantifier which", () => {
			it( "is accepted when following literal character", () => {
				const compiled = Pattern.fromSource( "f?" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "is rejected in a leading position", () => {
				( () => Pattern.fromSource( "?f" ) ).should.throw();
			} );

			it( "is rejected when following literal character multiple times", () => {
				( () => Pattern.fromSource( "f??" ) ).should.throw();
			} );

			it( "is rejected when following another qualifier", () => {
				( () => Pattern.fromSource( "f*?" ) ).should.throw();
				( () => Pattern.fromSource( "f+?" ) ).should.throw();
				( () => Pattern.fromSource( "f{1,2}?" ) ).should.throw();
			} );
		} );

		describe( "supports `*` quantifier which", () => {
			it( "is accepted when following literal character", () => {
				const compiled = Pattern.fromSource( "f*" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( Infinity );
			} );

			it( "is rejected in a leading position", () => {
				( () => Pattern.fromSource( "*" ) ).should.throw();
			} );

			it( "is rejected when following literal character multiple times", () => {
				( () => Pattern.fromSource( "f**" ) ).should.throw();
			} );

			it( "is rejected when following another quantifier", () => {
				( () => Pattern.fromSource( "f?*" ) ).should.throw();
				( () => Pattern.fromSource( "f+*" ) ).should.throw();
				( () => Pattern.fromSource( "f{1,2}*" ) ).should.throw();
			} );
		} );

		describe( "supports `+` quantifier which", () => {
			it( "is accepted when following literal character", () => {
				const compiled = Pattern.fromSource( "f+" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( Infinity );
			} );

			it( "is rejected in a leading position", () => {
				( () => Pattern.fromSource( "+" ) ).should.throw();
			} );

			it( "is rejected when following literal character multiple times", () => {
				( () => Pattern.fromSource( "f++" ) ).should.throw();
			} );

			it( "is rejected when following another quantifier", () => {
				( () => Pattern.fromSource( "f?+" ) ).should.throw();
				( () => Pattern.fromSource( "f*+" ) ).should.throw();
				( () => Pattern.fromSource( "f{1,2}+" ) ).should.throw();
			} );
		} );

		describe( "supports `{m,n}` range quantifiers which", () => {
			it( "is accepted when following literal character", () => {
				const compiled = Pattern.fromSource( "f{2,3}" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 2 );
				compiled[0].max.should.be.equal( 3 );
			} );

			it( "is rejected in a leading position", () => {
				( () => Pattern.fromSource( "{2,3}" ) ).should.throw();
			} );

			it( "is rejected when following literal character multiple times", () => {
				( () => Pattern.fromSource( "f{2,3}{4,5}" ) ).should.throw();
			} );

			it( "is rejected when following another quantifier", () => {
				( () => Pattern.fromSource( "f?{2,3}" ) ).should.throw();
				( () => Pattern.fromSource( "f*{2,3}" ) ).should.throw();
				( () => Pattern.fromSource( "f+{2,3}" ) ).should.throw();
			} );

			it( "works with single size", () => {
				const compiled = Pattern.fromSource( "f{3}" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 3 );
				compiled[0].max.should.be.equal( 3 );
			} );

			it( "requires single-size range to be positive integer", () => {
				( () => Pattern.fromSource( "f{-1}" ) ).should.throw();
				( () => Pattern.fromSource( "f{0}" ) ).should.throw();
				( () => Pattern.fromSource( "f{1}" ) ).should.not.throw();
			} );

			it( "works with left-open range defaulting to 0", () => {
				const compiled = Pattern.fromSource( "f{,3}" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( 3 );
			} );

			it( "requires lower limit to be non-negative", () => {
				( () => Pattern.fromSource( "f{-1,3}" ) ).should.throw();
				( () => Pattern.fromSource( "f{0,3}" ) ).should.not.throw();
				( () => Pattern.fromSource( "f{1,3}" ) ).should.not.throw();
			} );

			it( "works with right-open range defaulting to Infinity", () => {
				const compiled = Pattern.fromSource( "f{3,}" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 3 );
				compiled[0].max.should.be.equal( Infinity );
			} );

			it( "requires upper limit to be positive", () => {
				( () => Pattern.fromSource( "f{0,-1}" ) ).should.throw();
				( () => Pattern.fromSource( "f{0,0}" ) ).should.throw();
				( () => Pattern.fromSource( "f{0,1}" ) ).should.not.throw();
			} );

			it( "requires upper limit to be greater than or equal than lower limit", () => {
				( () => Pattern.fromSource( "f{3,2}" ) ).should.throw();
				( () => Pattern.fromSource( "f{3,3}" ) ).should.not.throw();
				( () => Pattern.fromSource( "f{3,4}" ) ).should.not.throw();
			} );
		} );

		describe( "supports escape sequences which", () => {
			it( "may be used to prevent detection of functional characters", () => {
				const compiled = Pattern.fromSource( "\\[\\?\\+\\*\\{" );

				compiled.should.be.an.Array().which.has.length( 5 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "[" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].values.should.have.length( 1 );
				compiled[1].values[0].equals( Buffer.from( "?" ) ).should.be.true();
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );
				compiled[2].values.should.have.length( 1 );
				compiled[2].values[0].equals( Buffer.from( "+" ) ).should.be.true();
				compiled[2].min.should.be.equal( 1 );
				compiled[2].max.should.be.equal( 1 );
				compiled[3].values.should.have.length( 1 );
				compiled[3].values[0].equals( Buffer.from( "*" ) ).should.be.true();
				compiled[3].min.should.be.equal( 1 );
				compiled[3].max.should.be.equal( 1 );
				compiled[4].values.should.have.length( 1 );
				compiled[4].values[0].equals( Buffer.from( "{" ) ).should.be.true();
				compiled[4].min.should.be.equal( 1 );
				compiled[4].max.should.be.equal( 1 );
			} );

			it( "may be used to denote any octet using two case-insensitive hexadecimal digits", () => {
				const compiled = Pattern.fromSource( "\\x00\\xfF" );

				compiled.should.be.an.Array().which.has.length( 2 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( [0] ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].values.should.have.length( 1 );
				compiled[1].values[0].equals( Buffer.from( [255] ) ).should.be.true();
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );
			} );

			it( "must be used with exactly two digits", () => {
				( () => Pattern.fromSource( "\\x" ) ).should.throw();
				( () => Pattern.fromSource( "\\xg" ) ).should.throw();
				( () => Pattern.fromSource( "\\xG" ) ).should.throw();
				( () => Pattern.fromSource( "\\x0" ) ).should.throw();
				( () => Pattern.fromSource( "\\xa" ) ).should.throw();
				( () => Pattern.fromSource( "\\xgg" ) ).should.throw();
				( () => Pattern.fromSource( "\\x." ) ).should.throw();
				( () => Pattern.fromSource( "\\x.." ) ).should.throw();

				( () => Pattern.fromSource( "\\xa0" ) ).should.not.throw();
			} );

			it( "may appear in groups", () => {
				let compiled = Pattern.fromSource( "[a\\-b\\x41c\\rd\\ne\\tf]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.false();
				compiled[0].values.should.have.length( 11 );
				compiled[0].values[0].equals( Buffer.from( "09", "hex" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "0a", "hex" ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( "0d", "hex" ) ).should.be.true();
				compiled[0].values[3].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[4].equals( Buffer.from( "A" ) ).should.be.true();
				compiled[0].values[5].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].values[6].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[0].values[7].equals( Buffer.from( "c" ) ).should.be.true();
				compiled[0].values[8].equals( Buffer.from( "d" ) ).should.be.true();
				compiled[0].values[9].equals( Buffer.from( "e" ) ).should.be.true();
				compiled[0].values[10].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[^a\\-b\\x41c\\rd\\ne\\tf]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.true();
				compiled[0].values.should.have.length( 11 );
				compiled[0].values[0].equals( Buffer.from( "09", "hex" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "0a", "hex" ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( "0d", "hex" ) ).should.be.true();
				compiled[0].values[3].equals( Buffer.from( "-" ) ).should.be.true();
				compiled[0].values[4].equals( Buffer.from( "A" ) ).should.be.true();
				compiled[0].values[5].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].values[6].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[0].values[7].equals( Buffer.from( "c" ) ).should.be.true();
				compiled[0].values[8].equals( Buffer.from( "d" ) ).should.be.true();
				compiled[0].values[9].equals( Buffer.from( "e" ) ).should.be.true();
				compiled[0].values[10].equals( Buffer.from( "f" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );

			it( "may appear at either end of a range in a group", () => {
				let compiled = Pattern.fromSource( "[A-\\x43]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.false();
				compiled[0].values.should.have.length( 3 );
				compiled[0].values[0].equals( Buffer.from( "A" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( "B" ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( "C" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "[^\\n-\\r]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].negated.should.be.true();
				compiled[0].values.should.have.length( 4 );
				compiled[0].values[0].equals( Buffer.from( [10] ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( [11] ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( [12] ) ).should.be.true();
				compiled[0].values[3].equals( Buffer.from( [13] ) ).should.be.true();
			} );

			it( "may be ignored when incomplete", () => {
				const compiled = Pattern.fromSource( "\\" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "\\" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );
		} );

		describe( "supports definition of a sequence of octets which", () => {
			it( "must be enclosed in parentheses", () => {
				let compiled = Pattern.fromSource( "(1234)" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( [ 0x12, 0x34 ] ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );

				compiled = Pattern.fromSource( "a(1234)b" );

				compiled.should.be.an.Array().which.has.length( 3 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
				compiled[1].values.should.have.length( 1 );
				compiled[1].values[0].equals( Buffer.from( [ 0x12, 0x34 ] ) ).should.be.true();
				compiled[1].min.should.be.equal( 1 );
				compiled[1].max.should.be.equal( 1 );
				compiled[2].values.should.have.length( 1 );
				compiled[2].values[0].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[2].min.should.be.equal( 1 );
				compiled[2].max.should.be.equal( 1 );
			} );

			it( "must start with opening parenthesis", () => {
				( () => Pattern.octetSequence( { source: "1234)", index: 0 } ) ).should.throw();
				( () => Pattern.octetSequence( { source: "(1234)", index: 0 } ) ).should.not.throw();
				( () => Pattern.octetSequence( { source: "(1234)", index: 1 } ) ).should.throw();
				( () => Pattern.octetSequence( { source: "a(1234)", index: 1 } ) ).should.not.throw();
			} );

			it( "must not appear without closing parenthesis", () => {
				( () => Pattern.fromSource( "(1234" ) ).should.throw();
			} );

			it( "must not be empty", () => {
				( () => Pattern.fromSource( "()" ) ).should.throw();
			} );

			it( "must consist of full octet values", () => {
				( () => Pattern.fromSource( "(a)" ) ).should.throw();
				( () => Pattern.fromSource( "(ab)" ) ).should.not.throw();
				( () => Pattern.fromSource( "(abc)" ) ).should.throw();
				( () => Pattern.fromSource( "(abcd)" ) ).should.not.throw();
				( () => Pattern.fromSource( "(abcde)" ) ).should.throw();
				( () => Pattern.fromSource( "(abcdef)" ) ).should.not.throw();
				( () => Pattern.fromSource( "(0abcdef)" ) ).should.throw();
				( () => Pattern.fromSource( "(01abcdef)" ) ).should.not.throw();
				( () => Pattern.fromSource( "(012abcdef)" ) ).should.throw();
				( () => Pattern.fromSource( "(0123abcdef)" ) ).should.not.throw();
			} );

			it( "may consist of hex-digits, only", () => {
				( () => Pattern.fromSource( "( 1234567890abcdefABCDEF )" ) ).should.throw();
				( () => Pattern.fromSource( "(abcdefg)" ) ).should.throw();
				( () => Pattern.fromSource( "(ABCDEFG)" ) ).should.throw();
				( () => Pattern.fromSource( "(())" ) ).should.throw();
				( () => Pattern.fromSource( "(-*+?{})" ) ).should.throw();

				( () => Pattern.fromSource( "(1234567890abcdefABCDEF)" ) ).should.not.throw();
			} );

			it( "can be quantified", () => {
				const compiled = Pattern.fromSource( "a*(1234){3,7}b?" );

				compiled.should.be.an.Array().which.has.length( 3 );
				compiled[0].values.should.have.length( 1 );
				compiled[0].values[0].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].min.should.be.equal( 0 );
				compiled[0].max.should.be.equal( Infinity );
				compiled[1].values.should.have.length( 1 );
				compiled[1].values[0].equals( Buffer.from( [ 0x12, 0x34 ] ) ).should.be.true();
				compiled[1].min.should.be.equal( 3 );
				compiled[1].max.should.be.equal( 7 );
				compiled[2].values.should.have.length( 1 );
				compiled[2].values[0].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[2].min.should.be.equal( 0 );
				compiled[2].max.should.be.equal( 1 );
			} );

			it( "does not work in a group of characters", () => {
				const compiled = Pattern.fromSource( "[a(1234)b]" );

				compiled.should.be.an.Array().which.has.length( 1 );
				compiled[0].values.should.have.length( 8 );
				compiled[0].values[0].equals( Buffer.from( "(" ) ).should.be.true();
				compiled[0].values[1].equals( Buffer.from( ")" ) ).should.be.true();
				compiled[0].values[2].equals( Buffer.from( "1" ) ).should.be.true();
				compiled[0].values[3].equals( Buffer.from( "2" ) ).should.be.true();
				compiled[0].values[4].equals( Buffer.from( "3" ) ).should.be.true();
				compiled[0].values[5].equals( Buffer.from( "4" ) ).should.be.true();
				compiled[0].values[6].equals( Buffer.from( "a" ) ).should.be.true();
				compiled[0].values[7].equals( Buffer.from( "b" ) ).should.be.true();
				compiled[0].min.should.be.equal( 1 );
				compiled[0].max.should.be.equal( 1 );
			} );
		} );
	} );
} );
