import { describe, it } from "mocha";
import "should";

import { PatternParser } from "../index.mjs";
import { testParser } from "./helper.mjs";

describe( "Pattern-based parser attached to stream", function() {
	it( "rejects empty string provided as pattern", () => {
		( () => new PatternParser( "" ) ).should.throw();
	} );

	it( "detects data matching simple pattern without any quantifiers", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole ? quantifier at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e?re ",
			[ "ere ", "ere ", "re " ],
			[ 37, 43, 52 ]
		);
	} );

	it( "detects data matching pattern with sole ? quantifier inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er?e ",
			[ "ee ", "ere ", "ere " ],
			[ 14, 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole ? quantifier at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere ?",
			[ "ere ", "ere ", "ere" ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple ? quantifiers", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e?r?e ?",
			[ "ee", "re", "e ", "e ", "e ", "ere ", "ere ", "e ", "re ", "e ", "ere" ],
			[ 5, 13, 15, 23, 33, 37, 43, 48, 52, 61, 70 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e*re ",
			[ "ere ", "ere ", "re " ],
			[ 37, 43, 52 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er*e ",
			[ "ee ", "ere ", "ere " ],
			[ 14, 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere *",
			[ "ere ", "ere ", "ere" ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple * quantifiers", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e*r*e *",
			[ "ee", "re", "e ", "e ", "e ", "ere ", "ere ", "e ", "re ", "e ", "ere" ],
			[ 5, 13, 15, 23, 33, 37, 43, 48, 52, 61, 70 ]
		);
	} );

	it( "detects data matching pattern with sole set of accepted values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[0a-f4]re ",
			[ "ere ", "ere ", "are " ],
			[ 37, 43, 51 ]
		);
	} );

	it( "detects data matching pattern with sole set of accepted values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[trx]e ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole set of accepted values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[ .]",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple sets of accepted values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[ea][rt][e][ .]",
			[ "ere ", "ere ", "are ", "ere." ],
			[ 37, 43, 51, 70 ]
		);
	} );

	it( "detects data matching pattern with sole negated set of accepted values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^a-df]re ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole negated set of accepted values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[^a-qs-z0]e ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole negated set of accepted values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[^.0-9x]",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with multiple negated sets of accepted values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^tra-dx][^af-qst]e[^a-z]",
			[ "see,", " we ", " be ", "ere ", "ere ", "ere." ],
			[ 4, 21, 31, 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a set of values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[ge]*re ",
			[ "ere ", "ere ", "re " ],
			[ 37, 43, 52 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a set of values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[ge]+re ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a set of values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[tr]*e ",
			[ "ee ", "ere ", "ere " ],
			[ 14, 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a set of values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[tr]+e ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a set of values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[ .]*",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a set of values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[ .]+",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple * quantifiers on sets of values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[eag]*[tre]*e[ .]*",
			[ "ee", "agree ", "e ", "e ", "ere ", "ere ", "e ", "are ", "e ", "ere." ],
			[ 5, 11, 23, 33, 37, 43, 48, 51, 61, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple + quantifiers on sets of values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[eag]+[tre]+e[ .]+",
			[ "agree ", "ere ", "ere ", "are ", "ere." ],
			[ 11, 37, 43, 51, 70 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a negated set of values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^af]*re ",
			[ "t we should be there where ", "re " ],
			[ 20, 52 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a negated set of values at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^af]+re ",
			["t we should be there where "],
			[20]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a negated set of values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[^ab]*e ",
			[ "ee ", "e there where we ", "e for the " ],
			[ 14, 33, 53 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a negated set of values inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e[^ab]+e ",
			[ "e there where we ", "e for the " ],
			[ 33, 53 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a negated set of values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[^,0-9w-]*",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a negated set of values at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere[^,0-9w-]+",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple * quantifiers on negated sets of values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^raswe]*[^gfwe]*e[^.0-9es-]*",
			[ "You se", "e, I agr", "e", "e that w", "e ", "should be th", "er", "e wh", "er", "e w", "e car", "e for th", "e atmo", "spher", "e" ],
			[ 0, 6, 14, 15, 23, 25, 37, 39, 43, 45, 48, 53, 61, 67, 72 ]
		);
	} );

	it( "detects data matching pattern with multiple + quantifiers on negated sets of values", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"[^raswe]+[^gfwe]+e[^.0-9es-]+",
			[ " should be th", " care for th", " atmospher" ],
			[ 24, 49, 62 ]
		);
	} );

	it( "detects data matching pattern with fixed-size sequence of octets at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6865)re ",
			[ "here ", "here " ],
			[ 36, 42 ]
		);
	} );

	it( "detects data matching pattern with fixed-size sequence of octets inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e(7265) ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with fixed-size sequence of octets at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er(6520)",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with multiple fixed-size sequences of octets", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6865)(72)(6520)",
			[ "here ", "here " ],
			[ 36, 42 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a fixed-size sequence of octets at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6865)*re ",
			[ "here ", "here ", "re " ],
			[ 36, 42, 52 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a fixed-size sequence of octets at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6865)+re ",
			[ "here ", "here " ],
			[ 36, 42 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a fixed-size sequence of octets inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e(7265)* ",
			[ "e ", "e ", "e ", "ere ", "ere ", "e ", "e ", "e " ],
			[ 15, 23, 33, 37, 43, 48, 53, 61 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a fixed-size sequence of octets inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e(7265)+ ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole * quantifier on a fixed-size sequence of octets at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er(6520)*",
			[ "ere ", "ere ", "er" ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with sole + quantifier on a fixed-size sequence of octets at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er(6520)+",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with multiple * quantifiers on fixed-size sequences of octets", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6520)*[tw](686572)*(6520)*",
			[ "e t", "t", "we ", "e there ", "where ", "we ", "t", "t" ],
			[ 15, 20, 22, 33, 41, 47, 59, 64 ]
		);
	} );

	it( "detects data matching pattern with multiple + quantifiers on fixed-size sequences of octets", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"(6520)+[tw](686572)+(6520)+",
			["e there "],
			[33]
		);
	} );

	it( "detects data matching pattern with any-match at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			".re ",
			[ "ere ", "ere ", "are " ],
			[ 37, 43, 51 ]
		);
	} );

	it( "detects data matching pattern with any-match inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e.e ",
			[ "ere ", "ere " ],
			[ 37, 43 ]
		);
	} );

	it( "detects data matching pattern with any-match at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"ere.",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple any-matches", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			".e.e.",
			[ "here ", "here ", "here." ],
			[ 36, 42, 69 ]
		);
	} );

	it( "detects data matching pattern with sole quantifier on an any-match at beginning of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			".{1,3}re ",
			[ "there ", "where ", " care " ],
			[ 35, 41, 49 ]
		);
	} );

	it( "detects data matching pattern with sole quantifier on an any-match inside a pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"e.{1,2} ",
			[ "ee, ", "ee ", "ere ", "ere " ],
			[ 5, 14, 37, 43 ]
		);
	} );

	it( "detects data matching pattern with sole quantifier on an any-match at end of pattern", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			"er.{2}",
			[ "ere ", "ere ", "ere." ],
			[ 37, 43, 70 ]
		);
	} );

	it( "detects data matching pattern with multiple quantifiers on fixed-size sequences of octets", async() => {
		await testParser(
			"You see, I agree that we should be there where we care for the atmosphere.",
			".{1,2}h.{0,2}e.{3}",
			[ " there wh", " the at", "sphere." ],
			[ 34, 58, 67 ]
		);
	} );

	it( "detects data matching pattern with single unlimited any-match", async() => {
		await testParser(
			"A foo and a bar are hiding in a jar.",
			" .+in",
			[" foo and a bar are hiding in"],
			[1]
		);
	} );
} );
