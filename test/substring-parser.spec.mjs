import { describe, it } from "mocha";
import "should";

import { createRandomData, tryAllChunkSizes } from "./helper.mjs";

import { SubstringParser } from "../index.mjs";


describe( "Substring parser attached to stream", function() {
	it( "rejects to search for empty string", () => {
		( () => new SubstringParser( Buffer.from( [] ) ) ).should.throw();
	} );

	it( "rejects to search for anything but a Buffer or string", () => {
		( () => new SubstringParser() ).should.throw();
		( () => new SubstringParser( 1 ) ).should.throw();
		( () => new SubstringParser( true ) ).should.throw();
		( () => new SubstringParser( [Buffer.from( "foo" )] ) ).should.throw();
		( () => new SubstringParser( { needle: Buffer.from( "foo" ) } ) ).should.throw();
		( () => new SubstringParser( () => Buffer.from( "foo" ) ) ).should.throw();

		( () => new SubstringParser( Buffer.from( "foo" ) ) ).should.not.throw();
		( () => new SubstringParser( "foo" ) ).should.not.throw();
	} );

	it( "detects sequences of streamed data", async() => {
		let _context;

		await tryAllChunkSizes( "I were there where we care for atmosphere.", () => {
			_context = null;

			const parser = new SubstringParser( Buffer.from( "ere ", "ascii" ) );

			parser.onMatch = function( context, buffers, atOffset ) {
				this.should.be.equal( parser );
				buffers.should.be.Array().which.is.not.empty();
				atOffset.should.be.Number().which.is.above( -1 );

				if ( _context ) {
					context.should.equal( _context );
				} else {
					_context = context;
				}

				if ( !Array.isArray( context.matches ) ) {
					context.matches = []; // eslint-disable-line no-param-reassign
				}

				context.matches.push( { match: this.data, offset: atOffset } );
			};

			return parser;
		}, ( parser1, stream ) => {
			const { context } = stream;

			context.should.equal( _context );

			context.matches.map( i => Object.keys( i ).sort().join( "," ) ).should.be.deepEqual( [ "match,offset", "match,offset","match,offset" ] );
			context.matches.map( i => i.match.toString() ).should.be.deepEqual( [ "ere ", "ere ","ere " ] );
			context.matches.map( i => i.offset ).should.be.deepEqual( [ 3, 9, 15 ] );
		} );
	} );

	it( "can be parsed for containing some substring", async function() {
		this.timeout( 10000 );

		const testString = Buffer.from( "some test-string to be searched", "utf8" );
		const { data, offsets } = await createRandomData( 256 * 1024, [testString] );

		await tryAllChunkSizes( data, () => {
			const parser = new SubstringParser( testString );

			parser.onMatch = function( context, buffers, atOffset ) {
				if ( !Array.isArray( context.matches ) ) {
					context.matches = []; // eslint-disable-line no-param-reassign
				}

				context.matches.push( { match: this.data, offset: atOffset } );
			};

			return parser;
		}, ( parser, stream, sunkData ) => {
			const { context: { matches } } = stream;

			sunkData.length.should.be.equal( 256 * 1024 );

			matches.should.be.Array().which.has.length( 1 );
			matches[0].should.be.Object().which.has.properties( "match", "offset" ).and.has.size( 2 );
			matches[0].match.toString( "utf8" ).should.be.equal( testString.toString( "utf8" ) );
			matches[0].offset.should.be.equal( offsets[0] );
		} );
	} );

	it( "properly restarts looking for needle after bailing from a partial match", async function() {
		const testString = Buffer.from( "abc" );

		for ( let size = 20; size < 30; size++ ) {
			const data = Buffer.alloc( size, "a" );

			for ( let offset = 0; offset < size - testString.length; offset++ ) {
				testString.copy( data, offset );

				await tryAllChunkSizes( data, () => { // eslint-disable-line no-await-in-loop
					const parser = new SubstringParser( testString );

					parser.onMatch = function( context, buffers, atOffset ) {
						if ( !Array.isArray( context.matches ) ) {
							context.matches = []; // eslint-disable-line no-param-reassign
						}

						context.matches.push( { match: this.data, offset: atOffset } );
					};

					return parser;
				}, ( parser, stream, sunkData ) => {
					const { context: { matches } } = stream;

					sunkData.length.should.be.equal( size );

					matches.should.be.Array().which.has.length( 1 );
					matches[0].should.be.Object().which.has.properties( "match", "offset" ).and.has.size( 2 );
					matches[0].match.toString( "utf8" ).should.be.equal( testString.toString( "utf8" ) );
					matches[0].offset.should.be.equal( offset );
				} );
			}
		}
	} );
} );
